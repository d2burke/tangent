//
//  CreatePostContolsView.swift
//  Tangent
//
//  Created by Daniel Burke on 2/12/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

protocol CreatePostControlsViewDelegate: class {
    func createPost()
    func addLink()
}

class CreatePostContolsView: UIView {
    weak var delegate: CreatePostControlsViewDelegate? = nil
    let createPostButton: Button = {
        let button = Button(type: .custom)
        button.setTitle("Post", for: .normal)
        button.setTitleColor(TAColor.gray.color, for: .normal)
        button.setTitleColor(TAColor.white.color, for: .highlighted)
        button.highlightedBackgroundColor = .gray
        button.defaultBackgroundColor = .medGray
        button.isUserInteractionEnabled = false
        return button
    }()
    
    let addLinkButton: Button = {
        let button = Button(type: .custom)
        button.setTitle("L", for: .normal)
        button.setTitleColor(TAColor.lightOrange.color, for: .normal)
        button.titleLabel?.font = UIFont(name: "tangent", size: 24)
        button.clipsToBounds = true
        button.layer.cornerRadius = 6
        return button
    }()
    
    lazy var linkPreviewBottom: NSLayoutConstraint = {
        return NSLayoutConstraint(
            item: self.linkPreview,
            attribute: NSLayoutAttribute.bottom,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.bottom,
            multiplier: 1,
            constant: 40
        )
    }()
    lazy var linkPreview: UIView = {
        let view = UIView(frame: .zero)
        view.addSubview(self.linkImageView)
        view.addSubview(self.linkDisplayView)
        view.addSubview(self.linkIcon)
        view.alpha = 0
        return view
    }()
    
    lazy var linkDisplayView: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = TAColor.medGray.color
        view.layer.cornerRadius = 6
        
        view.addSubview(self.linkTitleLabel)
        view.addSubview(self.linkLabel)
        
        return view
    }()
    
    let linkIcon: TALabel = {
        let view = TALabel(frame: .zero)
        view.font = UIFont(name: "tangent", size: 12)
        view.textAlignment = .center
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
        view.layer.borderColor = TAColor.white.color.cgColor
        view.layer.borderWidth = 2
        view.text = "L"
        view.textColor = TAColor.lightOrange.color
        view.layer.backgroundColor = TAColor.medGray.color.cgColor
        return view
    }()
    
    let linkImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.backgroundColor = TAColor.medGray.color
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 4
        return imageView
    }()
    
    let linkTitleLabel = TALabel(viewModel: LabelViewModel(
        text: "You pasted a link!",
        textStyle: .tiny,
        numberOfLines: 2
    ))
    
    let linkLabel = TALabel(viewModel: LabelViewModel(
        text: "",
        textColor: .gray,
        textStyle: .tiny,
        numberOfLines: 1
    ))
    
    init() {
        super.init(frame: .zero)
        backgroundColor = .clear
        
        createPostButton.addTarget(self, action: #selector(didTapCreatePostButton), for: .touchUpInside)
        addSubview(createPostButton)
        
        addLinkButton.isHidden = true
        addLinkButton.addTarget(self, action: #selector(didTapAddLinkButton), for: .touchUpInside)
        addSubview(addLinkButton)
        
        addSubview(linkPreview)
        
        
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "linkPreview": linkPreview,
            "linkIcon": linkIcon,
            "linkImageView": linkImageView,
            "linkLabel": linkLabel,
            "linkTitleLabel": linkTitleLabel,
            "linkDisplayView": linkDisplayView,
            "createPostButton": createPostButton,
            "addLinkButton": addLinkButton
        ]
        
        blockAutoResizing(views: views.flatMap({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[linkPreview]-[createPostButton(==80)]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[linkPreview(==44)]", options: [], metrics: nil, views: views),
            [linkPreviewBottom],
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[linkImageView(==50)]-[linkDisplayView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-(-10)-[linkIcon(==20)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[linkIcon(==20)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[addLinkButton(==44)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[addLinkButton(==44)]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[createPostButton(==44)]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[linkImageView]-(<=1)-[linkIcon]", options: .alignAllCenterY, metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[linkImageView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[linkDisplayView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[linkTitleLabel]-5-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[linkLabel]-5-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-1-[linkTitleLabel(==24)][linkLabel]-1-|", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    func didTapCreatePostButton() {
        delegate?.createPost()
    }
    
    func didTapAddLinkButton(){
        delegate?.addLink()
    }
}
