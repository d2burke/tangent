//
//  PostCommentCell.swift
//  Tangent
//
//  Created by Daniel Burke on 2/15/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class PostCommentCell: UITableViewCell, PostCell {

    var isPressed: Bool = false {
        didSet {
            let shadowHeight: CGFloat = self.isPressed ? 1 : 7
            self.cardContainer.layer.shadowOpacity = self.isPressed ? 0.1 : 0.2
            self.cardContainer.layer.shadowRadius = self.isPressed ? 2 : 5
            self.cardContainer.layer.shadowOffset = CGSize(width: 0, height: shadowHeight)
        }
    }
    let cardContainer: UIView = {
        let view = UIView()
        view.backgroundColor = TAColor.lightGray.color.withAlphaComponent(0.5)
        view.layer.cornerRadius = 6
        view.layer.shadowOpacity = 0
        view.layer.shadowRadius = 5
        view.layer.shadowOffset = CGSize(width: 0, height: 7)
        return view
    }()
    
    var commentImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "tweeting")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = false
        return imageView
    }()
    
    var userInfoLabel: TALabel = {
        let label = TALabel(viewModel: LabelViewModel(
            text: "d2burke commented on a topic",
            textColor: .dark,
            textStyle: .small,
            numberOfLines: 1
        ))
        label.isUserInteractionEnabled = false
        return label
    }()
    
    let moreButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("•", for: .normal)
        button.setTitleColor(.darkGray, for: .normal)
        button.titleLabel?.font = UIFont(name: "tangent", size: 20)
        return button
    }()
    
    var commentLabel: TALabel = {
        let label = TALabel(viewModel: LabelViewModel(
            text: "",
            textColor: .dark,
            textStyle: .paragraph,
            numberOfLines: 0
        ))
        label.isUserInteractionEnabled = false
        return label
    }()
    
    let reactionSelector = ReactionSelector()
    let reactionsDisplayView = ReactionsDisplay()
    let commentRepliesView = CommentRepliesView()
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        selectionStyle = .none
        cardContainer.addSubview(commentImageView)
        cardContainer.addSubview(moreButton)
        cardContainer.addSubview(userInfoLabel)
        cardContainer.addSubview(commentLabel)
        cardContainer.addSubview(commentRepliesView)
        cardContainer.addSubview(reactionSelector)
        reactionSelector.delegate = self
        
        addSubview(cardContainer)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        let viewModel = viewModel as! PostCommentCellViewModel
        commentLabel.text = viewModel.text
        commentImageView.image = UIImage(named: viewModel.imageURL)
        if viewModel.comments.count > 0 {
            commentRepliesView.isHidden = false
            commentRepliesView.replyCountLabel.text = "\(viewModel.comments.count) replies"
        } else {
            commentRepliesView.isHidden = true
        }
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "cardContainer": cardContainer,
            "moreButton": moreButton,
            "commentLabel": commentLabel,
            "userInfoLabel": userInfoLabel,
            "commentImageView": commentImageView,
            "reactionSelector": reactionSelector,
            "commentRepliesView": commentRepliesView
        ]
        
        blockAutoResizing(views: views.flatMap({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-14-[reactionSelector(==280)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[reactionSelector(==32)]-14-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[cardContainer]-15-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-30-[cardContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[commentLabel]-10-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-[commentImageView(==20)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[commentImageView(==20)]-[userInfoLabel]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-[userInfoLabel(==15)]-15-[commentLabel]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[moreButton(==44)]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[moreButton]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[commentRepliesView(==150)]-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[commentRepliesView(==40)]-|", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}

extension PostCommentCell: ReactionSelectorDelegate {
    func didRelease(reaction: ReactionType) {
        //
    }
    
    func didBeginReactionSelector() {
        //
    }
}
