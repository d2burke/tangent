//
//  ReplyView.swift
//  Tangent
//
//  Created by Daniel Burke on 2/15/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

/**
 *  The ReplyView contains a comment bar (UITextField) for
 *  basic test entry, an "Add Media" button, and a "Send"
 *  button. The comment bar properly handles growing text
 */
class ReplyView: UIView {
    let replyTextField: UITextField = {
        let field = TATextField(viewModel: TATextFieldViewModel(
            placeholderText: "Enter a comment...",
            backgroundColor: .lightGray
        ))
        field.rightViewMode = .always
        field.leftViewMode = .always
        return field
    }()
    
    let addMediaButton: Button = {
        let button = Button(type: .custom)
        button.setTitle("+", for: .normal)
        button.titleEdgeInsets = UIEdgeInsets(top: -3, left: 0, bottom: 0, right: 0)
        button.setTitleColor(TAColor.gray.color, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 28)
        button.setTitleColor(TAColor.white.color, for: .highlighted)
        button.highlightedBackgroundColor = .lightOrange
        button.defaultBackgroundColor = .clear
        return button
    }()
    
    let sendButton: Button = {
        let button = Button(type: .custom)
        button.setTitle("=", for: .normal)
        button.setTitleColor(TAColor.gray.color, for: .normal)
        button.titleLabel?.font = UIFont(name: "tangent", size: 20)
        button.setTitleColor(TAColor.white.color, for: .highlighted)
        button.highlightedBackgroundColor = .lightOrange
        button.defaultBackgroundColor = .clear
        return button
    }()
    
    var sendButtonAction: (() -> Void)? = nil {
        didSet {
            self.sendButton.removeTarget(nil, action: nil, for: .touchUpInside)
            self.sendButton.addTarget(self, action:#selector(self.didTapSendButton(_:)), for: .touchUpInside)
        }
    }
    
    var mediaButtonAction: (() -> Void)? = nil {
        didSet {
            self.addMediaButton.removeTarget(nil, action: nil, for: .touchUpInside)
            self.addMediaButton.addTarget(self, action:#selector(self.didTapMediaButton(_:)), for: .touchUpInside)
        }
    }
    
    let topBorder = CALayer()
    
    init() {
        super.init(frame: .zero)
        backgroundColor = TAColor.white.color
        replyTextField.leftView = addMediaButton
        replyTextField.rightView = sendButton
        addSubview(replyTextField)
        installConstraints()

        topBorder.backgroundColor = TAColor.medGray.color.cgColor
        layer.addSublayer(topBorder)
    }
    
    override func layoutSubviews() {
        let width = bounds.size.width
        topBorder.frame = CGRect(x: 0, y: 0, width: width, height: 1)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConstraints() {
        let views = ["replyTextField": replyTextField, "sendButton": sendButton, "addMediaButton": addMediaButton]
        replyTextField.translatesAutoresizingMaskIntoConstraints = false
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        addMediaButton.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-[replyTextField]-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-[replyTextField]-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[addMediaButton(==40)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[addMediaButton(==32)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[sendButton(==50)]", options: [], metrics: nil, views: views),
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    func didTapSendButton(_ sender: AnyObject?) {
        sendButtonAction?()
    }
    
    func didTapMediaButton(_ sender: AnyObject?) {
        mediaButtonAction?()
    }
}
