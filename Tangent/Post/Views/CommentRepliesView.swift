//
//  CommentRepliesView.swift
//  Tangent
//
//  Created by Daniel Burke on 4/22/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class CommentRepliesView: UIStackView {
    var replyUsersView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 5
        for _ in 0...5 {
            let view = UIImageView(frame: .zero)
            view.image = UIImage(named: "tweeting")
            view.backgroundColor = TAColor.lightGray.color
            view.layer.cornerRadius = 3
            view.clipsToBounds = true
            view.contentMode = .scaleAspectFill
            view.widthAnchor.constraint(equalToConstant: 20)
            view.heightAnchor.constraint(equalToConstant: 20)
            stackView.addArrangedSubview(view)
        }
        return stackView
    }()
    
    var replyCountLabel: TALabel = {
        let label = TALabel(viewModel: LabelViewModel(
            text: "14 Replies",
            textColor: .orange,
            textStyle: .small,
            numberOfLines: 0,
            textAlignment: .right
        ))
        label.isUserInteractionEnabled = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        axis = .vertical
        distribution = .fillEqually
        addArrangedSubview(replyUsersView)
        addArrangedSubview(replyCountLabel)
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "replyCountLabel": replyCountLabel,
            "replyUsersView": replyUsersView
        ]
        
        blockAutoResizing(views: views.flatMap({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[replyCountLabel]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[replyUsersView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[replyCountLabel(==20)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[replyUsersView(==20)]", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}
