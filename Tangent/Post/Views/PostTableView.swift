//
//  PostTableView.swift
//  Tangent
//
//  Created by Daniel Burke on 2/15/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class PostTableView: UITableView {
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: .grouped)
        separatorStyle = .none
        backgroundColor = .clear
        register(PostCommentCell.self, forCellReuseIdentifier: "postComment")
        register(PostTitleHeaderView.self, forHeaderFooterViewReuseIdentifier: "titleHeader")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
