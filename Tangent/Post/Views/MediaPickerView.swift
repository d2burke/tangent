//
//  MediaPickerView.swift
//  Tangent
//
//  Created by Daniel Burke on 2/12/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class MediaPickerView: UIView {
    fileprivate let photoItemSize: CGFloat = 80
    
    //TODO: Refactor with viewmodel
    fileprivate let photoPickerView: UICollectionView = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 80, height: 80)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 1
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .clear
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "photo")

        return collectionView
    }()
    
    //TODO: Refactor with viewmodel
    fileprivate let newsPickerView: UICollectionView = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 180, height: 135)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 1
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = .clear
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "news")
        
        return collectionView
    }()

    init() {
        super.init(frame: .zero)
        backgroundColor = TAColor.lightGray.color

        photoPickerView.delegate = self
        photoPickerView.dataSource = self
        addSubview(photoPickerView)
        
        newsPickerView.delegate = self
        newsPickerView.dataSource = self
        addSubview(newsPickerView)
        
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConstraints() {
        let views: [String: UIView] = ["photoPickerView": photoPickerView, "newsPickerView": newsPickerView]
        let metrics = ["photoItemSize": photoItemSize]
        newsPickerView.translatesAutoresizingMaskIntoConstraints = false
        photoPickerView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[photoPickerView]|", options: [], metrics: metrics, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[newsPickerView]|", options: [], metrics: metrics, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[photoPickerView(==photoItemSize)]-1-[newsPickerView]|", options: [], metrics: metrics, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}

extension MediaPickerView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = collectionView == photoPickerView ? "photo" : "news"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        cell.backgroundColor = .lightGray
        return cell
    }
}
