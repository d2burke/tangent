//
//  PostTitleHeaderView.swift
//  Tangent
//
//  Created by Daniel Burke on 4/18/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class PostTitleHeaderView: UITableViewHeaderFooterView {
    
    var userInfoLabel: TALabel = {
        let label = TALabel(viewModel: LabelViewModel(
            text: "d2burke posted a topic",
            textColor: .gray,
            textStyle: .small,
            numberOfLines: 1
        ))
        label.isUserInteractionEnabled = false
        label.sizeToFit()
        return label
    }()
    
    var titleLabel: TALabel = {
        let label = TALabel(viewModel: LabelViewModel(
            textColor: .dark,
            textStyle: .largePostTitle,
            numberOfLines: 0
        ))
        label.isUserInteractionEnabled = false
        label.sizeToFit()
        return label
    }()
    
    let reactionSelector = ReactionSelector()
    let reactionsDisplayView = ReactionsDisplay()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .white
        addSubview(titleLabel)
        addSubview(userInfoLabel)
        addSubview(reactionSelector)
        addSubview(reactionsDisplayView)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "titleLabel": titleLabel,
            "userInfoLabel": userInfoLabel,
            "reactionSelector": reactionSelector,
            "reactionsDisplayView": reactionsDisplayView
        ]
        
        blockAutoResizing(views: views.flatMap({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[titleLabel]-15-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[userInfoLabel]-15-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-15-[userInfoLabel(==12)][titleLabel]-[reactionSelector(==32)]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-14-[reactionSelector(==280)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-55-[reactionsDisplayView]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[reactionsDisplayView(==20)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[reactionSelector]-(<=1)-[reactionsDisplayView]", options: .alignAllCenterY, metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}
