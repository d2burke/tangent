//
//  PostTableViewDataSource.swift
//  Tangent
//
//  Created by Daniel Burke on 2/10/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

enum PostCommentType {
    case comment
    case reply
}

protocol PostCell {
    var isPressed: Bool { get set }
    func configure(viewModel: IdentifiableViewModel)
}

extension PostViewController: UITableViewDelegate, UITableViewDataSource {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = scrollView.contentOffset.y
        let topicImageTopPosition = y > 0 ? -y/2 : 0
        let topicImageHeight = y > 0 ? 240 : 240 + -y
        topicImageTopConstraint.first?.constant = topicImageTopPosition
        topicImageHeightConstraint.first?.constant = topicImageHeight
        
        if y > 220 {
            if viewIsDark {
                makeViewLight()
                backButton.setTitleColor(TAColor.dark.color, for: .normal)
            }
        } else if !viewIsDark && topicImageView.image != nil {
            makeViewDark()
            backButton.setTitleColor(TAColor.white.color, for: .normal)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "titleHeader") as! PostTitleHeaderView
        headerView.userInfoLabel.text = post.author.firstName + " posted this yesterday"
        headerView.titleLabel.text = post.text
        let currentUser = User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: "http://d2burke.com/img/portrait.jpg")
        
        if let authorReaction = self.post.reactions.filter({ $0.author == currentUser }).first {
            headerView.reactionSelector.setReactionButtonSelected(reaction: authorReaction.type)
        }
        headerView.reactionsDisplayView.configure(reactions: post.reactions, author: currentUser)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let label = TALabel(viewModel: LabelViewModel(
            text: post.text,
            textColor: .dark,
            textStyle: .largePostTitle,
            numberOfLines: 0
        ))
        let height = label.sizeThatFits(CGSize(width: view.frame.size.width-30, height: 500)).height
        return height + 100
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let viewModel = comments[indexPath.row] as? PostCommentCellViewModel else { return 0 }
        let height = viewModel.heightOfText(view: tableView)
        return height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = comments[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: viewModel.identifier, for: indexPath) as! PostCell
        cell.configure(viewModel: viewModel)
        return cell as! UITableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
    }
}
