//
//  CreatePostViewController.swift
//  Tangent
//
//  Created by Daniel Burke on 2/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit
import OpenGraph
import Alamofire
import AlamofireImage

protocol CreatePostViewControllerDelegate: class {
    func didCreatePost(post: Post)
    func didCancelCreatePost()
}

/**
 *  This VC handles the composition and creation
 *  of Tangent posts.
 */
class CreatePostViewController: UIViewController {
    
    weak var delegate: CreatePostViewControllerDelegate? = nil
    let CreatePostPlaceholderText = "Share a thought, photo, or link"
    
    var text: String? = nil
    var photo: UIImage? = nil
    var link: String? = nil
    var imageURL: String? = nil
    
    let cancelButton: Button = {
        let button = Button(type: .custom)
        button.setTitle("v", for: .normal)
        button.setTitleColor(TAColor.lightOrange.color, for: .normal)
        button.setTitleColor(.white, for: .highlighted)
        button.titleLabel?.font = UIFont(name: "tangent", size: 50)
        button.addTarget(self, action: #selector(PostViewController.dismissView), for: .touchUpInside)
        button.highlightedBackgroundColor = .lightOrange
        button.defaultBackgroundColor = .clear
        button.layer.cornerRadius = 25
        return button
    }()
    
    let createTextView: UITextView = {
        let textView = UITextView()
        textView.textColor = TAColor.gray.color
        textView.font = UIFont.systemFont(ofSize: 20)
        textView.keyboardType = .twitter
        textView.tintColor = TAColor.lightOrange.color
        textView.isScrollEnabled = true
        return textView
    }()
    
    //TODO: Refactor with viewmodel
    let createPostControlsView = CreatePostContolsView()
    lazy var createPostButtonBottom: NSLayoutConstraint = {
        return NSLayoutConstraint(
            item: self.createPostControlsView,
            attribute: NSLayoutAttribute.bottom,
            relatedBy: NSLayoutRelation.equal,
            toItem: self.view,
            attribute: NSLayoutAttribute.bottom,
            multiplier: 1,
            constant: -15
        )
    }()
    let labelsContainerView: UIView = {
        let view = UIView()
        return view
    }()
    let mediaPickerView = MediaPickerView()
    
    init(delegate: CreatePostViewControllerDelegate) {
        super.init(nibName: nil, bundle: nil)
        self.delegate = delegate
        cancelButton.addTarget(self, action: #selector(cancelCreatePost), for: .touchUpInside)
        view.addSubview(cancelButton)
        
        createTextView.delegate = self
        createTextView.text = CreatePostPlaceholderText
        view.addSubview(createTextView)
        
        createPostControlsView.delegate = self
        view.addSubview(createPostControlsView)
        
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "cancelButton": cancelButton,
            "createTextView": createTextView,
            "createPostControlsView": createPostControlsView
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-[cancelButton(==50)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-30-[cancelButton(==50)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-90-[createTextView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-[createTextView]-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[createPostControlsView]-15-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[createPostControlsView(==44)]", options: [], metrics: nil, views: views),
            [createPostButtonBottom]
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    func cancelCreatePost() {
        view.endEditing(true)
        delegate?.didCancelCreatePost()
    }
    
    func postIsEmpty() -> Bool {
        return (createTextView.text.characters.count == 0 ||
                createTextView.text == CreatePostPlaceholderText) &&
                photo == nil && link == nil
    }
}

extension CreatePostViewController {
    func captureLink() {
        //Check for pasted link
        let elements = createTextView.text.components(separatedBy: CharacterSet.whitespacesAndNewlines)
        guard var last = elements.last else { return }
        if last.lowercased().hasPrefix("www") {
            last = "https://" + last
        }
        if let url = URL(string: last) {
            if UIApplication.shared.canOpenURL(url) {
                print("Found link!")
                self.link = url.absoluteString
                
                self.createPostControlsView.linkLabel.text = self.link
                self.createPostControlsView.linkPreviewBottom.constant = 0
                
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.2, animations: {
                        self.createPostControlsView.linkPreview.alpha = 1
                        self.view.layoutIfNeeded()
                    })
                }
                
                createTextView.text = createTextView.text.replacingOccurrences(of: url.absoluteString, with: "")
                
                OpenGraph.fetch(url: url) { og, error in
                    
                    DispatchQueue.main.async {
                        self.link = og?[.url]
                        self.createPostControlsView.linkLabel.text = self.link
                        self.createPostControlsView.linkTitleLabel.text = og?[.title]
                        self.imageURL = og?[.image] ?? ""
                        
                        Alamofire.request(self.imageURL!).responseImage { response in
                            if let image = response.result.value {
                                self.createPostControlsView.linkImageView.image = image
                            }
                        }
                    }
                }
            }
        }
    }
}

//MARK: CreatePostControlsViewDelegate
extension CreatePostViewController: CreatePostControlsViewDelegate {
    func createPost() {
        guard !postIsEmpty() else { return }
        view.endEditing(true)
        text = createTextView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        let author = User(
            firstName: "Daniel",
            lastName: "Burke",
            userName: "d2burke",
            avatarURL: "http://d2burke.com/img/portrait.jpg"
        )
        
        let newPost = Post(text: text, author: author, link: link, imageURL: imageURL)
        delegate?.didCreatePost(post: newPost)
    }
    
    func addLink() {
        
    }
}

//MARK: TextView Methods
extension CreatePostViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == CreatePostPlaceholderText {
            textView.text = ""
        } else {
            
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let textColor: TAColor = postIsEmpty() ? .gray : .white
        
        //TODO: Use a 'setState' method to control this
        createPostControlsView.createPostButton.setTitleColor(textColor.color, for: .normal)
        createPostControlsView.createPostButton.defaultBackgroundColor = postIsEmpty() ? .medGray : .lightOrange
        createPostControlsView.createPostButton.isUserInteractionEnabled = !postIsEmpty()
        
        //Check for pasted link
        captureLink()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = CreatePostPlaceholderText
        }
    }
}

//MARK: Keyboard Methods
extension CreatePostViewController {
    func keyboardWillShow(notification: Notification) {
        guard let keyboardFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        let keyboardHeight = view.convert(keyboardFrame, from:nil).size.height
        let keyboardAnimationDuration = ((notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0)
        
        self.createPostButtonBottom.constant = -(keyboardHeight + 15)
        UIView.animate(withDuration: keyboardAnimationDuration, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func keyboardWillHide(notification: Notification) {
        let keyboardAnimationDuration = ((notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0)
        self.createPostButtonBottom.constant = -15
        UIView.animate(withDuration: keyboardAnimationDuration, animations: {
            self.view.layoutIfNeeded()
        })
    }
}
