//
//  Post.swift
//  Tangent
//
//  Created by Daniel Burke on 2/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

enum PostType {
    case text //use when it's text-only
    case photo
    case link
}

struct Post: Item {
    let author: User
    let type: PostType
    let dateCreated: Date
    
    var text: String? = nil
    var photo: UIImage? = nil
    var imageURL: String? = nil
    var link: String? = nil
    var comments: [Comment]
    var reactions: [Reaction]
    var participants: [User]? = nil
    
    init(author: User,
         type: PostType,
         dateCreated:Date,
        text: String? = nil,
        photo: UIImage? = nil,
        imageURL: String? = nil,
        link: String? = nil,
        comments: [Comment] = [Comment](),
        reactions: [Reaction] = [Reaction](),
        participants: [User]? = nil
    ){
        self.author = author
        self.type = type
        self.dateCreated = dateCreated
        self.text = text
        self.photo = photo
        self.imageURL = imageURL
        self.link = link
        self.comments = comments
        self.reactions = reactions
        self.participants = participants
    }
    
    init(text: String? = nil, author: User, link: String? = nil, imageURL: String? = nil) {
        self.text = text
        self.author = author
        self.link = link
        self.imageURL = imageURL
        
        self.photo = nil
        self.type = .text
        self.dateCreated = Date()
        self.comments = [Comment]()
        self.reactions = [Reaction]()
    }
    
    init(text: String, imageURL: String) {
        self.author = User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: "")
        self.type = .photo
        self.text = text
        self.imageURL = imageURL
        self.dateCreated = Date()
        self.comments = [Comment]()
        self.reactions = [Reaction]()
    }
    
    init(photo: UIImage, author: User) {
        self.photo = photo
        self.author = author
        self.type = .photo
        self.dateCreated = Date()
        self.comments = [Comment]()
        self.reactions = [Reaction]()
    }
    
    init(link: String, author: User) {
        self.link = link
        self.author = author
        self.type = .photo
        self.dateCreated = Date()
        self.comments = [Comment]()
        self.reactions = [Reaction]()
    }
}
