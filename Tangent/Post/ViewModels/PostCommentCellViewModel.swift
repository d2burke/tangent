//
//  PostCommentCellViewModel.swift
//  Tangent
//
//  Created by Daniel Burke on 2/15/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation
import UIKit

extension PostCommentCellViewModel: IdentifiableViewModel {
    var identifier: String { return "postComment" }
    var cellHeight: Float { return 230 }
}

public struct PostCommentCellViewModel {
    let text: String
    let imageURL: String
    let reaction: String?
    var reactions: [Reaction]
    var comments: [Comment]
    
    init(
        text: String = "",
        imageURL: String = "",
        reaction: String? = "none",
        reactions: [Reaction] = [Reaction](),
        comments: [Comment] = [Comment]()
        ) {
        self.text = text
        self.imageURL = imageURL
        self.reaction = reaction
        self.reactions = reactions
        self.comments = comments
    }
    
    func heightOfText(view: UIView) -> CGFloat {
        let height = TALabel(viewModel: LabelViewModel(
            text: self.text,
            textColor: .dark,
            textStyle: .paragraph,
            numberOfLines: 0
        )).sizeThatFits(CGSize(width: view.frame.size.width-50, height: 500)).height
        return height + 125
    }
}
