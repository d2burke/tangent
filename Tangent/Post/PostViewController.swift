//
//  PostViewController.swift
//  Tangent
//
//  Created by Daniel Burke on 2/10/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

protocol PostViewcControllerDelegate: class {
    func dismissPost()
}

class PostViewController: UIViewController {
    weak var delegate: PostViewcControllerDelegate? = nil
    let post: Post
    var viewIsDark = false
    
    let reactions = [
        Reaction(type: .agree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .agree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .disagree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .goodPoint, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .agree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .goodPoint, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .goodPoint, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .agree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .disagree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .disagree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .goodPoint, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .agree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date())
    ]
    
    lazy var headerView: UIView = {
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 240)
        view.clipsToBounds = true
        view.backgroundColor = .magenta
        return view
    }()
    
    var topicImageTopConstraint = [NSLayoutConstraint]()
    var topicImageHeightConstraint = [NSLayoutConstraint]()
    var topicImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = false
        return imageView
    }()
    
    let gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        gradientLayer.locations = [0,1]
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradientLayer.opacity = 0.6
        gradientLayer.isHidden = true
        return gradientLayer
    }()
    
    var userInfoLabel: TALabel = {
        let label = TALabel(viewModel: LabelViewModel(
            text: "d2burke posted a topic",
            textColor: .white,
            textStyle: .small,
            numberOfLines: 1
        ))
        label.isUserInteractionEnabled = false
        label.sizeToFit()
        return label
    }()
    
    lazy var backButton: Button = {
        let button = Button(type: .custom)
        button.setTitle("v", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.setTitleColor(.white, for: .highlighted)
        button.titleLabel?.font = UIFont(name: "tangent", size: 50)
        button.addTarget(self, action: #selector(PostViewController.dismissView), for: .touchUpInside)
        button.highlightedBackgroundColor = .lightOrange
        button.defaultBackgroundColor = .clear
        button.layer.cornerRadius = 25
        return button
    }()
    
    let postTableView = PostTableView()
    let replyView = ReplyView()
    
    lazy var replyViewBottomConstraint: NSLayoutConstraint = {
        return NSLayoutConstraint(
            item: self.replyView,
            attribute: NSLayoutAttribute.bottom,
            relatedBy: NSLayoutRelation.equal,
            toItem: self.view,
            attribute: NSLayoutAttribute.bottom,
            multiplier: 1,
            constant: 0
        )
    }()
    
    init(post: Post) {
        self.post = post
        super.init(nibName: nil, bundle: nil)
        
        replyView.sendButtonAction = { [unowned self] in
            guard let commentText = self.replyView.replyTextField.text, !commentText.isEmpty
                else { return }
            
            self.replyView.replyTextField.resignFirstResponder()
            self.replyView.replyTextField.becomeFirstResponder()
            
            let comment = PostCommentCellViewModel(
                text: commentText,
                imageURL: ""
            )
            self.replyView.replyTextField.text = ""
            self.updateConversation(comment: comment)
        }
        
        comments = post.comments.map({ PostCommentCellViewModel(text: $0.text, imageURL: $0.imageURL ?? "tweeting", comments: $0.comments!) })
        
        if let imageURL = post.imageURL {
            headerView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: 240)
            gradientLayer.isHidden = false
            makeViewDark()
            
            Alamofire.request(imageURL).responseImage { response in
                if let image = response.result.value {
                    self.topicImageView.image = image
                }
            }
        } else {
            makeViewLight()
            headerView.frame = .zero
            topicImageView.image = nil
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.layer.cornerRadius = 6
        automaticallyAdjustsScrollViewInsets = false
        
        postTableView.delegate = self
        postTableView.dataSource = self
        postTableView.tableHeaderView = headerView
        view.addSubview(topicImageView)
        view.addSubview(postTableView)
        view.addSubview(backButton)
        view.addSubview(replyView)
        installConstraints()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "headerView": headerView,
            "backButton": backButton,
            "topicImageView": topicImageView,
            "replyView": replyView,
            "postTableView": postTableView
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        topicImageTopConstraint = NSLayoutConstraint.constraints(withVisualFormat: "V:|[topicImageView]", options: [], metrics: nil, views: views)
        topicImageHeightConstraint = NSLayoutConstraint.constraints(withVisualFormat: "V:[topicImageView(==240)]", options: [], metrics: nil, views: views)
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[topicImageView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-[backButton(==50)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[backButton(==50)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[replyView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[postTableView][replyView(==50)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[headerView(==240)]", options: [], metrics: nil, views: views),
            [replyViewBottomConstraint],
            topicImageTopConstraint,
            topicImageHeightConstraint,
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[postTableView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[headerView]|", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    func updateConversation(comment: PostCommentCellViewModel) {
        comments.append(comment)
        let indexPath = IndexPath(row: comments.count-1, section: 0)
        postTableView.beginUpdates()
        postTableView.insertRows(at: [indexPath], with: .bottom)
        postTableView.endUpdates()
        postTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
    
    func dismissView() {
        delegate?.dismissPost()
    }
}

extension PostViewController {
    func makeViewDark() {
        viewIsDark = true
        setNeedsStatusBarAppearanceUpdate()
    }
    
    func makeViewLight() {
        viewIsDark = false
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return viewIsDark ? .lightContent : .default
    }
}

extension PostViewController: ReactionSelectorDelegate {
    func didRelease(reaction: ReactionType) {
        //
    }
    
    func didBeginReactionSelector() {
        //
    }
}

//MARK: Keyboard Methods
extension PostViewController {
    func keyboardWillShow(notification: Notification) {
        guard let keyboardFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        let keyboardHeight = view.convert(keyboardFrame, from:nil).size.height
        let keyboardAnimationDuration = ((notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0)
        
        replyViewBottomConstraint.constant = 49-keyboardHeight
        UIView.animate(withDuration: keyboardAnimationDuration, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
            self.view.layoutIfNeeded()
        }) { [unowned self] (complete) in
            if comments.count > 0 {
                let indexPath = IndexPath(row: comments.count-1, section: 0)
                self.postTableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
    }
    
    func keyboardWillHide(notification: Notification) {
        let keyboardAnimationDuration = ((notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0)
        replyViewBottomConstraint.constant = 0
        UIView.animate(withDuration: keyboardAnimationDuration, animations: {
            self.view.layoutIfNeeded()
        })
    }
}
