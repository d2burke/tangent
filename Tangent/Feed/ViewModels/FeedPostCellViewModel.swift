//
//  FeedPostCellViewModel.swift
//  Tangent
//
//  Created by Daniel Burke on 3/12/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

extension FeedPostCellViewModel: IdentifiableViewModel {
    var identifier: String { return "post" }
    var cellHeight: Float { return 305 }
}

public struct FeedPostCellViewModel {
    let author: User
    let text: String
    let imageURL: String
    let reaction: String?
    var reactions: [Reaction]
    var comments: [Comment]
    
    init(
        author: User,
        text: String = "",
        imageURL: String = "",
        reaction: String? = "none",
        reactions: [Reaction] = [Reaction](),
        comments: [Comment] = [Comment]()
        ) {
        self.author = author
        self.text = text
        self.imageURL = imageURL
        self.reaction = reaction
        self.reactions = reactions
        self.comments = comments
    }
    
    init(post: Post) {
        self.author = post.author
        self.text = post.text ?? ""
        self.imageURL = post.imageURL ?? ""
        self.reaction = post.reactions.first?.type.rawValue ?? "none"
        self.reactions = post.reactions
        self.comments = post.comments
    }
}

