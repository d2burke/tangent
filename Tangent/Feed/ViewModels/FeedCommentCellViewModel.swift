//
//  FeedCommentCellViewModel.swift
//  
//
//  Created by Daniel Burke on 1/2/17.
//
//

import Foundation

extension FeedCommentCellViewModel: IdentifiableViewModel {
    var identifier: String { return "comment" }
    var cellHeight: Float { return 230 }
}

public struct FeedCommentCellViewModel {
    let text: String
    let imageURL: String
    let reaction: String
    var reactions: [Reaction]
    
    init(
        text: String = "",
        imageURL: String = "",
        reaction: String = "none",
        reactions: [Reaction] = [Reaction]()
        ) {
        self.text = text
        self.imageURL = imageURL
        self.reaction = reaction
        self.reactions = reactions
    }
}

