//
//  FeedPeopleCellViewModel.swift
//  Tangent
//
//  Created by Daniel Burke on 4/28/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

extension FeedPeopleCellViewModel: IdentifiableViewModel {
    var identifier: String { return "people" }
    var cellHeight: Float { return 180 }
}

public struct FeedPeopleCellViewModel {
    
}
