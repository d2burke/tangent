//
//  FeedPersonCellViewModel.swift
//  Tangent
//
//  Created by Daniel Burke on 4/28/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

extension FeedPersonCellViewModel: IdentifiableViewModel {
    var identifier: String { return "person" }
    var cellHeight: Float { return 100 }
}

public struct FeedPersonCellViewModel {
    
}
