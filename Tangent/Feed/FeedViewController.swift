//
//  FeedViewController.swift
//  Tangent
//
//  Created by Daniel Burke on 12/29/16.
//  Copyright © 2016 Daniel Burke. All rights reserved.
//

import UIKit
import Firebase
import Apollo

class FeedViewController: UIViewController {
    
    var currentUser: FIRUser?
    var headerTopConstraint = [NSLayoutConstraint]()
    let headerView = PageHeaderView(viewModel: PageHeaderViewModel(
        searchFieldPlaceholderText: "Search Discussions",
        searchFieldIcon: "S",
        pageTitleText: Date.dayOfWeek(),
        actionButtonTitle: "+"
    ))
    let feedCollectionView = FeedCollectionView(frame: .zero)
    let loadingView = LoadingView()
    
    /**
     *  Header
     *  Topics (jump to tngnt topics, collView)
     *  Comment (jump to tngnt topics, collView)
     *  Reactions
     *  Polls
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        automaticallyAdjustsScrollViewInsets = false
        
        feedCollectionView.isHidden = true
        feedCollectionView.delegate = self
        feedCollectionView.dataSource = self
        view.addSubview(feedCollectionView)
        
        headerView.actionButtonAction = openCreatePost
        view.addSubview(headerView)
        
        view.addSubview(loadingView)
        
        installConstraints()
        getPosts()
    }

    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "view": view,
            "headerView": headerView,
            "loadingView": loadingView,
            "feedCollectionView": feedCollectionView
        ]
        
        headerView.translatesAutoresizingMaskIntoConstraints = false
        feedCollectionView.translatesAutoresizingMaskIntoConstraints = false
        
        headerTopConstraint = NSLayoutConstraint.constraints(withVisualFormat: "V:|[headerView]", options: [], metrics: nil, views: views)
        let constraints = [
            headerTopConstraint,
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[headerView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[headerView(==155)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[feedCollectionView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[feedCollectionView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[view]-(<=1)-[loadingView(==300)]", options: .alignAllCenterX, metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[view]-(<=1)-[loadingView(==300)]", options: .alignAllCenterY, metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    func getPosts() {
        self.loadingView.start()
        let service = GraphCoolService()
        service.fetchPosts(success: { [weak self] (posts) in
            topics = posts
            if topics.count > 0 {
                self?.feedCollectionView.isHidden = false
            }
            self?.feedCollectionView.reloadData()
            self?.loadingView.stop()
        })
    }
    
    func open(post: Post) {
        let postViewController = PostViewController(post: post)
        postViewController.delegate = self
        self.navigationController?.pushViewController(postViewController, animated: true)
        view.endEditing(true)
    }
    
    func openCreatePost() {
        let createPostView = CreatePostViewController(delegate: self)
        if let tabBarController = self.tabBarController as? TabBarViewController {
            tabBarController.present(createPostView, animated: true, completion: nil)
        }
    }
}

extension FeedViewController: PostViewcControllerDelegate {
    func dismissPost() {
        navigationController?.popViewController(animated: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            self?.getPosts()
        }
    }
}

extension FeedViewController: CreatePostViewControllerDelegate {
    func didCancelCreatePost() {
        dismiss(animated: true, completion: nil)
    }
    
    func didCreatePost(post: Post) {
        let postQuery = CreatePostMutation(text: post.text, authorId: post.author.userId, link: post.link, imageUrl: post.imageURL)
        apollo.perform(mutation: postQuery) { [weak self] (result, error) in
            //Upload image and get URL
            //Otherwise, Attempt to get imageURL from link
            //Persist post
            //Open post
            self?.open(post: post)
            self?.dismiss(animated: true, completion: nil)
        }
    }
}
