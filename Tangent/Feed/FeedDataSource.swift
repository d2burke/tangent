//
//  FeedDataSource.swift
//  Tangent
//
//  Created by Daniel Burke on 3/12/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

protocol FeedCell {
    var isPressed: Bool { get set }
    func configure(viewModel: IdentifiableViewModel)
}

extension FeedViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView.contentOffset.y < -280 {
            getPosts()
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = scrollView.contentOffset.y
        let headerTopPosition = y < -155 ? 0 : -155 - y
        headerTopConstraint.first?.constant = headerTopPosition
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        var cell = collectionView.cellForItem(at: indexPath) as! FeedCell
        cell.isPressed = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        var cell = collectionView.cellForItem(at: indexPath) as! FeedCell
        cell.isPressed = false
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let author = User(
            firstName: "Daniel",
            lastName: "Burke",
            userName: "d2burke",
            avatarURL: "http://d2burke.com/img/portrait.jpg"
        )
        let post: Post
        let selectedItem = topics[indexPath.row]
        if let feedPost = selectedItem as? FeedPostCellViewModel {
            post = Post(
                author: author,
                type: .text,
                dateCreated: Date(),
                text: feedPost.text,
                imageURL: feedPost.imageURL,
                comments: feedPost.comments,
                reactions: feedPost.reactions
            )
            open(post: post)
        } else if let feedComment = selectedItem as? FeedCommentCellViewModel {
            post = Post(
                author: author,
                type: .text,
                dateCreated: Date(),
                text: feedComment.text,
                photo: UIImage(named: feedComment.imageURL),
                reactions: feedComment.reactions
            )
            open(post: post)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return topics.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellViewModel = topics[indexPath.row]
        return CGSize(width: collectionView.frame.size.width, height: CGFloat(cellViewModel.cellHeight))
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellViewModel = topics[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellViewModel.identifier, for: indexPath) as! FeedCell
        cell.configure(viewModel: cellViewModel)
        
        if let postCell = cell as? FeedPostCell {
            postCell.delegate = self
        } else if let commentCell = cell as? FeedCommentCell {
            commentCell.delegate = self
        }
        
        return cell as! UICollectionViewCell
    }
}

extension FeedViewController: FeedPostCellDelegate {
    func didReactToPost(reaction: ReactionType, item: FeedCell) {
        guard let cell = item as? FeedPostCell,
              let indexPath = feedCollectionView.indexPath(for: cell),
              let post = topics[indexPath.row] as? FeedPostCellViewModel
        else { return }
        
        let currentUser = User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: "http://d2burke.com/img/portrait.jpg")
        var reactions = post.reactions
        
        //Remove user reaction
        if let _ = reactions.filter({ $0.author == currentUser }).first {
            reactions = reactions.filter({ $0.author != currentUser })
        }
        
        if reaction != .none {
            let newReaction = Reaction(type: reaction, author: currentUser, date: Date())
            reactions.append(newReaction)
        }
        
        topics[indexPath.row] = FeedPostCellViewModel(
            author: post.author,
            text: post.text,
            imageURL: post.imageURL,
            reaction: reaction.rawValue,
            reactions: reactions,
            comments: post.comments
        )
        feedCollectionView.reloadItems(at: [indexPath])
    }
}

extension FeedViewController: FeedCommentCellDelegate {
    func didReactToComment(reaction: ReactionType, item: FeedCell) {
        guard let cell = item as? FeedCommentCell,
            let indexPath = feedCollectionView.indexPath(for: cell),
            let post = topics[indexPath.row] as? FeedCommentCellViewModel
            else { return }
        var reactions = post.reactions
        let currentUser = User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: "http://d2burke.com/img/portrait.jpg")
        
        //Remove user reaction
        if let _ = reactions.filter({ $0.author == currentUser }).first {
            reactions = reactions.filter({ $0.author != currentUser })
        }
        
        if reaction != .none {
            let newReaction = Reaction(type: reaction, author: currentUser, date: Date())
            reactions.append(newReaction)
        }
        
        topics[indexPath.row] = FeedCommentCellViewModel(text: post.text, imageURL: post.imageURL, reaction: reaction.rawValue, reactions: reactions)
        feedCollectionView.reloadItems(at: [indexPath])
    }
}
