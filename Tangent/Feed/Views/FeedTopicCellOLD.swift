//
//  FeedTopicCell.swift
//  Tangent
//
//  Created by Daniel Burke on 1/2/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class FeedTopicCellOLD: UITableViewCell, FeedCell {
    
    var isPressed: Bool = false {
        didSet {
            let shadowHeight: CGFloat = self.isPressed ? 2 : 7
            self.cardContainer.layer.shadowOpacity = self.isPressed ? 0.1 : 0.2
            self.cardContainer.layer.shadowRadius = self.isPressed ? 2 : 7
            self.cardContainer.layer.shadowOffset = CGSize(width: 0, height: shadowHeight)
        }
    }
    
    let roundedContainer: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 6
        view.clipsToBounds = true
        return view
    }()
    
    let cardContainer: UIView = {
        let view = UIView()
        view.backgroundColor = TAColor.white.color
        view.layer.cornerRadius = 6
        view.layer.shadowOpacity = 0
        view.layer.shadowRadius = 7
        view.layer.shadowOffset = CGSize(width: 0, height: 7)
        view.layer.shadowColor = TAColor.dark.color.cgColor
        return view
    }()
    
    var topicImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "tweeting")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = false
        return imageView
    }()
    
    let gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        gradientLayer.locations = [0,1]
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradientLayer.opacity = 0.6
        return gradientLayer
    }()
    
    var userInfoLabel: TALabel = {
        let label = TALabel(viewModel: LabelViewModel(
            text: "d2burke posted a topic",
            textColor: .white,
            textStyle: .small,
            numberOfLines: 1
        ))
        label.isUserInteractionEnabled = false
        label.sizeToFit()
        return label
    }()
    
    let moreButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("•", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "tangent", size: 20)
        return button
    }()
    
    var topicLabel: TALabel = {
        let label = TALabel(viewModel: LabelViewModel(
            text: "",
            textColor: .white,
            numberOfLines: 3
        ))
        label.isUserInteractionEnabled = false
        label.sizeToFit()
        return label
    }()
    
    let cardFooterView = FeedCardFooterView()
    let reactionSelector = ReactionSelector()
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        topicImageView.layer.addSublayer(gradientLayer)
        roundedContainer.addSubview(topicImageView)
        roundedContainer.addSubview(moreButton)
        roundedContainer.addSubview(userInfoLabel)
        roundedContainer.addSubview(topicLabel)
        roundedContainer.addSubview(cardFooterView)
        reactionSelector.delegate = self
        roundedContainer.addSubview(reactionSelector)
        cardContainer.addSubview(roundedContainer)
        addSubview(cardContainer)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        let viewModel = viewModel as! FeedPostCellViewModel
        topicLabel.text = viewModel.text
        topicImageView.image = UIImage(named: viewModel.imageURL)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "cardContainer": cardContainer,
            "roundedContainer": roundedContainer,
            "moreButton": moreButton,
            "topicLabel": topicLabel,
            "userInfoLabel": userInfoLabel,
            "topicImageView": topicImageView,
            "cardFooterView": cardFooterView,
            "reactionSelector": reactionSelector
        ]
        
        cardContainer.translatesAutoresizingMaskIntoConstraints = false
        roundedContainer.translatesAutoresizingMaskIntoConstraints = false
        userInfoLabel.translatesAutoresizingMaskIntoConstraints = false
        topicLabel.translatesAutoresizingMaskIntoConstraints = false
        moreButton.translatesAutoresizingMaskIntoConstraints = false
        topicImageView.translatesAutoresizingMaskIntoConstraints = false
        cardFooterView.translatesAutoresizingMaskIntoConstraints = false
        reactionSelector.translatesAutoresizingMaskIntoConstraints = false
        
        var constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[reactionSelector(==280)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[reactionSelector(==60)]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[cardContainer]-15-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[cardContainer]-20-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[roundedContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[roundedContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[topicImageView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[topicLabel]-70-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[topicLabel]-10-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[cardFooterView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[cardFooterView(==60)]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-[userInfoLabel]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-[userInfoLabel]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[moreButton(==44)]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[moreButton]", options: [], metrics: nil, views: views)
        ]
        
        if let _ = topicImageView.image {
            let constraint = NSLayoutConstraint.constraints(withVisualFormat: "V:|[topicImageView]-60-|", options: [], metrics: nil, views: views)
            constraints.append(constraint)
        }
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}

extension FeedTopicCellOLD: ReactionSelectorDelegate {
    func didRelease(reaction: ReactionType) {
        switch reaction {
        case .agree:
            print("Agree")
        case .disagree:
            print("Disagree")
        case .goodPoint:
            print("Good Point")
        case .offensive:
            print("Offensive")
        case .apathetic:
            print("Meh")
        case .none: ()
        }
    }
    
    func didBeginReactionSelector() {
        //
    }
}
