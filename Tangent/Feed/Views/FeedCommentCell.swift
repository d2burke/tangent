//
//  FeedCommentCell.swift
//  Tangent
//
//  Created by Daniel Burke on 3/12/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

protocol FeedCommentCellDelegate: class {
    func didReactToComment(reaction: ReactionType, item: FeedCell)
}

class FeedCommentCell: UICollectionViewCell, FeedCell {
    weak var delegate: FeedCommentCellDelegate? = nil
    
    var isPressed: Bool = false {
        didSet {
            let shadowHeight: CGFloat = self.isPressed ? 1 : 7
            self.cardContainer.layer.shadowOpacity = self.isPressed ? 0.1 : 0.2
            self.cardContainer.layer.shadowRadius = self.isPressed ? 2 : 5
            self.cardContainer.layer.shadowOffset = CGSize(width: 0, height: shadowHeight)
        }
    }
    
    let roundedContainer: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 6
        view.clipsToBounds = true
        return view
    }()
    
    let cardContainer: UIView = {
        let view = UIView()
        view.backgroundColor = TAColor.white.color
        view.layer.cornerRadius = 6
        view.layer.shadowOpacity = 0.2
        view.layer.shadowRadius = 5
        view.layer.shadowOffset = CGSize(width: 0, height: 7)
        view.layer.shadowColor = TAColor.dark.color.cgColor
        return view
    }()
    
    var commentImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "tweeting")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = false
        return imageView
    }()
    
    var userInfoLabel: TALabel = {
        let label = TALabel(viewModel: LabelViewModel(
            text: "d2burke commented on a topic",
            textColor: .dark,
            textStyle: .small,
            numberOfLines: 1
        ))
        label.isUserInteractionEnabled = false
        label.sizeToFit()
        return label
    }()
    
    let moreButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("•", for: .normal)
        button.setTitleColor(.darkGray, for: .normal)
        button.titleLabel?.font = UIFont(name: "tangent", size: 20)
        return button
    }()
    
    var commentLabel: TALabel = {
        let label = TALabel(viewModel: LabelViewModel(
            text: "",
            textColor: .dark,
            textStyle: .paragraph,
            numberOfLines: 3
        ))
        label.isUserInteractionEnabled = false
        return label
    }()
    
    let reactionSelector = ReactionSelector()
    let reactionsDisplayView = ReactionsDisplay()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        roundedContainer.addSubview(commentImageView)
        roundedContainer.addSubview(moreButton)
        roundedContainer.addSubview(userInfoLabel)
        roundedContainer.addSubview(commentLabel)
        reactionSelector.delegate = self
        roundedContainer.addSubview(reactionSelector)
        roundedContainer.addSubview(reactionsDisplayView)
        cardContainer.addSubview(roundedContainer)
        addSubview(cardContainer)
        installConstraints()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        guard let viewModel = viewModel as? FeedCommentCellViewModel else { return }
        
        let currentUser = User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: "http://d2burke.com/img/portrait.jpg")
        reactionsDisplayView.configure(reactions: viewModel.reactions, author: currentUser)
        
        commentLabel.text = viewModel.text
        commentImageView.image = UIImage(named: viewModel.imageURL)
        if let type = ReactionType(string: viewModel.reaction) {
            reactionSelector.setReactionButtonSelected(reaction: type)
        }
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "cardContainer": cardContainer,
            "roundedContainer": roundedContainer,
            "moreButton": moreButton,
            "commentLabel": commentLabel,
            "userInfoLabel": userInfoLabel,
            "commentImageView": commentImageView,
            "reactionSelector": reactionSelector,
            "reactionsDisplayView": reactionsDisplayView
        ]
        
        blockAutoResizing(views: views.flatMap({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-14-[reactionSelector(==280)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-55-[reactionsDisplayView(<=120)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[reactionsDisplayView(==20)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[reactionSelector]-(<=1)-[reactionsDisplayView]", options: .alignAllCenterY, metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[reactionSelector(==32)]-14-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[cardContainer]-15-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-2-[cardContainer]-20-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[roundedContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[roundedContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[commentImageView(==30)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[commentLabel]-10-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-[userInfoLabel]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-[userInfoLabel(==15)]-[commentImageView(==30)]-[commentLabel]-50-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[moreButton(==44)]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[moreButton]", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}

extension FeedCommentCell: ReactionSelectorDelegate {
    func didRelease(reaction: ReactionType) {
        delegate?.didReactToComment(reaction: reaction, item: self)
    }
    
    func didBeginReactionSelector() {
        //
    }
}
