//
//  PeopleCell.swift
//  Tangent
//
//  Created by Daniel Burke on 4/28/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class PeopleCell: UICollectionViewCell, FeedCell {
    let peopleCollectionView = PeopleCollectionView(frame: .zero)
    
    let sectionNameLabel = TALabel(viewModel: LabelViewModel(
        text: "People with dissimilar views",
        textColor: .orange,
        textStyle: .boldParagraph,
        numberOfLines: 0
    ))
    
    var people = [
        FeedPersonCellViewModel(),
        FeedPersonCellViewModel(),
        FeedPersonCellViewModel(),
        FeedPersonCellViewModel(),
        FeedPersonCellViewModel(),
        FeedPersonCellViewModel(),
        FeedPersonCellViewModel(),
        FeedPersonCellViewModel(),
        FeedPersonCellViewModel(),
        FeedPersonCellViewModel(),
        FeedPersonCellViewModel()
    ]

    var isPressed: Bool = false {
        didSet {
            //
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(sectionNameLabel)
        peopleCollectionView.clipsToBounds = false
        peopleCollectionView.delegate = self
        peopleCollectionView.dataSource = self
        addSubview(peopleCollectionView)
        
        installConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configure(viewModel: IdentifiableViewModel) {
        guard let _ = viewModel as? FeedPeopleCellViewModel else { return }
    }

    func installConstraints() {
        let views: [String: UIView] = ["peopleCollectionView": peopleCollectionView, "sectionNameLabel": sectionNameLabel]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-[sectionNameLabel(==30)]-[peopleCollectionView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[sectionNameLabel]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[peopleCollectionView]|", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}

extension PeopleCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return people.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        var cell = collectionView.cellForItem(at: indexPath) as! FeedCell
        cell.isPressed = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        var cell = collectionView.cellForItem(at: indexPath) as! FeedCell
        cell.isPressed = false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let images = [#imageLiteral(resourceName: "anu"),#imageLiteral(resourceName: "cern"),#imageLiteral(resourceName: "jorg"), #imageLiteral(resourceName: "profile-1"),#imageLiteral(resourceName: "martin"),#imageLiteral(resourceName: "moses"),#imageLiteral(resourceName: "paolo"),#imageLiteral(resourceName: "rory"),#imageLiteral(resourceName: "salvador"),#imageLiteral(resourceName: "shawn"),#imageLiteral(resourceName: "toraza")]
        let names = ["Anu", "Leslie", "Jorg", "Burke", "Moses", "Emilia", "Paulo", "Rory", "Mariela", "Shawn","Linda"]
        let cellModel = people[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "person", for: indexPath) as! PersonCell
        cell.personImageView.image = images[indexPath.row]
        cell.personNameLabel.text = names[indexPath.row]
        cell.configure(viewModel: cellModel)
        return cell
    }
}
