//
//  FeedPostCell.swift
//  Tangent
//
//  Created by Daniel Burke on 3/12/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import AlamofireImage

protocol FeedPostCellDelegate: class {
    func didReactToPost(reaction: ReactionType, item: FeedCell)
}

class FeedPostCell: UICollectionViewCell, FeedCell {
    weak var delegate: FeedPostCellDelegate? = nil
    
    var isPressed: Bool = false {
        didSet {
            let shadowHeight: CGFloat = self.isPressed ? 2 : 7
            self.postView.cardContainer.layer.shadowOpacity = self.isPressed ? 0.1 : 0.2
            self.postView.cardContainer.layer.shadowRadius = self.isPressed ? 2 : 7
            self.postView.cardContainer.layer.shadowOffset = CGSize(width: 0, height: shadowHeight)
        }
    }
    
    let postView = PostView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        postView.reactionSelector.delegate = self
        addSubview(postView)
        installConstraints()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        guard let viewModel = viewModel as? FeedPostCellViewModel else { return }
        
        postView.topicLabel.text = viewModel.text
        //TODO: change this text based on the post type
        postView.userInfoLabel.text = viewModel.author.firstName + " posted this"
        
        if !viewModel.imageURL.isEmpty {
            Alamofire.request(viewModel.imageURL).responseImage { response in
                if let image = response.result.value {
                    self.postView.topicLabel.textColor = TAColor.white.color
                    self.postView.topicImageView.isHidden = false
                    self.postView.topicImageView.image = image
                }
            }
        } else {
            self.postView.topicLabel.textColor = TAColor.dark.color
            self.postView.topicImageView.isHidden = true
        }
        
        //TODO: Handle comments & nested comments here
        
        let commentCount = viewModel.comments.count > 0 ? "\(viewModel.comments.count) comments" : ""
        postView.commentCountLabel.text = commentCount
        
        let currentUser = User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: "http://d2burke.com/img/portrait.jpg")
        
        if let authorReaction = viewModel.reactions.filter({ $0.author == currentUser }).first {
            postView.reactionSelector.setReactionButtonSelected(reaction: authorReaction.type)
        }
        postView.reactionsDisplayView.configure(reactions: viewModel.reactions, author: currentUser)
        
        postView.gradientLayer.frame = bounds
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "postView": postView
        ]
        
        postView.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[postView]-15-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[postView]|", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}

extension FeedPostCell: ReactionSelectorDelegate {
    func didRelease(reaction: ReactionType) {
        delegate?.didReactToPost(reaction: reaction, item: self)
    }
    
    func didBeginReactionSelector() {
        //
    }
}
