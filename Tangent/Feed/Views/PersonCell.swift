//
//  PersonCell.swift
//  Tangent
//
//  Created by Daniel Burke on 4/27/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class PersonCell: UICollectionViewCell, FeedCell {
    let personContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.layer.cornerRadius = 3
        view.clipsToBounds = true
        return view
    }()
    
    let personImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 30
        imageView.backgroundColor = TAColor.lightGray.color
        return imageView
    }()
    
    let personNameLabel = TALabel(viewModel: LabelViewModel(
        text: "Personality",
        textColor: .dark,
        textStyle: .small,
        numberOfLines: 1,
        textAlignment: .center
    ))
    
    var isPressed: Bool = false {
        didSet {
            personContainerView.backgroundColor = self.isPressed ? TAColor.lightGray.color : .clear
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        personContainerView.addSubview(personImageView)
        personContainerView.addSubview(personNameLabel)
        addSubview(personContainerView)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        guard let _ = viewModel as? FeedPeopleCellViewModel else { return }
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "personContainerView": personContainerView,
            "personImageView": personImageView,
            "personNameLabel": personNameLabel
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[personContainerView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[personContainerView]-5-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[personImageView(==60)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[personNameLabel]-5-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[personNameLabel]-(<=1)-[personImageView]", options:.alignAllCenterX, metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[personImageView(==60)]-10-[personNameLabel]", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}
