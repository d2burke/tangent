//
//  FeedCollectionView.swift
//  Tangent
//
//  Created by Daniel Burke on 3/12/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class FeedCollectionView: UICollectionView {
    let layout: UICollectionViewFlowLayout = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        return layout
    }()
    
    init(frame: CGRect) {
        super.init(frame: frame, collectionViewLayout: layout)
        backgroundColor = .clear
        delaysContentTouches = false
        showsVerticalScrollIndicator = false
        contentInset = UIEdgeInsets(top: 155, left: 0, bottom: 0, right: 0)
        register(FeedPostCell.self, forCellWithReuseIdentifier: "post")
        register(FeedCommentCell.self, forCellWithReuseIdentifier: "comment")
        register(PeopleCell.self, forCellWithReuseIdentifier: "people")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
