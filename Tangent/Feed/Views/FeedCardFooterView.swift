//
//  FeedCardFooterView.swift
//  Tangent
//
//  Created by Daniel Burke on 1/2/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class FeedCardFooterView: UIView {
    
    let bookmarkButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("b", for: .normal)
        button.setTitleColor(.gray, for: .normal)
        button.titleLabel?.font = UIFont(name: "tangent", size: 18)
        return button
    }()
    
    let commentCountLabel = TALabel(viewModel: LabelViewModel(
        text: "24 Comments",
        textColor: .gray,
        textStyle: .small,
        textAlignment: .right
    ))
    
    init(){
        super.init(frame: .zero)
        addSubview(bookmarkButton)
        addSubview(commentCountLabel)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "bookmarkButton": bookmarkButton,
            "commentCountLabel": commentCountLabel
        ]
        
        for (_, view) in views {
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:[bookmarkButton(==0)]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[bookmarkButton(==0)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[commentCountLabel]-10-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[commentCountLabel]-10-|", options: [], metrics: nil, views: views),
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}
