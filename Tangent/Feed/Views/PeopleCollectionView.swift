//
//  PeopleCollectionView.swift
//  Tangent
//
//  Created by Daniel Burke on 4/27/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class PeopleCollectionView: UICollectionView {
    let layout: UICollectionViewFlowLayout = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        return layout
    }()
    
    init(frame: CGRect) {
        super.init(frame: frame, collectionViewLayout: layout)
        backgroundColor = .clear
        clipsToBounds = false
        delaysContentTouches = false
        showsHorizontalScrollIndicator = false
        register(PersonCell.self, forCellWithReuseIdentifier: "person")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
