//
//  Comment.swift
//  Tangent
//
//  Created by Daniel Burke on 2/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

struct Comment: Item {
    let text: String
    let author: User
    let dateCreated: Date
    var imageURL: String? = nil
    var comments: [Comment]? = nil
    var reactions: [Reaction]? = nil
    
    init(
        text: String = "",
        imageURL: String? = nil,
        author: User,
        dateCreated: Date = Date(),
        comments: [Comment]? = nil,
        reactions: [Reaction]? = nil
    ) {
        self.text = text
        self.author = author
        self.imageURL = imageURL
        self.dateCreated = dateCreated
        self.comments = comments
        self.reactions = reactions
    }
}
