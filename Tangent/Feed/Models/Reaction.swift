//
//  Reaction.swift
//  Tangent
//
//  Created by Daniel Burke on 2/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

struct Reaction: Equatable, Hashable, Item {
    static func ==(lhs: Reaction, rhs: Reaction) -> Bool {
        return lhs.type == rhs.type
    }

    let type: ReactionType
    let author: User
    let date: Date
    
    var hashValue: Int {
        return 0
    }
}

