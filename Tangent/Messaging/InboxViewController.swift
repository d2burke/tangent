//
//  InboxViewController.swift
//  Tangent
//
//  Created by Daniel Burke on 1/8/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class InboxViewController: UIViewController {
    
    let inboxHeaderView = PageHeaderView(viewModel: PageHeaderViewModel(
        searchFieldPlaceholderText: "Search Messages",
        searchFieldIcon: "S",
        pageTitleText: "Messages",
        headerImageViewURL: "inbox"
    ))
    
    let inboxTableView = InboxTableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inboxTableView.delegate = self
        inboxTableView.dataSource = self
        view.addSubview(inboxHeaderView)
        view.addSubview(inboxTableView)
        installConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        view.backgroundColor = TAColor.lightGray.color
        if let tabBarController = self.tabBarController as? TabBarViewController {
            tabBarController.tabBarTopBorderVisible = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "inboxHeaderView": inboxHeaderView,
            "inboxTableView": inboxTableView
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[inboxHeaderView(==155)][inboxTableView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[inboxHeaderView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[inboxTableView]|", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    func openConversation(conversation: Conversation) {
        let conversationViewController = ConversationViewController()
        self.navigationController?.pushViewController(conversationViewController, animated: true)
    }
}
