//
//  Message.swift
//  Tangent
//
//  Created by Daniel Burke on 1/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

struct Message {
    let conversationId: String
    let text: String
    let sender: User
    let sentDate: Date
    let isRead: Bool
    
    init(
        conversationId: String,
        text: String,
        sender: User,
        sentDate: Date,
        isRead: Bool
        ) {
        self.conversationId = conversationId
        self.text = text
        self.sender = sender
        self.sentDate = sentDate
        self.isRead = isRead
    }
    
    /*
    convenience init(jsonDictionary: [String: Any]) {
        
    }
    */
}
