//
//  Conversation.swift
//  Tangent
//
//  Created by Daniel Burke on 1/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

struct Conversation {
    
    let messages: [Message]
    let members: [User]
    var lastUpdatedDate = Date()
    var isRead: Bool = true
    var excerpt: String = ""
    
    init(
        messages: [Message],
        members: [User]
        ) {
        self.messages = messages
        self.members = members
        if let message = self.messages.last {
            self.isRead = message.isRead
            self.excerpt = message.text
            self.lastUpdatedDate = message.sentDate
        }
    }
}
