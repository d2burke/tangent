//
//  ConversationTableViewDataSource.swift
//  Tangent
//
//  Created by Daniel Burke on 1/22/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

protocol MessageCell {
    func configure(viewModel: IdentifiableViewModel)
}

extension ConversationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = messages[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: viewModel.identifier, for: indexPath) as! MessageCell
        cell.configure(viewModel: viewModel)
        return cell as! UITableViewCell
    }
}
