//
//  InboxTableViewDataSource.swift
//  Tangent
//
//  Created by Daniel Burke on 1/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

protocol ConversationCell {
    func configure(viewModel: IdentifiableViewModel)
}

extension InboxViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let viewModel = conversations[indexPath.row]
        return CGFloat(viewModel.cellHeight)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = conversations[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: viewModel.identifier, for: indexPath) as! ConversationCell
        cell.configure(viewModel: viewModel)
        return cell as! UITableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let conversationCellViewModel = conversations[indexPath.row] as? InboxConversationCellViewModel {
            openConversation(conversation: conversationCellViewModel.conversation)
        }
    }
}
