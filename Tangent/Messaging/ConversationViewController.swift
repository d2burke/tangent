//
//  ConversationViewController.swift
//  Tangent
//
//  Created by Daniel Burke on 1/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ConversationViewController: UIViewController {

    let conversationTableView = ConversationTableView()
    let conversationReplyView = ConversationReplyView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        conversationTableView.delegate = self
        conversationTableView.dataSource = self
        conversationTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 55, right: 0)
        view.addSubview(conversationTableView)
        view.addSubview(conversationReplyView)
        installConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        view.backgroundColor = TAColor.lightGray.color
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "conversationReplyView": conversationReplyView,
            "conversationTableView": conversationTableView
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[conversationTableView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[conversationTableView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[conversationReplyView(==55)]-49-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[conversationReplyView]|", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}
