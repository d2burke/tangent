//
//  ConversationTableView.swift
//  Tangent
//
//  Created by Daniel Burke on 1/22/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ConversationTableView: UITableView {
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: .plain)
        separatorStyle = .none
        estimatedRowHeight = 100
        backgroundColor = .clear
        rowHeight = UITableViewAutomaticDimension
        register(ConversationMessageCell.self, forCellReuseIdentifier: "message")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
