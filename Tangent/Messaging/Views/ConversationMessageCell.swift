//
//  ConversationMessageCell.swift
//  Tangent
//
//  Created by Daniel Burke on 1/22/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ConversationMessageCell: UITableViewCell, MessageCell {
    
    let messageView = ConversationMessageView()
    var viewModel:ConversationMessageCellViewModel? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        addSubview(messageView)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        let viewModel = viewModel as! ConversationMessageCellViewModel
        messageView.orientation = viewModel.isRead ? .left : .right //Check for viewModel.user -> isCurrentUser
        messageView.messageTextLabel.text = viewModel.messageText
    }
    
    func installConstraints() {
        NSLayoutConstraint.deactivate(self.constraints)
        let views: [String: UIView] = ["messageView": messageView]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[messageView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[messageView]|", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
}
