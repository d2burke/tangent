//
//  ConversationReplyView.swift
//  Tangent
//
//  Created by Daniel Burke on 1/22/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ConversationReplyView: UIView {
    
    let blurView: UIVisualEffectView = {
        let view = UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
        return view
    }()
    
    let replyContainerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 3
        view.backgroundColor = TAColor.medGray.color
        return view
    }()
    
    let replyTextField: UITextField = {
        let field = TATextField(viewModel: TATextFieldViewModel(placeholderText: "Enter a comment...", backgroundColor: .clear))
        return field
    }()
    
    let addMediaButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("+", for: .normal)
        button.setTitleColor(TAColor.gray.color, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 30)
        return button
    }()
    
    init() {
        super.init(frame: .zero)
        backgroundColor = .clear
        replyContainerView.addSubview(addMediaButton)
        replyContainerView.addSubview(replyTextField)
        blurView.addSubview(replyContainerView)
        addSubview(blurView)
        installConnstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConnstraints() {
        let views = [
            "blurView": blurView,
            "replyContainerView": replyContainerView,
            "replyTextField": replyTextField,
            "addMediaButton": addMediaButton
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[blurView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[blurView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-[replyContainerView]-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-[replyContainerView]-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[replyTextField]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[addMediaButton(==44)][replyTextField]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[addMediaButton]|", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}
