//
//  InboxConversationCell.swift
//  Tangent
//
//  Created by Daniel Burke on 1/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class InboxConversationCell: UITableViewCell, ConversationCell {
    
    let newMessageIndicator: UIView = {
        let view = UIView()
        view.backgroundColor = TAColor.green.color
        view.layer.cornerRadius = 5
        return view
    }()
    
    let memberImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 20
        imageView.backgroundColor = TAColor.lightGray.color
        return imageView
    }()
    
    let memberNamesLabel = TALabel(viewModel: LabelViewModel(textStyle: .boldParagraph))
    let excerptLabel = TALabel(viewModel: LabelViewModel(textColor: .gray, textStyle: .small))
    let lastUpdatedDateLabel = TALabel(viewModel: LabelViewModel(textColor: .gray, textStyle: .small, textAlignment: .right))
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        selectionStyle = .none
        addSubview(newMessageIndicator)
        addSubview(memberImageView)
        addSubview(memberNamesLabel)
        addSubview(excerptLabel)
        addSubview(lastUpdatedDateLabel)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        let viewModel = viewModel as! InboxConversationCellViewModel
        newMessageIndicator.isHidden = viewModel.isRead
        memberNamesLabel.text = "Heather, Damien, Daniel"
        excerptLabel.text = viewModel.excerpt
        lastUpdatedDateLabel.text = viewModel.lastUpdatedDateString
    }
    
    func installConstraints() {
        let views = [
            "newMessageIndicator": newMessageIndicator,
            "memberNamesLabel": memberNamesLabel,
            "excerptLabel": excerptLabel,
            "lastUpdatedDateLabel": lastUpdatedDateLabel,
            "memberImageView": memberImageView
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-(-5)-[newMessageIndicator(==10)]-[memberImageView(==40)]-[memberNamesLabel]-60-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-15-[newMessageIndicator]-15-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-15-[memberImageView]-15-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-15-[memberNamesLabel(==20)][excerptLabel(==20)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-60-[excerptLabel]-60-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[lastUpdatedDateLabel(==60)]-15-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-15-[lastUpdatedDateLabel(==20)]", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }

}
