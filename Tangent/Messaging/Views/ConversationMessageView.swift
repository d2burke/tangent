//
//  ConversationMessageView.swift
//  Tangent
//
//  Created by Daniel Burke on 1/22/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

enum MessageOrientation {
    case right
    case left
}

class ConversationMessageView: UIView {
    var orientation: MessageOrientation = .left {
        didSet {
            relayoutMessageBubble()
        }
    }
    
    let messageBubble: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 6
        view.layer.shadowOpacity = 0.3
        view.layer.shadowRadius = 5
        view.layer.shadowOffset = CGSize(width: 0, height: 5)
        view.layer.shadowColor = TAColor.dark.color.cgColor
        return view
    }()
    
    let messageTextLabel = TALabel(viewModel: LabelViewModel(
        textStyle: .message,
        numberOfLines: 0
    ))
    
    let arrowView = TriangleView(color: .clear)
    
    init() {
        super.init(frame: .zero)
        backgroundColor = .clear
        
        messageBubble.addSubview(messageTextLabel)
        messageBubble.addSubview(arrowView)
        addSubview(messageBubble)
        
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConstraints() {
        NSLayoutConstraint.deactivate(self.constraints)
        NSLayoutConstraint.deactivate(messageBubble.constraints)
        
        let views = ["messageBubble": messageBubble, "arrowView": arrowView, "messageTextLabel": messageTextLabel]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let arrowHorizontalConstraintString = orientation == .left ? "H:|-15-[arrowView(==20)]" : "H:[arrowView(==20)]-15-|"
        let containerHorizontalConstraintString = orientation == .left ? "H:|-15-[messageBubble]-55-|" : "H:|-55-[messageBubble]-15-|"
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: containerHorizontalConstraintString, options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[messageBubble]-30-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: arrowHorizontalConstraintString, options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[arrowView(==15)]-(-12)-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-[messageTextLabel]-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-[messageTextLabel]-|", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    func relayoutMessageBubble() {
        arrowView.direction = orientation == .left ? .left : .right
        arrowView.arrowColor = orientation == .left ? TAColor.white.color : TAColor.lightOrange.color
        messageBubble.backgroundColor = orientation == .left ? TAColor.white.color : TAColor.lightOrange.color
        messageTextLabel.textColor = orientation == .left ? TAColor.dark.color : TAColor.white.color
        installConstraints()
    }
}
