//
//  InboxTableView.swift
//  Tangent
//
//  Created by Daniel Burke on 1/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class InboxTableView: UITableView {
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: .plain)
        backgroundColor = .clear
        register(InboxConversationCell.self, forCellReuseIdentifier: "conversation")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
