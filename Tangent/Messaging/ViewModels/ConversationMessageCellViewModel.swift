//
//  ConversationMessageCellViewModel.swift
//  Tangent
//
//  Created by Daniel Burke on 1/22/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation
import UIKit

extension ConversationMessageCellViewModel: IdentifiableViewModel {
    var identifier: String { return "message" }
}

public struct ConversationMessageCellViewModel {
    let message: Message
    let messageText: String
    let sender: User
    let sentDateString: String
    let isRead: Bool
    let cellHeight: Float
    
    init(message: Message) {
        self.message = message
        messageText = message.text
        sender = message.sender
        isRead = message.isRead
        sentDateString = stringFromDate(date: message.sentDate)
        cellHeight = 100 //NOTE: This is not used due to automatically sized cells
    }
    
    
}

