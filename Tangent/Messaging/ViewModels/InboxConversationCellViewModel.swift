//
//  InboxConversationCellViewModel.swift
//  Tangent
//
//  Created by Daniel Burke on 1/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation
import UIKit

extension InboxConversationCellViewModel: IdentifiableViewModel {
    var identifier: String { return "conversation" }
    var cellHeight: Float { return 70 }
}

public struct InboxConversationCellViewModel {
    let conversation: Conversation
    let excerpt: String
    let members: [User]
    let lastUpdatedDateString: String
    let isRead: Bool
    
    init(conversation: Conversation) {
        self.conversation = conversation
        excerpt = "This is an excerpt of a message" //conversation.excerpt
        members = conversation.members
        isRead = false //conversation.isRead
        lastUpdatedDateString = stringFromDate(date: conversation.lastUpdatedDate)
    }
}
