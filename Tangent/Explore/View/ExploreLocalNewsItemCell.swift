//
//  ExploreLocalNewsItemCell.swift
//  Tangent
//
//  Created by Daniel Burke on 3/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ExploreLocalNewsItemCell: UICollectionViewCell, ExploreCell {
    
    var isPressed: Bool = false {
        didSet {
            backgroundColor = self.isPressed ? TAColor.lightGray.color : .clear
        }
    }
    
    let roundedContainer: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 6
        view.clipsToBounds = true
        return view
    }()
    
    let cardContainer: UIView = {
        let view = UIView()
        view.backgroundColor = TAColor.white.color
        view.layer.cornerRadius = 6
        view.layer.shadowOpacity = 0
        view.layer.shadowRadius = 7
        view.layer.shadowOffset = CGSize(width: 0, height: 7)
        view.layer.shadowColor = TAColor.dark.color.cgColor
        return view
    }()
    
    lazy var imageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = false
        return imageView
    }()
    
    let newsTitleLabel = TALabel(viewModel: LabelViewModel(
        text: "These are the words for the article card in Explore",
        numberOfLines: 0
    ))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        layer.cornerRadius = 6
        roundedContainer.addSubview(imageView)
        cardContainer.addSubview(roundedContainer)
        addSubview(cardContainer)
        addSubview(newsTitleLabel)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        guard let cellModel = viewModel as? ExploreLocalNewsItemCellViewModel else { return }
        newsTitleLabel.text = cellModel.title
        newsTitleLabel.sizeToFit()
        imageView.image = UIImage(named: cellModel.imageURL)
        cardContainer.isHidden = cellModel.imageURL.isEmpty
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "imageView": imageView,
            "cardContainer": cardContainer,
            "roundedContainer": roundedContainer,
            "newsTitleLabel": newsTitleLabel
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[imageView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[imageView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[cardContainer]-5-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[newsTitleLabel]-5-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[cardContainer(==100)]-5-[newsTitleLabel]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[roundedContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[roundedContainer]|", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
}
