//
//  ExploreCollectionView.swift
//  Tangent
//
//  Created by Daniel Burke on 3/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

protocol ExploreCell {
    var isPressed: Bool { get set }
    func configure(viewModel: IdentifiableViewModel)
}

class ExploreCollectionView: UICollectionView {
    
    /**
     *  TrendingTopics CollectionViewCell
     *  LocalNews CollectionCell
     *  Categories CollectionViewCell //Could be "top", "interested", or "suggested" based on activity or balance need
     *
     */
    
    //TODO: Get sections from server based on current user
    let sections: [ExploreCollectionCellViewModel] = [
        ExploreLocalNewsCellViewModel(articles: exploreArticles),
        ExploreTrendingCellViewModel(items: trendingPosts),
        ExploreCategoriesCellViewModel(categories: exploreCategories)
    ]
    
    let layout: UICollectionViewFlowLayout = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.headerReferenceSize = CGSize(width: 300, height: 40)
        return layout
    }()
    
    init(frame: CGRect) {
        super.init(frame: frame, collectionViewLayout: layout)
        backgroundColor = .clear
        showsVerticalScrollIndicator = false
        delaysContentTouches = false
        contentInset = UIEdgeInsets(top: 155, left: 0, bottom: 0, right: 0)
        register(ExploreCollectionViewSectionHeaderView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "sectionHeader")
        register(ExploreTrendingCell.self, forCellWithReuseIdentifier: "trending")
        register(ExploreLocalNewsCell.self, forCellWithReuseIdentifier: "localNews")
        register(ExploreCategoriesCell.self, forCellWithReuseIdentifier: "categories")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
