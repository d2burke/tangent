//
//  ExploreLocalNewsCell.swift
//  Tangent
//
//  Created by Daniel Burke on 3/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ExploreLocalNewsCell: UICollectionViewCell, ExploreCell {
    let exploreLocalNewsCollectionView = ExploreLocalNewsCollectionView(frame: .zero)
    var articles = [ExploreLocalNewsItemCellViewModel]()
    
    var isPressed: Bool = false {
        didSet {
            //
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        exploreLocalNewsCollectionView.clipsToBounds = false
        exploreLocalNewsCollectionView.delegate = self
        exploreLocalNewsCollectionView.dataSource = self
        addSubview(exploreLocalNewsCollectionView)
        
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        guard let cellModel = viewModel as? ExploreLocalNewsCellViewModel else { return }
        articles = cellModel.articles
        exploreLocalNewsCollectionView.reloadData()
    }
    
    func installConstraints() {
        let views: [String: UIView] = ["exploreLocalNewsCollectionView": exploreLocalNewsCollectionView]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[exploreLocalNewsCollectionView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[exploreLocalNewsCollectionView]|", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}

extension ExploreLocalNewsCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return articles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        var cell = collectionView.cellForItem(at: indexPath) as! ExploreCell
        cell.isPressed = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        var cell = collectionView.cellForItem(at: indexPath) as! ExploreCell
        cell.isPressed = false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2+10, height: collectionView.frame.size.height-10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellModel = articles[indexPath.row] as ExploreLocalNewsItemCellViewModel
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "localNewsItem", for: indexPath) as! ExploreCell
        cell.configure(viewModel: cellModel)
        return cell as! UICollectionViewCell
    }
}
