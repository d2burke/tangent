//
//  ExploreTrendingCell.swift
//  Tangent
//
//  Created by Daniel Burke on 3/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ExploreTrendingCell: UICollectionViewCell, ExploreCell {
    let exploreTrendingCollectionView = ExploreTrendingCollectionView(frame: .zero)
    var posts = [ExploreTrendingItemCellViewModel]()
    
    var isPressed: Bool = false {
        didSet {
            //
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        exploreTrendingCollectionView.clipsToBounds = false
        exploreTrendingCollectionView.delegate = self
        exploreTrendingCollectionView.dataSource = self
        addSubview(exploreTrendingCollectionView)
        
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        guard let cellModel = viewModel as? ExploreTrendingCellViewModel else { return }
        posts = cellModel.items
        exploreTrendingCollectionView.reloadData()
    }
    
    func installConstraints() {
        let views: [String: UIView] = ["exploreTrendingCollectionView": exploreTrendingCollectionView]
        exploreTrendingCollectionView.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[exploreTrendingCollectionView]-10-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[exploreTrendingCollectionView]|", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}

extension ExploreTrendingCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        var cell = collectionView.cellForItem(at: indexPath) as! ExploreCell
        cell.isPressed = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        var cell = collectionView.cellForItem(at: indexPath) as! ExploreCell
        cell.isPressed = false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellModel = posts[indexPath.row] as ExploreTrendingItemCellViewModel
        return CGSize(width: collectionView.frame.size.width-20, height: CGFloat(cellModel.cellHeight))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellModel = posts[indexPath.row] as ExploreTrendingItemCellViewModel
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "trendingPost", for: indexPath) as! ExploreCell
        cell.configure(viewModel: cellModel)
        return cell as! UICollectionViewCell
    }
}
