//
//  ExploreCategoriesCollectionView.swift
//  Tangent
//
//  Created by Daniel Burke on 3/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ExploreCategoriesCollectionView: UICollectionView {
    let layout: UICollectionViewFlowLayout = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        return layout
    }()
    
    init(frame: CGRect) {
        super.init(frame: frame, collectionViewLayout: layout)
        backgroundColor = .clear
        showsHorizontalScrollIndicator = false
        delaysContentTouches = false
        register(ExploreCategoriesItemCell.self, forCellWithReuseIdentifier: "categoriesItem")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
