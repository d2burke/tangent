//
//  ExploreTrendingItemCell.swift
//  Tangent
//
//  Created by Daniel Burke on 3/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ExploreTrendingItemCell: UICollectionViewCell, ExploreCell {
    var isPressed: Bool = false {
        didSet {
            let shadowHeight: CGFloat = self.isPressed ? 2 : 7
            self.postView.cardContainer.layer.shadowOpacity = self.isPressed ? 0.1 : 0.2
            self.postView.cardContainer.layer.shadowRadius = self.isPressed ? 2 : 7
            self.postView.cardContainer.layer.shadowOffset = CGSize(width: 0, height: shadowHeight)
        }
    }
    
    let postView = PostView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        postView.reactionSelector.delegate = self
        addSubview(postView)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        guard let cellModel = viewModel as? ExploreTrendingItemCellViewModel else { return }
        //TODO: Change to configure() with view model
        postView.topicLabel.text = cellModel.text
        postView.topicImageView.image = UIImage(named: cellModel.imageURL)
        let currentUser = User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: "http://d2burke.com/img/portrait.jpg")
        if let reactions = cellModel.reactions {
            postView.reactionsDisplayView.configure(reactions: reactions, author: currentUser)
        }
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "postView": postView
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[postView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[postView]-15-|", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}



extension ExploreTrendingItemCell: ReactionSelectorDelegate {
    func didRelease(reaction: ReactionType) {
        //        switch reaction {
        //        case .agree:
        //            print("Agree")
        //        case .disagree:
        //            print("Disagree")
        //        case .goodPoint:
        //            print("Good Point")
        //        case .offensive:
        //            print("Offensive")
        //        case .apathetic:
        //            print("Meh")
        //        case .none: ()
        //        }
    }
    
    func didBeginReactionSelector() {
        //
    }
}
