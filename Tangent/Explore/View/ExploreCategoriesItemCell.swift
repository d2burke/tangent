//
//  ExploreCategoriesItemCell.swift
//  Tangent
//
//  Created by Daniel Burke on 3/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ExploreCategoriesItemCell: UICollectionViewCell {
    
    var isPressed: Bool = false {
        didSet {
            categoryLabel.alpha = isPressed ? 0.8 : 1
        }
    }
    
    let roundedContainer: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 6
        view.clipsToBounds = true
        return view
    }()
    
    let cardContainer: UIView = {
        let view = UIView()
        view.backgroundColor = TAColor.white.color
        view.layer.cornerRadius = 6
        return view
    }()
    
    let categoryLabel = TALabel(viewModel: LabelViewModel(
        textColor: .white,
        textStyle: .boldSmall,
        textAlignment: .center
    ))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        roundedContainer.addSubview(categoryLabel)
        cardContainer.addSubview(roundedContainer)
        addSubview(cardContainer)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: ExploreCategoriesItemCellViewModel) {
        categoryLabel.text = viewModel.categoryName
        categoryLabel.backgroundColor = viewModel.color.color
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "categoryLabel": categoryLabel,
            "cardContainer": cardContainer,
            "roundedContainer": roundedContainer
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[categoryLabel]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[categoryLabel]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[cardContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[cardContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[roundedContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[roundedContainer]|", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}
