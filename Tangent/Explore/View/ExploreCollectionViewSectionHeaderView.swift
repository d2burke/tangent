//
//  ExploreCollectionViewSectionHeaderView.swift
//  Tangent
//
//  Created by Daniel Burke on 3/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ExploreCollectionViewSectionHeaderView: UICollectionReusableView {
    let headerTitleLabel = TALabel(viewModel:LabelViewModel(textStyle: .boldParagraph))
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(headerTitleLabel)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConstraints() {
        let views: [String: UIView] = ["headerTitleLabel": headerTitleLabel]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[headerTitleLabel]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[headerTitleLabel]|", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    func configureWithViewModel() {//viewModel: ExploreCollectionViewSectionHeaderViewModel) {
        
    }
}
