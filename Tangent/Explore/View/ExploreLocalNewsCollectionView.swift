//
//  ExploreLocalNewsCollectionView.swift
//  Tangent
//
//  Created by Daniel Burke on 3/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ExploreLocalNewsCollectionView: UICollectionView {
    let layout: UICollectionViewFlowLayout = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        return layout
    }()
    
    init(frame: CGRect) {
        super.init(frame: frame, collectionViewLayout: layout)
        backgroundColor = .clear
        showsHorizontalScrollIndicator = false
        delaysContentTouches = false
        contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 15)
        register(ExploreLocalNewsItemCell.self, forCellWithReuseIdentifier: "localNewsItem")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
