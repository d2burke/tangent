//
//  ExploreCategoriesCell.swift
//  Tangent
//
//  Created by Daniel Burke on 3/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ExploreCategoriesCell: UICollectionViewCell, ExploreCell {
    let exploreCategoriesCollectionView = ExploreCategoriesCollectionView(frame: .zero)
    
    var isPressed: Bool = false {
        didSet {
            //
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        exploreCategoriesCollectionView.clipsToBounds = false
        exploreCategoriesCollectionView.delegate = self
        exploreCategoriesCollectionView.dataSource = self
        addSubview(exploreCategoriesCollectionView)
        
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        guard viewModel is ExploreCategoriesCellViewModel else { return }
    }
    
    func installConstraints() {
        let views: [String: UIView] = ["exploreCategoriesCollectionView": exploreCategoriesCollectionView]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[exploreCategoriesCollectionView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[exploreCategoriesCollectionView]-15-|", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
}

extension ExploreCategoriesCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoriesItem", for: indexPath) as! ExploreCategoriesItemCell
        cell.isPressed = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoriesItem", for: indexPath) as! ExploreCategoriesItemCell
        cell.isPressed = false
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return exploreCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/3, height: collectionView.frame.size.height-10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellViewModel = exploreCategories[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoriesItem", for: indexPath) as! ExploreCategoriesItemCell
        cell.configure(viewModel: cellViewModel)
        return cell
    }
}

