//
//  ExploreTrendingCollectionView.swift
//  Tangent
//
//  Created by Daniel Burke on 3/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ExploreTrendingCollectionView: UICollectionView {
    let layout: UICollectionViewFlowLayout = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        return layout
    }()
    
    init(frame: CGRect) {
        super.init(frame: frame, collectionViewLayout: layout)
        backgroundColor = .clear
        clipsToBounds = false
        delaysContentTouches = false
        showsHorizontalScrollIndicator = false
        register(ExploreTrendingItemCell.self, forCellWithReuseIdentifier: "trendingPost")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
