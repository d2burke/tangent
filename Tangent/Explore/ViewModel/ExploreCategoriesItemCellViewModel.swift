//
//  ExploreCategoriesItemCellViewModel.swift
//  Tangent
//
//  Created by Daniel Burke on 3/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

struct ExploreCategoriesItemCellViewModel {
    let categoryName:String
    let color: TAColor
    
    init(name: String, color: TAColor = .white) {
        self.categoryName = name
        self.color = color
    }
}
