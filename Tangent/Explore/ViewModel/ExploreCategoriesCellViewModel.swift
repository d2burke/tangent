//
//  ExploreCategoriesCellViewModel.swift
//  Tangent
//
//  Created by Daniel Burke on 3/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

extension ExploreCategoriesCellViewModel: ExploreCollectionCellViewModel {
    var identifier: String { return "categories" }
    var cellHeight: Float { return 50.0 }
}

public struct ExploreCategoriesCellViewModel {
    let title = "Suggested Categories"
    let categories: [ExploreCategoriesItemCellViewModel]
    
    init(categories: [ExploreCategoriesItemCellViewModel]) {
        self.categories = categories
    }
}
