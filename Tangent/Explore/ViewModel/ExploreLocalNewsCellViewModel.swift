//
//  ExploreLocalNewsCellViewModel.swift
//  Tangent
//
//  Created by Daniel Burke on 3/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

extension ExploreLocalNewsCellViewModel: ExploreCollectionCellViewModel {
    var identifier: String { return "localNews" }
    var cellHeight: Float { return 220.0 }
}

public struct ExploreLocalNewsCellViewModel {
    let title = "Local News & Opinion"
    let articles: [ExploreLocalNewsItemCellViewModel]
    
    init(articles: [ExploreLocalNewsItemCellViewModel]) {
        self.articles = articles
    }
}
