//
//  ExploreTrendingCellViewModel.swift
//  Tangent
//
//  Created by Daniel Burke on 3/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

extension ExploreTrendingCellViewModel: ExploreCollectionCellViewModel {
    var identifier: String { return "trending" }
    var cellHeight: Float { return 330.0 }
}

public struct ExploreTrendingCellViewModel {
    let title = "Trending Posts"
    let items: [ExploreTrendingItemCellViewModel]
    
    init(items: [ExploreTrendingItemCellViewModel]) {
        self.items = items
    }
}
