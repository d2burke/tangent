//
//  ExploreTrendingItemCellViewModel.swift
//  Tangent
//
//  Created by Daniel Burke on 3/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

extension ExploreTrendingItemCellViewModel: IdentifiableViewModel {
    var identifier: String { return "trendingPost" }
    var cellHeight: Float { return 300 }
}

struct ExploreTrendingItemCellViewModel {
    let text: String
    let imageURL: String
    let reaction: String
    var reactions: [Reaction]?
    
    init(
        text: String = "",
        imageURL: String = "",
        reaction: String = "none",
        reactions: [Reaction]? = nil
        ) {
        self.text = text
        self.imageURL = imageURL
        self.reaction = reaction
        self.reactions = reactions
    }
}
