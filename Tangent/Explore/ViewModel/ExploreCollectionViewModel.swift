//
//  ExploreCollectionViewModel.swift
//  Tangent
//
//  Created by Daniel Burke on 3/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

//Sections
//  Section Titles
//  Section ViewModels

public struct ExploreCollectionViewModel {
    let sections: [ExploreCollectionCellViewModel]
    
    init(sections: [ExploreCollectionCellViewModel]) {
        self.sections = sections
    }
}
