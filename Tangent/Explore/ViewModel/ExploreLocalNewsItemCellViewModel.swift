//
//  ExploreLocalNewsItemCellViewModel.swift
//  Tangent
//
//  Created by Daniel Burke on 3/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

extension ExploreLocalNewsItemCellViewModel: IdentifiableViewModel {
    var identifier: String { return "localNewsItem" }
    var cellHeight: Float { return 320 }
}

struct ExploreLocalNewsItemCellViewModel {
    let title: String
    let imageURL: String
    let dateString: String
    
    init(title: String, imageURL: String, dateString: String) {
        self.title = title
        self.imageURL = imageURL
        self.dateString = dateString
    }
    
    
    //cell height
    //new item info
}
