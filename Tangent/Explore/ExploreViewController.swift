//
//  ExploreViewController.swift
//  Tangent
//
//  Created by Daniel Burke on 1/8/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ExploreViewController: UIViewController {
    var headerTopConstraint = [NSLayoutConstraint]()
    let pageHeaderView = PageHeaderView(viewModel: PageHeaderViewModel(
        searchFieldPlaceholderText: "Search Tangent",
        searchFieldIcon: "S",
        pageTitleText: "Explore",
        headerImageViewURL: "explore"
    ))
    
    let exploreCollectionView = ExploreCollectionView(frame: .zero)

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        
        exploreCollectionView.dataSource = self
        exploreCollectionView.delegate = self
        view.addSubview(exploreCollectionView)
        view.addSubview(pageHeaderView)
        installConstraints()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "pageHeaderView": pageHeaderView,
            "exploreCollectionView": exploreCollectionView
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        headerTopConstraint = NSLayoutConstraint.constraints(withVisualFormat: "V:|[pageHeaderView]", options: [], metrics: nil, views: views)
        let constraints = [
            headerTopConstraint,
            NSLayoutConstraint.constraints(withVisualFormat: "V:[pageHeaderView(==155)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[pageHeaderView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[exploreCollectionView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[exploreCollectionView]|", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}


extension ExploreViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = scrollView.contentOffset.y
        let headerTopPosition = y < -155 ? 0 : -155 - y
        headerTopConstraint.first?.constant = headerTopPosition
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 15)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return exploreCollectionView.sections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellViewModel = exploreCollectionView.sections[indexPath.section] as ExploreCollectionCellViewModel
        return CGSize(width: collectionView.frame.size.width, height: CGFloat(cellViewModel.cellHeight))
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier:"sectionHeader", for: indexPath) as! ExploreCollectionViewSectionHeaderView
        let cellViewModel = exploreCollectionView.sections[indexPath.section] as ExploreCollectionCellViewModel
        
        header.headerTitleLabel.textColor = TAColor.orange.color
        if indexPath.section == 0 {
            header.headerTitleLabel.attributedText = append(city: "Virginia Beach", toTitle: cellViewModel.title)
        } else {
            header.headerTitleLabel.text = cellViewModel.title
        }
        return header

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellViewModel = exploreCollectionView.sections[indexPath.section] as ExploreCollectionCellViewModel
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellViewModel.identifier, for: indexPath) as! ExploreCell
        cell.configure(viewModel: cellViewModel)
        return cell as! UICollectionViewCell
    }
}

extension ExploreViewController {
    func append(city: String, toTitle title: String) -> NSAttributedString {
        let appendedStr = " • \(city)"
        let appendedStrLen = appendedStr.characters.count
        let startLoc = title.characters.count
        let attr = [NSForegroundColorAttributeName: TAColor.gray.color]
        let attrString = NSMutableAttributedString(string: "\(title)\(appendedStr)")
        attrString.addAttributes(attr, range: NSMakeRange(startLoc, appendedStrLen))
        return attrString
    }
}
