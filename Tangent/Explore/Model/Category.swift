//
//  Category.swift
//  Tangent
//
//  Created by Daniel Burke on 3/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

struct Category {
    let name: String
    let score: Float
    
    init(name: String, score: Float = 0.0) {
        self.name = name
        self.score = score
    }
}
