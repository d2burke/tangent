//
//  Article.swift
//  Tangent
//
//  Created by Daniel Burke on 3/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

struct Article {
    let author: String
    let title: String
    let description: String
    let url: String
    let imageURL: String
    let publishedDate: Date //2017-03-09T19:43:35Z
    
    init(author: String, title: String, description: String, url: String, imageURL: String, publishedDate: String) {
        self.author = author
        self.title = title
        self.description = description
        self.url = url
        self.imageURL = imageURL
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from:publishedDate)!
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day, .hour], from: date)
        if let date = calendar.date(from:components) {
            self.publishedDate = date
        } else {
            self.publishedDate = Date()
        }
    }
    
    //TODO: init from JSON
    /*
    init(json: [String: Any]) {
        
    }
    */
}
