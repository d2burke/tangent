//
//  DataSource.swift
//  Tangent
//
//  Created by Daniel Burke on 1/8/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

var onboarding: [IdentifiableViewModel] = [
    OnboardingReactionCellModel(text: "The death penalty still has a place in our society", imageURL: "death-penalty"),
    OnboardingReactionCellModel(text: "Trump's taxes should be supoenaed in House Intel investigation", imageURL: "tweeting"),
    OnboardingReactionCellModel(text: "Snoop Dogg's anti Trump video went too far", imageURL: "snoop"),
    OnboardingInterestsCellModel(interests: [
        InterestCellModel(interest: Interest(name: "Politics")),
        InterestCellModel(interest: Interest(name: "Technology")),
        InterestCellModel(interest: Interest(name: "Education")),
        InterestCellModel(interest: Interest(name: "Start-ups")),
        InterestCellModel(interest: Interest(name: "Travel")),
        InterestCellModel(interest: Interest(name: "World")),
        InterestCellModel(interest: Interest(name: "Entertainment")),
        InterestCellModel(interest: Interest(name: "Science")),
        InterestCellModel(interest: Interest(name: "Photography")),
        InterestCellModel(interest: Interest(name: "Health")),
        InterestCellModel(interest: Interest(name: "Music")),
        InterestCellModel(interest: Interest(name: "Fitness")),
        InterestCellModel(interest: Interest(name: "Business")),
        InterestCellModel(interest: Interest(name: "Automotive")),
        InterestCellModel(interest: Interest(name: "Science")),
        InterestCellModel(interest: Interest(name: "Photography")),
        InterestCellModel(interest: Interest(name: "Health")),
        InterestCellModel(interest: Interest(name: "Music")),
        InterestCellModel(interest: Interest(name: "Fitness")),
        InterestCellModel(interest: Interest(name: "Business")),
        InterestCellModel(interest: Interest(name: "Automotive")),
        InterestCellModel(interest: Interest(name: "Science")),
        InterestCellModel(interest: Interest(name: "Photography")),
        InterestCellModel(interest: Interest(name: "Health")),
        InterestCellModel(interest: Interest(name: "Music")),
        InterestCellModel(interest: Interest(name: "Fitness")),
        InterestCellModel(interest: Interest(name: "Business")),
        InterestCellModel(interest: Interest(name: "Automotive"))
        ]),
    OnboardingReactionCellModel(text: "The death penalty still has a place in our society", imageURL: "death-penalty"),
    OnboardingReactionCellModel(text: "Trump's taxes should be supoenaed in House Intel investigation", imageURL: "tweeting"),
    OnboardingReactionCellModel(text: "Snoop Dogg's anti Trump video went too far", imageURL: "snoop"),
    OnboardingLocationCellModel(),
    OnboardingReactionCellModel(text: "The death penalty still has a place in our society", imageURL: "death-penalty"),
    OnboardingReactionCellModel(text: "Trump's taxes should be supoenaed in House Intel investigation", imageURL: "tweeting"),
    OnboardingReactionCellModel(text: "Snoop Dogg's anti Trump video went too far", imageURL: "snoop"),
]

var trendingPosts: [ExploreTrendingItemCellViewModel] = [
    ExploreTrendingItemCellViewModel(text: "Sexism keeps women from being sports coaches", imageURL: "sexism", reaction: "none", reactions: [
        Reaction(type: .agree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .agree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .disagree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .goodPoint, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .agree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .goodPoint, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .goodPoint, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .agree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .disagree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .disagree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .goodPoint, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .agree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date())
    ]),
    ExploreTrendingItemCellViewModel(text: "Removing FCC Rules Undermines Online Privacy", imageURL: "privacy", reaction: "none"),
    ExploreTrendingItemCellViewModel(text: "Ed Sheeran's record-breaking week", imageURL: "ed", reaction: "none"),
    ExploreTrendingItemCellViewModel(text: "1984 was the NBA's best draft class", imageURL: "draft", reactions: [
        Reaction(type: .agree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .agree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .disagree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .goodPoint, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .agree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .goodPoint, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .goodPoint, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .agree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .disagree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .disagree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .goodPoint, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date()),
        Reaction(type: .agree, author: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: ""), date: Date())
    ]),
    ExploreTrendingItemCellViewModel(text: "Transgender people have a right to safe toilets", imageURL: "bathrooms", reaction: "none"),
    ExploreTrendingItemCellViewModel(text: "Paul Ryan should resign over health care bill handling", imageURL: "ryan", reaction: "none")
]

var exploreArticles = [
    ExploreLocalNewsItemCellViewModel(
        title: "Residents worry historic black community in Virginia Beach is disappearing",
        imageURL: "seatac",
        dateString: "at 3:23pm"
    ),
    ExploreLocalNewsItemCellViewModel(
        title: "Virginia Beach leaders want to build a $40 million sports field house at the Oceanfront",
        imageURL: "fieldhouse",
        dateString: "at 3:23pm"
    ),
    ExploreLocalNewsItemCellViewModel(
        title: "Appeals court decision could help Wesley Hadsell overturn 20-year prison sentence",
        imageURL: "",
        dateString: "at 3:23pm"
    ),
    ExploreLocalNewsItemCellViewModel(
        title: "Discussing the limits of artificial intelligence",
        imageURL: "ai",
        dateString: "at 3:23pm"
    ),
    ExploreLocalNewsItemCellViewModel(
        title: "Robinhood stock trading app valued at $1.3 billion with big raise from DST",
        imageURL: "gold",
        dateString: "at 3:23pm"
    )
]

var articles: [Article] = [
    Article(
        author: "Napier Lopez",
        title: "Google takes on Slack with new Hangouts 'Chat' and 'Meet' apps",
        description: "Ever since Google launched Allo and Duo, we’ve known that Hangouts would be making the transition from a consumer-focused product to a business one. That change begins today with the announcement of Hangouts Chat and Hangouts Meet. While the standard Hangouts app will stick around for a while, it’s essentially being replaced by Chat and Meet. …",
        url: "https://thenextweb.com/google/2017/03/09/google-takes-slack-new-hangouts-chat-meet-apps/",
        imageURL: "https://cdn2.tnwcdn.com/wp-content/blogs.dir/1/files/2017/03/d-inscreen-01-room_1-1.png",
        publishedDate: "2017-03-09T19:43:35Z"
    ),
    Article(
        author: "Napier Lopez",
        title: "Google takes on Slack with new Hangouts 'Chat' and 'Meet' apps",
        description: "Ever since Google launched Allo and Duo, we’ve known that Hangouts would be making the transition from a consumer-focused product to a business one. That change begins today with the announcement of Hangouts Chat and Hangouts Meet. While the standard Hangouts app will stick around for a while, it’s essentially being replaced by Chat and Meet. …",
        url: "https://thenextweb.com/google/2017/03/09/google-takes-slack-new-hangouts-chat-meet-apps/",
        imageURL: "https://cdn2.tnwcdn.com/wp-content/blogs.dir/1/files/2017/03/d-inscreen-01-room_1-1.png",
        publishedDate: "2017-03-09T19:43:35Z"
    ),
    Article(
        author: "Napier Lopez",
        title: "Google takes on Slack with new Hangouts 'Chat' and 'Meet' apps",
        description: "Ever since Google launched Allo and Duo, we’ve known that Hangouts would be making the transition from a consumer-focused product to a business one. That change begins today with the announcement of Hangouts Chat and Hangouts Meet. While the standard Hangouts app will stick around for a while, it’s essentially being replaced by Chat and Meet. …",
        url: "https://thenextweb.com/google/2017/03/09/google-takes-slack-new-hangouts-chat-meet-apps/",
        imageURL: "https://cdn2.tnwcdn.com/wp-content/blogs.dir/1/files/2017/03/d-inscreen-01-room_1-1.png",
        publishedDate: "2017-03-09T19:43:35Z"
    ),
    Article(
        author: "Napier Lopez",
        title: "Google takes on Slack with new Hangouts 'Chat' and 'Meet' apps",
        description: "Ever since Google launched Allo and Duo, we’ve known that Hangouts would be making the transition from a consumer-focused product to a business one. That change begins today with the announcement of Hangouts Chat and Hangouts Meet. While the standard Hangouts app will stick around for a while, it’s essentially being replaced by Chat and Meet. …",
        url: "https://thenextweb.com/google/2017/03/09/google-takes-slack-new-hangouts-chat-meet-apps/",
        imageURL: "https://cdn2.tnwcdn.com/wp-content/blogs.dir/1/files/2017/03/d-inscreen-01-room_1-1.png",
        publishedDate: "2017-03-09T19:43:35Z"
    ),
    Article(
        author: "Napier Lopez",
        title: "Google takes on Slack with new Hangouts 'Chat' and 'Meet' apps",
        description: "Ever since Google launched Allo and Duo, we’ve known that Hangouts would be making the transition from a consumer-focused product to a business one. That change begins today with the announcement of Hangouts Chat and Hangouts Meet. While the standard Hangouts app will stick around for a while, it’s essentially being replaced by Chat and Meet. …",
        url: "https://thenextweb.com/google/2017/03/09/google-takes-slack-new-hangouts-chat-meet-apps/",
        imageURL: "https://cdn2.tnwcdn.com/wp-content/blogs.dir/1/files/2017/03/d-inscreen-01-room_1-1.png",
        publishedDate: "2017-03-09T19:43:35Z"
    )
]

var exploreCategories = [
    ExploreCategoriesItemCellViewModel(name: "Technology", color: .blue),
    ExploreCategoriesItemCellViewModel(name: "Education", color: .teal),
    ExploreCategoriesItemCellViewModel(name: "Start-ups", color: .green),
    ExploreCategoriesItemCellViewModel(name: "Politics", color: .lightOrange),
    ExploreCategoriesItemCellViewModel(name: "Travel", color: .red)
]

var topics: [IdentifiableViewModel] = [FeedPeopleCellViewModel()]

var comments: [IdentifiableViewModel] = [
    PostCommentCellViewModel(text: "This is a comment about a post", imageURL: "tweeting"),
    PostCommentCellViewModel(text: "For example, let’s say you have a header label that is configured one way vs a subhead label (configured with a smaller font and lighter color) across the app", imageURL: "tweeting"),
    PostCommentCellViewModel(text: "This is a comment about a post", imageURL: "tweeting"),
    PostCommentCellViewModel(text: "This is a comment about a post", imageURL: "tweeting"),
    PostCommentCellViewModel(text: "This is a comment about a post", imageURL: "tweeting"),
    PostCommentCellViewModel(text: "This is a comment about a post", imageURL: "tweeting"),
    PostCommentCellViewModel(text: "This is a comment about a post", imageURL: "tweeting"),
    PostCommentCellViewModel(text: "This is a comment about a post", imageURL: "tweeting")
]

var conversations: [IdentifiableViewModel] = [
    InboxConversationCellViewModel(conversation: Conversation(
        messages: [Message](),
        members: [User]()
    )),
    InboxConversationCellViewModel(conversation: Conversation(
        messages: [Message](),
        members: [User]()
    )),
    InboxConversationCellViewModel(conversation: Conversation(
        messages: [Message](),
        members: [User]()
    )),
    InboxConversationCellViewModel(conversation: Conversation(
        messages: [Message](),
        members: [User]()
    )),
    InboxConversationCellViewModel(conversation: Conversation(
        messages: [Message](),
        members: [User]()
    ))
]

var messages: [IdentifiableViewModel] = [
    ConversationMessageCellViewModel(message: Message(
        conversationId: "a8UioOOj8nqpx",
        text: "Lorem Khaled Ipsum is a major key to success. You see the hedges, how I got it shaped up? It’s important to shape up your hedges, it’s like getting a haircut, stay fresh",
        sender: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: "http://d2burke.com/img/portrait.jpg"),
        sentDate: Date(),
        isRead: false
    )),
    ConversationMessageCellViewModel(message: Message(
        conversationId: "a8UioOOj8nqpx",
        text: "The key to success is to keep your head above the water, never give up. You do know, you do know that they don’t want you to have lunch.",
        sender: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: "http://d2burke.com/img/portrait.jpg"),
        sentDate: Date(),
        isRead: true
    )),
    ConversationMessageCellViewModel(message: Message(
        conversationId: "a8UioOOj8nqpx",
        text: "I’m keeping it real with you, so what you going do is have lunch.",
        sender: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: "http://d2burke.com/img/portrait.jpg"),
        sentDate: Date(),
        isRead: false
    )),
    ConversationMessageCellViewModel(message: Message(
        conversationId: "a8UioOOj8nqpx",
        text: "They don’t want us to win. The key to success is to keep your head above the water, never give up. \n\nLook at the sunset, life is amazing, life is beautiful, life is what you make it.",
        sender: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: "http://d2burke.com/img/portrait.jpg"),
        sentDate: Date(),
        isRead: true
    )),
    ConversationMessageCellViewModel(message: Message(
        conversationId: "a8UioOOj8nqpx",
        text: "Cloth talk.",
        sender: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: "http://d2burke.com/img/portrait.jpg"),
        sentDate: Date(),
        isRead: false
    )),
    ConversationMessageCellViewModel(message: Message(
        conversationId: "a8UioOOj8nqpx",
        text: "Find peace, life is like a water fall, you’ve gotta flow. You do know, you do know that they don’t want you to have lunch. I’m keeping it real with you, so what you going do is have lunch. They never said winning was easy. Some people can’t handle success, I can. A major key, never panic. Don’t panic, when it gets crazy and rough, don’t panic, stay calm.",
        sender: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: "http://d2burke.com/img/portrait.jpg"),
        sentDate: Date(),
        isRead: false
    )),
    ConversationMessageCellViewModel(message: Message(
        conversationId: "a8UioOOj8nqpx",
        text: "The weather is amazing, walk with me through the pathway of more success. Take this journey with me, Lion!",
        sender: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: "http://d2burke.com/img/portrait.jpg"),
        sentDate: Date(),
        isRead: true
    )),
    ConversationMessageCellViewModel(message: Message(
        conversationId: "a8UioOOj8nqpx",
        text: "They key is to have every key, the key to open every door. Special cloth alert. The other day the grass was brown, now it’s green because I ain’t give up.",
        sender: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: "http://d2burke.com/img/portrait.jpg"),
        sentDate: Date(),
        isRead: false
    )),
    ConversationMessageCellViewModel(message: Message(
        conversationId: "a8UioOOj8nqpx",
        text: "Never surrender.",
        sender: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: "http://d2burke.com/img/portrait.jpg"),
        sentDate: Date(),
        isRead: true
    )),
    ConversationMessageCellViewModel(message: Message(
        conversationId: "a8UioOOj8nqpx",
        text: "Learning is cool, but knowing is better, and I know the key to success. I told you all this before, when you have a swimming pool, do not use chlorine, use salt water, the healing, salt water is the healing. You should never complain, complaining is a weak emotion, you got life, we breathing, we blessed.",
        sender: User(firstName: "Daniel", lastName: "Burke", userName: "d2burke", avatarURL: "http://d2burke.com/img/portrait.jpg"),
        sentDate: Date(),
        isRead: true
    ))
]

var profileTabs: [IdentifiableViewModel] = [
    ProfileActivityCellModel(items: profileActivityItems),
    //    ProfileSavedCellModel(items: profileActivityItems),
    ProfileInfoCellModel(items: infoFields)
]

var profileActivityItems = [
    ActivityReactionCellModel(reactionIcon: "(", authorName: "Heather", postTitle: "The nuclear option will only hurt the GOP", dateString: "4 hours ago"),
    ActivityReactionCellModel(reactionIcon: ")", authorName: "Drake", postTitle: "Snoop’s Trump video went too far", dateString: "5 hours ago"),
    ActivityReactionCellModel(reactionIcon: "I", authorName: "thesphinx", postTitle: "Snoop’s Trump video went too far", dateString: "Yesterday"),
    ActivityReactionCellModel(reactionIcon: ")", authorName: "Drake", postTitle: "Snoop’s Trump video went too far", dateString: "5 hours ago"),
    ActivityReactionCellModel(reactionIcon: "I", authorName: "thesphinx", postTitle: "Snoop’s Trump video went too far", dateString: "Yesterday"),
    ActivityReactionCellModel(reactionIcon: ")", authorName: "Drake", postTitle: "Snoop’s Trump video went too far", dateString: "5 hours ago"),
    ActivityReactionCellModel(reactionIcon: "I", authorName: "thesphinx", postTitle: "Snoop’s Trump video went too far", dateString: "Yesterday")
]

let infoFields: [IdentifiableViewModel] = [
    InfoFieldCellModel(placeHolderText: "Full Name", fieldIcon: "u"),
    InfoFieldCellModel(placeHolderText: "Username", fieldIcon: "E"),
    InfoFieldCellModel(placeHolderText: "Email Address", fieldIcon: "m"),
    InfoFieldCellModel(placeHolderText: "Password", fieldIcon: "l", secure: true),
    InfoButtonCellModel(buttonText: "Log Out")
]

func stringFromDate(date: Date) -> String {
    //If within 2 minutes       -> now
    //If today                  -> time eg. 10:24am
    //If yesterday              -> Yesterday
    //If this week              -> Tuesday
    //If older                  -> 12/23/16
    return "10:24am"
}

