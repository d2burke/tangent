//
//  TabBarViewController.swift
//  Tangent
//
//  Created by Daniel Burke on 1/8/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit
import Firebase

class TabBarViewController: UITabBarController {
    
    let exploreViewController = ExploreViewController()
    let inboxViewController = InboxViewController()
    let profileViewController = ProfileViewController()
    let feedViewController = FeedViewController()
    
    var tabBarTopBorderVisible = true {
        didSet {
            self.tabBar.layer.borderWidth = self.tabBarTopBorderVisible ? 1 : 0.50
            self.tabBar.layer.borderColor = self.tabBarTopBorderVisible ? UIColor.lightGray.cgColor : UIColor.clear.cgColor
            self.tabBar.clipsToBounds = !self.tabBarTopBorderVisible
        }
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
        let feedTabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "feed"), tag: 0)
        feedTabBarItem.title = nil
        feedTabBarItem.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
        feedViewController.tabBarItem = feedTabBarItem
        feedViewController.view.backgroundColor = .clear
        let feedNavController = UINavigationController(rootViewController: feedViewController)
        feedNavController.isNavigationBarHidden = true
        
        let exploreTabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "explore"), tag: 1)
        exploreTabBarItem.title = nil
        exploreTabBarItem.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
        exploreViewController.tabBarItem = exploreTabBarItem
        exploreViewController.view.backgroundColor = .clear
        
        let inboxTabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "inbox"), tag: 2)
        inboxTabBarItem.title = nil
        inboxTabBarItem.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
        inboxViewController.tabBarItem = inboxTabBarItem
        inboxViewController.view.backgroundColor = .clear
        let inboxNavController = UINavigationController(rootViewController: inboxViewController)
        inboxNavController.isNavigationBarHidden = true
        
        let profileTabBarItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "profile"), tag: 3)
        profileTabBarItem.title = nil
        profileTabBarItem.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
        profileViewController.tabBarItem = profileTabBarItem
        profileViewController.view.backgroundColor = .clear
        
        view.backgroundColor = TAColor.white.color
        view.layer.cornerRadius = 6
        view.clipsToBounds = true
        tabBar.isTranslucent = false
        tabBar.tintColor = TAColor.orange.color
        tabBar.barTintColor = .white
        viewControllers = [feedNavController, exploreViewController, profileViewController]
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let auth = FIRAuth.auth() {
            auth.addStateDidChangeListener({ (auth, user) in
                if user == nil {
                    let loginViewController = LoginViewController()
                    loginViewController.delegate = self
                    self.present(loginViewController, animated: false, completion: nil)
                }
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension TabBarViewController: LoginViewControllerDelegate {
    func didLogin() {
        self.selectedIndex = 0
        self.dismiss(animated: true, completion: nil)
    }
}
