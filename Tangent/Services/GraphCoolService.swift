//
//  GraphCoolService.swift
//  Tangent
//
//  Created by Daniel Burke on 4/26/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit
import Apollo

let graphlQLEndpointURL = "https://api.graph.cool/simple/v1/tangent"
let apollo = ApolloClient(url: URL(string: graphlQLEndpointURL)!)

class GraphCoolService {
    init() { }
}

extension String {
    func date() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.000Z"
        return dateFormatter.date(from: self) ?? Date()
    }
}

extension GetFeedQuery.Data.AllPost.Reaction {
    
}

extension GetFeedQuery.Data.AllPost.Comment.Comments1.Comments1.Author {
    func toUser() -> User {
        return User(
            firstName: self.firstName ?? "",
            lastName: self.lastName ?? "",
            userName: self.username ?? "",
            avatarURL: "",
            createdAt: self.createdAt.date()
        )
    }
}

extension GetFeedQuery.Data.AllPost.Comment.Author {
    func toUser() -> User {
        return User(
            firstName: self.firstName ?? "",
            lastName: self.lastName ?? "",
            userName: self.username ?? "",
            avatarURL: "",
            createdAt: self.createdAt.date()
        )
    }
}

extension GetFeedQuery.Data.AllPost.Comment.Comments1.Author {
    func toUser() -> User {
        return User(
            firstName: self.firstName ?? "",
            lastName: self.lastName ?? "",
            userName: self.username ?? "",
            avatarURL: "",
            createdAt: self.createdAt.date()
        )
    }
}

extension GraphCoolService: FeedService {
    
    //TODO:
    /**
     *  Move to exetension and allow Tangent types to conform
     *  thereby gaining the ability to return GraphCool types
     *  of themselves
     */
    
    func fetchPosts(success: @escaping ([IdentifiableViewModel]) -> Void) {
        apollo.fetch(query: GetFeedQuery(), cachePolicy: .fetchIgnoringCacheData) { result, error in
            if let error = error {
                print(#function, "ERROR | An error occured: \(error)")
                return
            }
            guard let allPosts = result?.data?.allPosts else {
                print(#function, "ERROR | Could not retrieve trainer")
                return
            }
            
            var posts = [IdentifiableViewModel]()
            for postJSON in allPosts {
                var post = Post(text: postJSON.text ?? "", imageURL: postJSON.imageUrl ?? "")
                
                if let imageURL = postJSON.imageUrl {
                    post.imageURL = imageURL
                }
                
                if let reactions = postJSON.reactions {
                    post.reactions = reactions.map{ reaction in
                        var type: ReactionType = .none
                        
                        //TODO: This sucks, factor this out
                        if let reactionType = reaction.reactionType {
                            switch reactionType {
                            case .agree: type = ReactionType.agree
                            case .disagree: type = ReactionType.disagree
                            case .goodPoint: type = ReactionType.goodPoint
                            case .offensive: type = ReactionType.offensive
                            case .apathetic: type = ReactionType.apathetic
                            case .none: type = ReactionType.none
                            }
                        }
                        return Reaction(
                            type:  type,
                            author: User(firstName: reaction.author?.firstName ?? "",
                                         lastName: reaction.author?.lastName ?? "",
                                         userName: reaction.author?.username ?? "",
                                         avatarURL: ""),
                            date: reaction.createdAt.date()
                        )
                    }
                }
                
                if let comments = postJSON.comments {
                    post.comments = comments.map {
                        return Comment(
                            text: $0.text ?? "",
                            author: ($0.author?.toUser())!,
                            dateCreated: $0.createdAt.date(),
                            comments: $0.comments1?.map { reply in
                                return Comment(
                                    text: reply.text ?? "",
                                    author: (reply.author?.toUser())!,
                                    dateCreated: reply.createdAt.date(),
                                    comments: reply.comments1?.map { reply in
                                        return Comment(
                                            text: reply.text ?? "",
                                            author: (reply.author?.toUser())!,
                                            dateCreated: reply.createdAt.date(),
                                            comments: nil,
                                            reactions: nil
                                        )
                                    },
                                    reactions: nil
                                )
                            }
                        )
                    }
                }
                
                let postCellModel = FeedPostCellViewModel(post: post)
                posts.append(postCellModel)
            }
            posts.insert(FeedPeopleCellViewModel(), at: 1)
            success(posts)
        }
    }
}
