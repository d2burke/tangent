//
//  FeedService.swift
//  Tangent
//
//  Created by Daniel Burke on 4/24/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

/**
 *  Any service (GraphQL, REST, LocalDB, etc, must provide this interface
 */
protocol FeedService {
    func fetchPosts(success: @escaping ([IdentifiableViewModel]) -> Void)
}
