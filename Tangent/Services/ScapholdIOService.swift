//
//  ScapholdIOService.swift
//  Tangent
//
//  Created by Daniel Burke on 4/24/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation
import Apollo

let graphlQLEndpointURL = "https://us-west-2.api.scaphold.io/graphql/tangent"
let apollo = ApolloClient(url: URL(string: graphlQLEndpointURL)!)

class ScapholdIOService {
    init() { }
}

extension ScapholdIOService: FeedService {
//    func fetchPosts(failure: @escaping (Error) -> Void, success: @escaping ([Post]) -> Void) {
//        let postQuery = GetPostsQuery()
//        apollo.fetch(query: postQuery) { result, error in
//            if let error = error {
//                print(#function, "ERROR | An error occured: \(error)")
//                return
//            } else {
//                var posts = [Post]()
//                
//                guard let edges = result?.data?.viewer?.allPosts?.edges else { return }
//                
//                for edge in edges {
//                    guard let postNode = edge?.node else { continue }
//                    var post = Post(text: postNode.text ?? "", imageURL: postNode.imageUrl ?? "")
//                    if let comments = postNode.comments?.edges {
//                        for commentNode in comments {
//                            guard let text = commentNode?.node.text, let author = commentNode?.node.author else { continue }
//                            let comment = Comment(
//                                text: text,
//                                imageURL: commentNode?.node.imageUrl,
//                                author: User(
//                                    firstName: author.firstName ?? "",
//                                    lastName: author.lastName ?? "",
//                                    userName: author.username,
//                                    avatarURL: author.imageUrl ?? ""
//                                ),
//                                dateCreated: Date()
//                            )
//                            post.comments.append(comment)
//                        }
//                    }
//                    if let reactions = postNode.reactions?.edges {
//                        for reactionNode in reactions {
//                            guard
//                                let type = reactionNode?.node.type,
//                                let reactionType = ReactionType(rawValue: type),
//                                let author = reactionNode?.node.author
//                                else { continue }
//                            
//                            let reaction = Reaction(
//                                type: reactionType,
//                                author: User(
//                                    firstName: author.firstName ?? "",
//                                    lastName: author.lastName ?? "",
//                                    userName: author.username,
//                                    avatarURL: author.imageUrl ?? ""
//                                ),
//                                date: Date()
//                            )
//                            post.reactions.append(reaction)
//                        }
//                    }
//                    posts.append(post)
//                }
//                success(posts)
//            }
//        }
//    }
}
