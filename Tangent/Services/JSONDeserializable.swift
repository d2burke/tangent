//
//  JSONDeserializable.swift
//  goals
//
//  Created by Grant Oladipo on 3/18/17.
//  Copyright © 2017 DevColor. All rights reserved.
//

import Foundation

protocol JSONDeserializable {
  static func make(from json: Any) -> Self?
}

extension JSONDeserializable {
  
  static func makeArray(from json: Any) ->[Self]? {
    guard let array = json as? Array<Any> else {
      return nil
    }
    
    return array.flatMap { json in
      return self.make(from: json)
    }
  }
}
