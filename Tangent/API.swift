//  This file was automatically generated and should not be edited.

import Apollo

public enum REACTION_REACTION_TYPE: String {
  case agree = "Agree"
  case disagree = "Disagree"
  case goodPoint = "Good_Point"
  case offensive = "Offensive"
  case apathetic = "Apathetic"
  case `none` = "None"
}

extension REACTION_REACTION_TYPE: JSONDecodable, JSONEncodable {}

public final class GetFeedQuery: GraphQLQuery {
  public static let operationDefinition =
    "query getFeed {" +
    "  allPosts(orderBy: createdAt_DESC) {" +
    "    __typename" +
    "    author {" +
    "      __typename" +
    "      id" +
    "      username" +
    "      firstName" +
    "      lastName" +
    "      email" +
    "      createdAt" +
    "      photoURL" +
    "    }" +
    "    imageURL" +
    "    text" +
    "    link" +
    "    createdAt" +
    "    reactions {" +
    "      __typename" +
    "      author {" +
    "        __typename" +
    "        id" +
    "        username" +
    "        firstName" +
    "        lastName" +
    "        email" +
    "        createdAt" +
    "        photoURL" +
    "      }" +
    "      reactionType" +
    "      createdAt" +
    "    }" +
    "    comments {" +
    "      __typename" +
    "      text" +
    "      author {" +
    "        __typename" +
    "        id" +
    "        username" +
    "        firstName" +
    "        lastName" +
    "        email" +
    "        createdAt" +
    "        photoURL" +
    "      }" +
    "      reactions {" +
    "        __typename" +
    "        reactionType" +
    "        author {" +
    "          __typename" +
    "          id" +
    "          username" +
    "          firstName" +
    "          lastName" +
    "          email" +
    "          createdAt" +
    "          photoURL" +
    "        }" +
    "        createdAt" +
    "      }" +
    "      comments1 {" +
    "        __typename" +
    "        text" +
    "        author {" +
    "          __typename" +
    "          id" +
    "          username" +
    "          firstName" +
    "          lastName" +
    "          email" +
    "          createdAt" +
    "          photoURL" +
    "        }" +
    "        reactions {" +
    "          __typename" +
    "          reactionType" +
    "          author {" +
    "            __typename" +
    "            id" +
    "            username" +
    "            firstName" +
    "            lastName" +
    "            email" +
    "            createdAt" +
    "            photoURL" +
    "          }" +
    "          createdAt" +
    "        }" +
    "        comments1 {" +
    "          __typename" +
    "          text" +
    "          author {" +
    "            __typename" +
    "            id" +
    "            username" +
    "            firstName" +
    "            lastName" +
    "            email" +
    "            createdAt" +
    "            photoURL" +
    "          }" +
    "          reactions {" +
    "            __typename" +
    "            reactionType" +
    "            author {" +
    "              __typename" +
    "              id" +
    "              username" +
    "              firstName" +
    "              lastName" +
    "              email" +
    "              createdAt" +
    "              photoURL" +
    "            }" +
    "            createdAt" +
    "          }" +
    "          comments1 {" +
    "            __typename" +
    "            text" +
    "            author {" +
    "              __typename" +
    "              id" +
    "              username" +
    "              firstName" +
    "              lastName" +
    "              email" +
    "              createdAt" +
    "              photoURL" +
    "            }" +
    "            reactions {" +
    "              __typename" +
    "              reactionType" +
    "              author {" +
    "                __typename" +
    "                id" +
    "                username" +
    "                firstName" +
    "                lastName" +
    "                email" +
    "                createdAt" +
    "                photoURL" +
    "              }" +
    "              createdAt" +
    "            }" +
    "            createdAt" +
    "          }" +
    "          createdAt" +
    "        }" +
    "        createdAt" +
    "      }" +
    "      createdAt" +
    "    }" +
    "  }" +
    "}"
  public init() {
  }

  public struct Data: GraphQLMappable {
    public let allPosts: [AllPost]

    public init(reader: GraphQLResultReader) throws {
      allPosts = try reader.list(for: Field(responseName: "allPosts", arguments: ["orderBy": "createdAt_DESC"]))
    }

    public struct AllPost: GraphQLMappable {
      public let __typename: String
      public let author: Author?
      public let imageUrl: String?
      public let text: String?
      public let link: String?
      public let createdAt: String
      public let reactions: [Reaction]?
      public let comments: [Comment]?

      public init(reader: GraphQLResultReader) throws {
        __typename = try reader.value(for: Field(responseName: "__typename"))
        author = try reader.optionalValue(for: Field(responseName: "author"))
        imageUrl = try reader.optionalValue(for: Field(responseName: "imageURL"))
        text = try reader.optionalValue(for: Field(responseName: "text"))
        link = try reader.optionalValue(for: Field(responseName: "link"))
        createdAt = try reader.value(for: Field(responseName: "createdAt"))
        reactions = try reader.optionalList(for: Field(responseName: "reactions"))
        comments = try reader.optionalList(for: Field(responseName: "comments"))
      }

      public struct Author: GraphQLMappable {
        public let __typename: String
        public let id: GraphQLID
        public let username: String?
        public let firstName: String?
        public let lastName: String?
        public let email: String?
        public let createdAt: String
        public let photoUrl: String?

        public init(reader: GraphQLResultReader) throws {
          __typename = try reader.value(for: Field(responseName: "__typename"))
          id = try reader.value(for: Field(responseName: "id"))
          username = try reader.optionalValue(for: Field(responseName: "username"))
          firstName = try reader.optionalValue(for: Field(responseName: "firstName"))
          lastName = try reader.optionalValue(for: Field(responseName: "lastName"))
          email = try reader.optionalValue(for: Field(responseName: "email"))
          createdAt = try reader.value(for: Field(responseName: "createdAt"))
          photoUrl = try reader.optionalValue(for: Field(responseName: "photoURL"))
        }
      }

      public struct Reaction: GraphQLMappable {
        public let __typename: String
        public let author: Author?
        public let reactionType: REACTION_REACTION_TYPE?
        public let createdAt: String

        public init(reader: GraphQLResultReader) throws {
          __typename = try reader.value(for: Field(responseName: "__typename"))
          author = try reader.optionalValue(for: Field(responseName: "author"))
          reactionType = try reader.optionalValue(for: Field(responseName: "reactionType"))
          createdAt = try reader.value(for: Field(responseName: "createdAt"))
        }

        public struct Author: GraphQLMappable {
          public let __typename: String
          public let id: GraphQLID
          public let username: String?
          public let firstName: String?
          public let lastName: String?
          public let email: String?
          public let createdAt: String
          public let photoUrl: String?

          public init(reader: GraphQLResultReader) throws {
            __typename = try reader.value(for: Field(responseName: "__typename"))
            id = try reader.value(for: Field(responseName: "id"))
            username = try reader.optionalValue(for: Field(responseName: "username"))
            firstName = try reader.optionalValue(for: Field(responseName: "firstName"))
            lastName = try reader.optionalValue(for: Field(responseName: "lastName"))
            email = try reader.optionalValue(for: Field(responseName: "email"))
            createdAt = try reader.value(for: Field(responseName: "createdAt"))
            photoUrl = try reader.optionalValue(for: Field(responseName: "photoURL"))
          }
        }
      }

      public struct Comment: GraphQLMappable {
        public let __typename: String
        public let text: String?
        public let author: Author?
        public let reactions: [Reaction]?
        public let comments1: [Comments1]?
        public let createdAt: String

        public init(reader: GraphQLResultReader) throws {
          __typename = try reader.value(for: Field(responseName: "__typename"))
          text = try reader.optionalValue(for: Field(responseName: "text"))
          author = try reader.optionalValue(for: Field(responseName: "author"))
          reactions = try reader.optionalList(for: Field(responseName: "reactions"))
          comments1 = try reader.optionalList(for: Field(responseName: "comments1"))
          createdAt = try reader.value(for: Field(responseName: "createdAt"))
        }

        public struct Author: GraphQLMappable {
          public let __typename: String
          public let id: GraphQLID
          public let username: String?
          public let firstName: String?
          public let lastName: String?
          public let email: String?
          public let createdAt: String
          public let photoUrl: String?

          public init(reader: GraphQLResultReader) throws {
            __typename = try reader.value(for: Field(responseName: "__typename"))
            id = try reader.value(for: Field(responseName: "id"))
            username = try reader.optionalValue(for: Field(responseName: "username"))
            firstName = try reader.optionalValue(for: Field(responseName: "firstName"))
            lastName = try reader.optionalValue(for: Field(responseName: "lastName"))
            email = try reader.optionalValue(for: Field(responseName: "email"))
            createdAt = try reader.value(for: Field(responseName: "createdAt"))
            photoUrl = try reader.optionalValue(for: Field(responseName: "photoURL"))
          }
        }

        public struct Reaction: GraphQLMappable {
          public let __typename: String
          public let reactionType: REACTION_REACTION_TYPE?
          public let author: Author?
          public let createdAt: String

          public init(reader: GraphQLResultReader) throws {
            __typename = try reader.value(for: Field(responseName: "__typename"))
            reactionType = try reader.optionalValue(for: Field(responseName: "reactionType"))
            author = try reader.optionalValue(for: Field(responseName: "author"))
            createdAt = try reader.value(for: Field(responseName: "createdAt"))
          }

          public struct Author: GraphQLMappable {
            public let __typename: String
            public let id: GraphQLID
            public let username: String?
            public let firstName: String?
            public let lastName: String?
            public let email: String?
            public let createdAt: String
            public let photoUrl: String?

            public init(reader: GraphQLResultReader) throws {
              __typename = try reader.value(for: Field(responseName: "__typename"))
              id = try reader.value(for: Field(responseName: "id"))
              username = try reader.optionalValue(for: Field(responseName: "username"))
              firstName = try reader.optionalValue(for: Field(responseName: "firstName"))
              lastName = try reader.optionalValue(for: Field(responseName: "lastName"))
              email = try reader.optionalValue(for: Field(responseName: "email"))
              createdAt = try reader.value(for: Field(responseName: "createdAt"))
              photoUrl = try reader.optionalValue(for: Field(responseName: "photoURL"))
            }
          }
        }

        public struct Comments1: GraphQLMappable {
          public let __typename: String
          public let text: String?
          public let author: Author?
          public let reactions: [Reaction]?
          public let comments1: [Comments1]?
          public let createdAt: String

          public init(reader: GraphQLResultReader) throws {
            __typename = try reader.value(for: Field(responseName: "__typename"))
            text = try reader.optionalValue(for: Field(responseName: "text"))
            author = try reader.optionalValue(for: Field(responseName: "author"))
            reactions = try reader.optionalList(for: Field(responseName: "reactions"))
            comments1 = try reader.optionalList(for: Field(responseName: "comments1"))
            createdAt = try reader.value(for: Field(responseName: "createdAt"))
          }

          public struct Author: GraphQLMappable {
            public let __typename: String
            public let id: GraphQLID
            public let username: String?
            public let firstName: String?
            public let lastName: String?
            public let email: String?
            public let createdAt: String
            public let photoUrl: String?

            public init(reader: GraphQLResultReader) throws {
              __typename = try reader.value(for: Field(responseName: "__typename"))
              id = try reader.value(for: Field(responseName: "id"))
              username = try reader.optionalValue(for: Field(responseName: "username"))
              firstName = try reader.optionalValue(for: Field(responseName: "firstName"))
              lastName = try reader.optionalValue(for: Field(responseName: "lastName"))
              email = try reader.optionalValue(for: Field(responseName: "email"))
              createdAt = try reader.value(for: Field(responseName: "createdAt"))
              photoUrl = try reader.optionalValue(for: Field(responseName: "photoURL"))
            }
          }

          public struct Reaction: GraphQLMappable {
            public let __typename: String
            public let reactionType: REACTION_REACTION_TYPE?
            public let author: Author?
            public let createdAt: String

            public init(reader: GraphQLResultReader) throws {
              __typename = try reader.value(for: Field(responseName: "__typename"))
              reactionType = try reader.optionalValue(for: Field(responseName: "reactionType"))
              author = try reader.optionalValue(for: Field(responseName: "author"))
              createdAt = try reader.value(for: Field(responseName: "createdAt"))
            }

            public struct Author: GraphQLMappable {
              public let __typename: String
              public let id: GraphQLID
              public let username: String?
              public let firstName: String?
              public let lastName: String?
              public let email: String?
              public let createdAt: String
              public let photoUrl: String?

              public init(reader: GraphQLResultReader) throws {
                __typename = try reader.value(for: Field(responseName: "__typename"))
                id = try reader.value(for: Field(responseName: "id"))
                username = try reader.optionalValue(for: Field(responseName: "username"))
                firstName = try reader.optionalValue(for: Field(responseName: "firstName"))
                lastName = try reader.optionalValue(for: Field(responseName: "lastName"))
                email = try reader.optionalValue(for: Field(responseName: "email"))
                createdAt = try reader.value(for: Field(responseName: "createdAt"))
                photoUrl = try reader.optionalValue(for: Field(responseName: "photoURL"))
              }
            }
          }

          public struct Comments1: GraphQLMappable {
            public let __typename: String
            public let text: String?
            public let author: Author?
            public let reactions: [Reaction]?
            public let comments1: [Comments1]?
            public let createdAt: String

            public init(reader: GraphQLResultReader) throws {
              __typename = try reader.value(for: Field(responseName: "__typename"))
              text = try reader.optionalValue(for: Field(responseName: "text"))
              author = try reader.optionalValue(for: Field(responseName: "author"))
              reactions = try reader.optionalList(for: Field(responseName: "reactions"))
              comments1 = try reader.optionalList(for: Field(responseName: "comments1"))
              createdAt = try reader.value(for: Field(responseName: "createdAt"))
            }

            public struct Author: GraphQLMappable {
              public let __typename: String
              public let id: GraphQLID
              public let username: String?
              public let firstName: String?
              public let lastName: String?
              public let email: String?
              public let createdAt: String
              public let photoUrl: String?

              public init(reader: GraphQLResultReader) throws {
                __typename = try reader.value(for: Field(responseName: "__typename"))
                id = try reader.value(for: Field(responseName: "id"))
                username = try reader.optionalValue(for: Field(responseName: "username"))
                firstName = try reader.optionalValue(for: Field(responseName: "firstName"))
                lastName = try reader.optionalValue(for: Field(responseName: "lastName"))
                email = try reader.optionalValue(for: Field(responseName: "email"))
                createdAt = try reader.value(for: Field(responseName: "createdAt"))
                photoUrl = try reader.optionalValue(for: Field(responseName: "photoURL"))
              }
            }

            public struct Reaction: GraphQLMappable {
              public let __typename: String
              public let reactionType: REACTION_REACTION_TYPE?
              public let author: Author?
              public let createdAt: String

              public init(reader: GraphQLResultReader) throws {
                __typename = try reader.value(for: Field(responseName: "__typename"))
                reactionType = try reader.optionalValue(for: Field(responseName: "reactionType"))
                author = try reader.optionalValue(for: Field(responseName: "author"))
                createdAt = try reader.value(for: Field(responseName: "createdAt"))
              }

              public struct Author: GraphQLMappable {
                public let __typename: String
                public let id: GraphQLID
                public let username: String?
                public let firstName: String?
                public let lastName: String?
                public let email: String?
                public let createdAt: String
                public let photoUrl: String?

                public init(reader: GraphQLResultReader) throws {
                  __typename = try reader.value(for: Field(responseName: "__typename"))
                  id = try reader.value(for: Field(responseName: "id"))
                  username = try reader.optionalValue(for: Field(responseName: "username"))
                  firstName = try reader.optionalValue(for: Field(responseName: "firstName"))
                  lastName = try reader.optionalValue(for: Field(responseName: "lastName"))
                  email = try reader.optionalValue(for: Field(responseName: "email"))
                  createdAt = try reader.value(for: Field(responseName: "createdAt"))
                  photoUrl = try reader.optionalValue(for: Field(responseName: "photoURL"))
                }
              }
            }

            public struct Comments1: GraphQLMappable {
              public let __typename: String
              public let text: String?
              public let author: Author?
              public let reactions: [Reaction]?
              public let createdAt: String

              public init(reader: GraphQLResultReader) throws {
                __typename = try reader.value(for: Field(responseName: "__typename"))
                text = try reader.optionalValue(for: Field(responseName: "text"))
                author = try reader.optionalValue(for: Field(responseName: "author"))
                reactions = try reader.optionalList(for: Field(responseName: "reactions"))
                createdAt = try reader.value(for: Field(responseName: "createdAt"))
              }

              public struct Author: GraphQLMappable {
                public let __typename: String
                public let id: GraphQLID
                public let username: String?
                public let firstName: String?
                public let lastName: String?
                public let email: String?
                public let createdAt: String
                public let photoUrl: String?

                public init(reader: GraphQLResultReader) throws {
                  __typename = try reader.value(for: Field(responseName: "__typename"))
                  id = try reader.value(for: Field(responseName: "id"))
                  username = try reader.optionalValue(for: Field(responseName: "username"))
                  firstName = try reader.optionalValue(for: Field(responseName: "firstName"))
                  lastName = try reader.optionalValue(for: Field(responseName: "lastName"))
                  email = try reader.optionalValue(for: Field(responseName: "email"))
                  createdAt = try reader.value(for: Field(responseName: "createdAt"))
                  photoUrl = try reader.optionalValue(for: Field(responseName: "photoURL"))
                }
              }

              public struct Reaction: GraphQLMappable {
                public let __typename: String
                public let reactionType: REACTION_REACTION_TYPE?
                public let author: Author?
                public let createdAt: String

                public init(reader: GraphQLResultReader) throws {
                  __typename = try reader.value(for: Field(responseName: "__typename"))
                  reactionType = try reader.optionalValue(for: Field(responseName: "reactionType"))
                  author = try reader.optionalValue(for: Field(responseName: "author"))
                  createdAt = try reader.value(for: Field(responseName: "createdAt"))
                }

                public struct Author: GraphQLMappable {
                  public let __typename: String
                  public let id: GraphQLID
                  public let username: String?
                  public let firstName: String?
                  public let lastName: String?
                  public let email: String?
                  public let createdAt: String
                  public let photoUrl: String?

                  public init(reader: GraphQLResultReader) throws {
                    __typename = try reader.value(for: Field(responseName: "__typename"))
                    id = try reader.value(for: Field(responseName: "id"))
                    username = try reader.optionalValue(for: Field(responseName: "username"))
                    firstName = try reader.optionalValue(for: Field(responseName: "firstName"))
                    lastName = try reader.optionalValue(for: Field(responseName: "lastName"))
                    email = try reader.optionalValue(for: Field(responseName: "email"))
                    createdAt = try reader.value(for: Field(responseName: "createdAt"))
                    photoUrl = try reader.optionalValue(for: Field(responseName: "photoURL"))
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

public final class CreatePostMutation: GraphQLMutation {
  public static let operationDefinition =
    "mutation CreatePost($text: String, $authorId: ID, $link: String, $imageURL: String) {" +
    "  createPost(text: $text, authorId: $authorId, link: $link, imageURL: $imageURL) {" +
    "    __typename" +
    "    id" +
    "  }" +
    "}"

  public let text: String?
  public let authorId: GraphQLID?
  public let link: String?
  public let imageUrl: String?

  public init(text: String? = nil, authorId: GraphQLID? = nil, link: String? = nil, imageUrl: String? = nil) {
    self.text = text
    self.authorId = authorId
    self.link = link
    self.imageUrl = imageUrl
  }

  public var variables: GraphQLMap? {
    return ["text": text, "authorId": authorId, "link": link, "imageURL": imageUrl]
  }

  public struct Data: GraphQLMappable {
    public let createPost: CreatePost?

    public init(reader: GraphQLResultReader) throws {
      createPost = try reader.optionalValue(for: Field(responseName: "createPost", arguments: ["text": reader.variables["text"], "authorId": reader.variables["authorId"], "link": reader.variables["link"], "imageURL": reader.variables["imageURL"]]))
    }

    public struct CreatePost: GraphQLMappable {
      public let __typename: String
      public let id: GraphQLID

      public init(reader: GraphQLResultReader) throws {
        __typename = try reader.value(for: Field(responseName: "__typename"))
        id = try reader.value(for: Field(responseName: "id"))
      }
    }
  }
}