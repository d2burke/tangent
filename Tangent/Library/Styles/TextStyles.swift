//
//  TextStyles.swift
//  Tangent
//
//  Created by Daniel Burke on 12/29/16.
//  Copyright © 2016 Daniel Burke. All rights reserved.
//

import UIKit

enum TextStyle {
    case pageTitle
    case header
    case field
    case button
    case largePostTitle
    case paragraph
    case boldParagraph
    case message
    case subtext
    case small
    case boldSmall
    case tiny
    
    public var font: UIFont {
        switch self {
        case .pageTitle:
            return UIFont.boldSystemFont(ofSize: 36)
        case .largePostTitle:
            return UIFont.boldSystemFont(ofSize: 24)
        case .button:
            return UIFont.systemFont(ofSize: 24)
        case .header:
            return UIFont.systemFont(ofSize: 20)
        case .field:
            return UIFont.systemFont(ofSize: 18)
        case .paragraph:
            return UIFont.systemFont(ofSize: 18)
        case .boldParagraph:
            return UIFont.boldSystemFont(ofSize: 18)
        case .message:
            return UIFont.systemFont(ofSize: 16)
        case .subtext:
            return UIFont.systemFont(ofSize: 14)
        case .small:
            return UIFont.systemFont(ofSize: 12)
        case .boldSmall:
            return UIFont.boldSystemFont(ofSize: 12)
        case .tiny:
            return UIFont.systemFont(ofSize: 10)
        }
    }
}
