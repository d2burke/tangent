//
//  Color.swift
//  Tangent
//
//  Created by Daniel Burke on 12/29/16.
//  Copyright © 2016 Daniel Burke. All rights reserved.
//

import Foundation
import UIKit

enum TAColor {
    case orange
    case green
    case lightGreen
    case teal
    case blue
    case lightBlue
    case red
    case yellow
    case lightOrange
    case dark
    case night
    case gray
    case medGray
    case lightGray
    case white
    case clear
    
    public var color: UIColor {
        switch self {
        case .orange:
            return UIColor.hex(value: 0xD65907)
        case .green:
            return UIColor.hex(value: 0x5CB100)
        case .lightGreen:
            return UIColor.hex(value: 0xAAD180)
        case .teal:
            return UIColor.hex(value: 0x50E3C2)
        case .blue:
            return UIColor.hex(value: 0x4990E2)
        case .lightBlue:
            return UIColor.hex(value: 0xB5D8FF)
        case .red:
            return UIColor.hex(value: 0xD0011B)
        case .yellow:
            return UIColor.hex(value: 0xFFEE60)
        case .lightOrange:
            return UIColor.hex(value: 0xFFA570)
        case .dark:
            return UIColor.hex(value: 0x4A4A4A)
        case .night:
            return UIColor.hex(value: 0x292929)
        case .gray:
            return UIColor.hex(value: 0x9B9B9B)
        case .medGray:
            return UIColor.hex(value: 0xE9E9E9)
        case .lightGray:
            return UIColor.hex(value: 0xEEEEEE)
        case .white:
            return UIColor.hex(value: 0xFFFFFF)
        case .clear:
            return UIColor.clear
        }
    }
}
