//
//  PageHeaderViewModel.swift
//  Tangent
//
//  Created by Daniel Burke on 1/16/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation
import UIKit


public struct PageHeaderViewModel {
    let searchFieldPlaceholderText: String
    let searchFieldIcon: String
    let pageTitleText: String
    var headerImageViewURL: String? = nil
    var actionButtonTitle: String? = nil
    var actionButtonAction: TapAction? = nil
    
    init(
        searchFieldPlaceholderText: String = "",
        searchFieldIcon: String = "",
        pageTitleText: String = "",
        headerImageViewURL: String? = nil,
        actionButtonTitle: String? = nil,
        actionButtonAction: TapAction? = nil
        ) {
        self.searchFieldPlaceholderText = searchFieldPlaceholderText
        self.searchFieldIcon = searchFieldIcon
        self.pageTitleText = pageTitleText
        
        if let imageURL = headerImageViewURL {
            self.headerImageViewURL = imageURL
        }
        
        if let buttonTitle = actionButtonTitle {
            self.actionButtonTitle = buttonTitle
        }
        
        if let action = actionButtonAction {
            self.actionButtonAction = action
        }
    }
}
