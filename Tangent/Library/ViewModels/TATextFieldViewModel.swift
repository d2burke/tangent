//
//  TATextFieldViewModel.swift
//  Tangent
//
//  Created by Daniel Burke on 12/30/16.
//  Copyright © 2016 Daniel Burke. All rights reserved.
//


import Foundation
import UIKit

public struct TATextFieldViewModel {
    
    let text: String
    let placeholderText: String
    let textColor: TAColor
    let textStyle: TextStyle
    let placeholderColor: TAColor
    let backgroundColor: TAColor
    let textAlignment: NSTextAlignment
    let leftViewIcon: String?
    let secure: Bool
    
    init(
        text: String? = nil,
        placeholderText: String? = nil,
        textColor: TAColor = .dark,
        textStyle: TextStyle = .field,
        placeholderColor: TAColor = .dark,
        backgroundColor: TAColor = .medGray,
        textAlignment: NSTextAlignment = .left,
        leftViewIcon: String? = nil,
        secure: Bool = false
        ) {
        self.text = text ?? ""
        self.placeholderText = placeholderText ?? ""
        self.textColor = textColor
        self.textStyle = textStyle
        self.textAlignment = textAlignment
        self.placeholderColor = placeholderColor
        self.backgroundColor = backgroundColor
        if let icon = leftViewIcon {
            self.leftViewIcon = icon
        } else {
            self.leftViewIcon = nil
        }
        self.secure = secure
    }
}
