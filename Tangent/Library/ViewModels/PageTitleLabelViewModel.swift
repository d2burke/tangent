//
//  PageTitleLabelViewModel.swift
//  Tangent
//
//  Created by Daniel Burke on 12/29/16.
//  Copyright © 2016 Daniel Burke. All rights reserved.
//

import Foundation
import UIKit

public struct PageTitleLabelViewModel {
    
    let text: String
    let textColor: TAColor
    let textStyle: TextStyle
    let numberOfLines: Int
    let textAlignment: NSTextAlignment
    let lineBreakMode: NSLineBreakMode
    
    init(
        text: String? = nil,
        textColor: TAColor = .dark,
        textStyle: TextStyle = .pageTitle,
        numberOfLines: Int = 1,
        textAlignment: NSTextAlignment = .left,
        lineBreakMode: NSLineBreakMode = .byTruncatingTail
        ) {
        self.text = text ?? ""
        self.textColor = textColor
        self.textStyle = textStyle
        self.textAlignment = textAlignment
        self.numberOfLines = numberOfLines
        self.lineBreakMode = lineBreakMode
    }
}


