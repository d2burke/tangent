//
//  Utils.swift
//  Tangent
//
//  Created by Daniel Burke on 2/15/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation
import UIKit

func translatesAutoresizingMaskIntoConstraints(views: [UIView]) {
    for view in views {
        view.translatesAutoresizingMaskIntoConstraints = false
    }
}

func blockAutoResizing(views: [UIView]) {
    for view in views {
        view.translatesAutoresizingMaskIntoConstraints = false
    }
}

extension Date {
    static func dayOfWeek() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: Date()).capitalized
        // or use capitalized(with: locale) if you want
    }
}
