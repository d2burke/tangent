//
//  TALabel.swift
//  Tangent
//
//  Created by Daniel Burke on 12/31/16.
//  Copyright © 2016 Daniel Burke. All rights reserved.
//

import UIKit

class TALabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure(with: LabelViewModel()) //Default label
    }
    
    public convenience init(viewModel: LabelViewModel) {
        self.init()
        configure(with: viewModel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func configure(with viewModel: LabelViewModel) {
        text = viewModel.text
        font = viewModel.textStyle.font
        textColor = viewModel.textColor.color
        textAlignment = viewModel.textAlignment
        numberOfLines = viewModel.numberOfLines
        lineBreakMode = viewModel.lineBreakMode
    }
}
