//
//  IdentifiableCell.swift
//  Tangent
//
//  Created by Daniel Burke on 1/22/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

protocol IdentifiableViewModel {
    var identifier: String { get }
    var cellHeight: Float { get }
}
