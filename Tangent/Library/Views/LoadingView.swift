//
//  LoadingView.swift
//  Tangent
//
//  Created by Daniel Burke on 4/25/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class LoadingView: UIControl {
    var stopLoading = false
    let containerView: UIView = {
        let view = UIView(frame: .zero)
        view.layer.cornerRadius = 6
        view.backgroundColor = TAColor.lightGray.color.withAlphaComponent(0.3)
        view.clipsToBounds = true
        return view
    }()
    
    let logoImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "logo").withRenderingMode(.alwaysTemplate))
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = TAColor.orange.color
        return imageView
    }()
    
    init() {
        super.init(frame: .zero)
        containerView.addSubview(logoImageView)
        addSubview(containerView)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "view": self,
            "containerView": containerView,
            "logoImageView": logoImageView
        ]
        
        blockAutoResizing(views: views.flatMap({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:[view]-(<=1)-[containerView(==100)]", options: .alignAllCenterY, metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[view]-(<=1)-[containerView(==100)]", options: .alignAllCenterX, metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-[logoImageView]-|", options: .alignAllCenterX, metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-[logoImageView]-|", options: .alignAllCenterX, metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    func fadeIn() {
        UIView.animate(withDuration: 1, animations: {
            self.alpha = 1
        }) { (complete) in
            self.fadeOut()
        }
    }
    
    func fadeOut() {
        guard !stopLoading else { return }
        UIView.animate(withDuration: 1, animations: {
            self.alpha = 0.2
        }) { (complete) in
            self.fadeIn()
        }
    }
    
    func start() {
        isHidden = false
        stopLoading = false
        fadeIn()
    }
    
    func stop() {
        stopLoading = true
        isHidden = true
    }}
