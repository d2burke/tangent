//
//  ReactionsControl.swift
//  Tangent
//
//  Created by Daniel Burke on 1/7/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ReactionsControl: UIView {
    
    let agreeButton = ReactionButton(icon: "(", color: .green, labelText: "")
    let disagreeButton = ReactionButton(icon: ")", color: .orange, labelText: "")
    let goodPointButton = ReactionButton(icon: "I", color: .blue, labelText: "")
    let offensiveButton = ReactionButton(icon: "i", color: .red, labelText: "")
    let mehButton = ReactionButton(icon: "-", color: .yellow, labelText: "", iconColor: TAColor.dark.color)
    let arrowView = TriangleView(color: TAColor.medGray.color)
    
    lazy var agreeTopConstraint: NSLayoutConstraint = {
        return NSLayoutConstraint(
            item: self,
            attribute: NSLayoutAttribute.top,
            relatedBy: NSLayoutRelation.greaterThanOrEqual,
            toItem: self.agreeButton,
            attribute: NSLayoutAttribute.top,
            multiplier: 1,
            constant: -5
        )
    }()
    
    lazy var disagreeTopConstraint: NSLayoutConstraint = {
        return NSLayoutConstraint(
            item: self,
            attribute: NSLayoutAttribute.top,
            relatedBy: NSLayoutRelation.greaterThanOrEqual,
            toItem: self.disagreeButton,
            attribute: NSLayoutAttribute.top,
            multiplier: 1,
            constant: -5
        )
    }()
    
    lazy var goodPointTopConstraint: NSLayoutConstraint = {
        return NSLayoutConstraint(
            item: self,
            attribute: NSLayoutAttribute.top,
            relatedBy: NSLayoutRelation.greaterThanOrEqual,
            toItem: self.goodPointButton,
            attribute: NSLayoutAttribute.top,
            multiplier: 1,
            constant: -5
        )
    }()
    
    lazy var offensiveTopConstraint: NSLayoutConstraint = {
        return NSLayoutConstraint(
            item: self,
            attribute: NSLayoutAttribute.top,
            relatedBy: NSLayoutRelation.greaterThanOrEqual,
            toItem: self.offensiveButton,
            attribute: NSLayoutAttribute.top,
            multiplier: 1,
            constant: -5
        )
    }()
    
    lazy var mehTopConstraint: NSLayoutConstraint = {
        return NSLayoutConstraint(
            item: self,
            attribute: NSLayoutAttribute.top,
            relatedBy: NSLayoutRelation.greaterThanOrEqual,
            toItem: self.mehButton,
            attribute: NSLayoutAttribute.top,
            multiplier: 1,
            constant: -5
        )
    }()
    
    init(){
        super.init(frame: .zero)

        backgroundColor = TAColor.medGray.color
        layer.cornerRadius = 25
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 10
        layer.shadowOffset = CGSize(width: 2, height: 7)
        layer.shadowColor = TAColor.dark.color.cgColor
        
        addSubview(arrowView)
        agreeButton.highlightType = .size(scale: 1.5)
        addSubview(agreeButton)
        addSubview(disagreeButton)
        addSubview(goodPointButton)
        addSubview(offensiveButton)
        addSubview(mehButton)
        
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "agreeButton": agreeButton,
            "disagreeButton": disagreeButton,
            "goodPointButton": goodPointButton,
            "offensiveButton": offensiveButton,
            "mehButton": mehButton,
            "arrowView": arrowView
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            [agreeTopConstraint],
            [disagreeTopConstraint],
            [goodPointTopConstraint],
            [offensiveTopConstraint],
            [mehTopConstraint],
            NSLayoutConstraint.constraints(withVisualFormat: "V:[agreeButton(==40)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[disagreeButton(==40)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[goodPointButton(==40)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[offensiveButton(==40)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[mehButton(==40)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[agreeButton(==40)]-15-[disagreeButton(==40)]-15-[goodPointButton(==40)]-15-[offensiveButton(==40)]-15-[mehButton(==40)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[arrowView(==20)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[arrowView(==15)]-(-12)-|", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    func setActive(reaction: ReactionType) {
        
        agreeTopConstraint.constant = -5
        disagreeTopConstraint.constant = -5
        goodPointTopConstraint.constant = -5
        offensiveTopConstraint.constant = -5
        mehTopConstraint.constant = -5
        
        switch reaction {
        case .agree:
            agreeTopConstraint.constant = 25
        case .disagree:
            disagreeTopConstraint.constant = 25
        case .goodPoint:
            goodPointTopConstraint.constant = 25
        case .offensive:
            offensiveTopConstraint.constant = 25
        case .apathetic:
            mehTopConstraint.constant = 25
        default: ()
        }
        
        UIView.animate(withDuration: 0.2) { 
            self.layoutIfNeeded()
        }
    }
}
