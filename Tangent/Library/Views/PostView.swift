//
//  PostView.swift
//  Tangent
//
//  Created by Daniel Burke on 4/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class PostView: UIView {
    
    let roundedContainer: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 6
        view.clipsToBounds = true
        return view
    }()
    
    let cardContainer: UIView = {
        let view = UIView()
        view.backgroundColor = TAColor.white.color
        view.layer.cornerRadius = 6
        view.layer.shadowOpacity = 0.2
        view.layer.shadowRadius = 5
        view.layer.shadowOffset = CGSize(width: 0, height: 7)
        view.layer.shadowColor = TAColor.dark.color.cgColor
        return view
    }()
    
    var topicImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = false
        return imageView
    }()
    
    let gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        gradientLayer.locations = [0,1]
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradientLayer.opacity = 1
        return gradientLayer
    }()
    
    var userInfoLabel: TALabel = {
        let label = TALabel(viewModel: LabelViewModel(
            text: "",
            textColor: .white,
            textStyle: .small,
            numberOfLines: 1
        ))
        label.isUserInteractionEnabled = false
        label.sizeToFit()
        label.layer.shadowOffset = CGSize(width: 0, height: 0)
        label.layer.shadowOpacity = 0.5
        label.layer.shadowRadius = 6
        return label
    }()
    
    let dateLabel: TALabel = {
        let label = TALabel(viewModel: LabelViewModel(
            text: " yesterday",
            textColor: .white,
            textStyle: .small,
            textAlignment: .right
        ))
        label.layer.shadowOffset = CGSize(width: 0, height: 0)
        label.layer.shadowOpacity = 0.5
        label.layer.shadowRadius = 6
        return label
    }()
    
    let moreButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("•", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "tangent", size: 20)
        return button
    }()
    
    var topicLabel: TALabel = {
        let label = TALabel(viewModel: LabelViewModel(
            text: "",
            textColor: .white,
            textStyle: .message,
            numberOfLines: 3
        ))
        label.isUserInteractionEnabled = false
        label.sizeToFit()
        return label
    }()
    
    var commentCountLabel: TALabel = {
        let label = TALabel(viewModel: LabelViewModel(
            text: "",
            textColor: .dark,
            textStyle: .small,
            numberOfLines: 1,
            textAlignment: .right
        ))
        label.isUserInteractionEnabled = false
        return label
    }()
    
    let reactionSelector = ReactionSelector()
    let reactionsDisplayView = ReactionsDisplay()
    
    init() {
        super.init(frame: .zero)
        backgroundColor = .clear
        topicImageView.layer.addSublayer(gradientLayer)
        roundedContainer.addSubview(topicImageView)
        roundedContainer.addSubview(moreButton)
        roundedContainer.addSubview(userInfoLabel)
        roundedContainer.addSubview(dateLabel)
        roundedContainer.addSubview(topicLabel)
        roundedContainer.addSubview(reactionsDisplayView)
        roundedContainer.addSubview(reactionSelector)
        roundedContainer.addSubview(commentCountLabel)
        cardContainer.addSubview(roundedContainer)
        addSubview(cardContainer)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        gradientLayer.frame = bounds
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "cardContainer": cardContainer,
            "roundedContainer": roundedContainer,
            "moreButton": moreButton,
            "topicLabel": topicLabel,
            "userInfoLabel": userInfoLabel,
            "dateLabel": dateLabel,
            "topicImageView": topicImageView,
            "reactionSelector": reactionSelector,
            "commentCountLabel": commentCountLabel,
            "reactionsDisplayView": reactionsDisplayView
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[reactionSelector(==280)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[reactionSelector(==32)]-10-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-55-[reactionsDisplayView]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[reactionsDisplayView(==20)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[reactionSelector]-(<=1)-[reactionsDisplayView]", options: .alignAllCenterY, metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[cardContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[cardContainer]-20-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[roundedContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[roundedContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[topicImageView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[commentCountLabel(==180)]-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[topicImageView][commentCountLabel(==50)]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[topicLabel]-60-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[topicLabel]-10-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-[userInfoLabel][dateLabel]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-[userInfoLabel]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-[dateLabel]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[moreButton(==44)]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[moreButton]", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}
