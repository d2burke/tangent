//
//  SignInFormView.swift
//  Tangent
//
//  Created by Daniel Burke on 3/13/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class SignInFormView: UIView {
    let usernameField = TATextField(viewModel: TATextFieldViewModel(text: "d2burke@gmail.com", placeholderText: "Username"))
    let passwordField = TATextField(viewModel: TATextFieldViewModel(text: "daniel34", placeholderText: "Password", secure: true))
    
    let loginButton: Button = {
        let button = Button(type: .custom)
        button.setTitle("Log In", for: .normal)
        button.defaultBackgroundColor = .orange
        button.layer.cornerRadius = 6
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(usernameField)
        addSubview(passwordField)
        addSubview(loginButton)
        
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    func installConstraints() {
        let views: [String: UIView] = [
            "usernameField": usernameField,
            "passwordField": passwordField,
            "loginButton": loginButton
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[usernameField(>=40)]-10-[passwordField(>=40)]-10-[loginButton(>=40)]", options: [.alignAllCenterX], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[usernameField]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[passwordField]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[loginButton(==170)]", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}
