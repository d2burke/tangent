//
//  ReactionSelector.swift
//  Tangent
//
//  Created by Daniel Burke on 1/7/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

enum ReactionType: String {
    case agree
    case disagree
    case goodPoint
    case offensive
    case apathetic
    case none
    
    init?(string: String) {
        self.init(rawValue: string)
    }
}

protocol ReactionSelectorDelegate: class {
    func didRelease(reaction: ReactionType)
    func didBeginReactionSelector()
}

class ReactionSelector: UIControl {
    weak var delegate: ReactionSelectorDelegate?
    var reaction = ReactionType.none
    let longPress = UILongPressGestureRecognizer()
    
    let reactionButton: Button = {
        let button = Button(type: .custom)
        button.setTitle("(", for: .normal)
        button.setTitleColor(TAColor.gray.color, for: .normal)
        button.titleLabel?.font = UIFont(name: "tangent", size: 16)
        button.layer.cornerRadius = 16
        return button
    }()
    
    let reactionsControl = ReactionsControl()
    
    init() {
        super.init(frame: .zero)
        longPress.addTarget(self, action: #selector(ReactionSelector.react))
        longPress.minimumPressDuration = 0.4
        reactionButton.addGestureRecognizer(longPress)
        reactionButton.addTarget(self, action: #selector(ReactionSelector.didTapReactionButton), for: .touchUpInside)
        reactionsControl.isHidden = true
        
        addSubview(reactionButton)
        addSubview(reactionsControl)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "reactionButton": reactionButton,
            "reactionsControl": reactionsControl
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[reactionButton(==32)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[reactionButton(==32)]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-(-5)-[reactionsControl(==270)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-(-60)-[reactionsControl(==50)]", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    func didTapReactionButton(button: UIButton) {
        if button.isSelected {
            setReactionButtonSelected(reaction: .none)
        } else {
            setReactionButtonSelected(reaction: .agree)
        }
    }
    
    func setReactionButtonSelected(reaction: ReactionType) {
        switch reaction {
        case .agree:
            reactionButton.setTitle("(", for: .normal)
            reactionButton.setTitleColor(TAColor.white.color, for: .normal)
            reactionButton.defaultBackgroundColor = .green
            reactionButton.highlightedBackgroundColor = .green
            reactionButton.isSelected = true
        case .disagree:
            reactionButton.setTitle(")", for: .normal)
            reactionButton.setTitleColor(TAColor.white.color, for: .normal)
            reactionButton.defaultBackgroundColor = .orange
            reactionButton.highlightedBackgroundColor = .orange
            reactionButton.isSelected = true
        case .goodPoint:
            reactionButton.setTitle("I", for: .normal)
            reactionButton.setTitleColor(TAColor.white.color, for: .normal)
            reactionButton.defaultBackgroundColor = .blue
            reactionButton.highlightedBackgroundColor = .blue
            reactionButton.isSelected = true
        case .offensive:
            reactionButton.setTitle("i", for: .normal)
            reactionButton.setTitleColor(TAColor.white.color, for: .normal)
            reactionButton.defaultBackgroundColor = .red
            reactionButton.highlightedBackgroundColor = .red
            reactionButton.isSelected = true
        case .apathetic:
            reactionButton.setTitle("-", for: .normal)
            reactionButton.setTitleColor(TAColor.dark.color, for: .normal)
            reactionButton.defaultBackgroundColor = .yellow
            reactionButton.highlightedBackgroundColor = .yellow
            reactionButton.isSelected = true
        case .none:
            reactionButton.setTitle("(", for: .normal)
            reactionButton.setTitleColor(TAColor.gray.color, for: .normal)
            reactionButton.defaultBackgroundColor = .clear
            reactionButton.isSelected = false
        }
        
        delegate?.didRelease(reaction: reaction)
    }
    
    func react(recognizer: UILongPressGestureRecognizer) {
        let location = recognizer.location(in: self)
        switch recognizer.state {
        case .began:
            let generator = UIImpactFeedbackGenerator(style: .medium)
            generator.impactOccurred()
            delegate?.didBeginReactionSelector()
            reactionsControl.isHidden = false
        case .changed:
            setActiveReaction(x: location.x)
        default:
            selectReaction(x: location.x)
        }
    }
    
    func setActiveReaction(x:CGFloat) {
        switch x {
        case reactionsControl.agreeButton.frame.origin.x...reactionsControl.disagreeButton.frame.origin.x:
            reactionsControl.setActive(reaction: .agree)
        case reactionsControl.disagreeButton.frame.origin.x...reactionsControl.goodPointButton.frame.origin.x:
            reactionsControl.setActive(reaction: .disagree)
        case reactionsControl.goodPointButton.frame.origin.x...reactionsControl.offensiveButton.frame.origin.x:
            reactionsControl.setActive(reaction: .goodPoint)
        case reactionsControl.offensiveButton.frame.origin.x...reactionsControl.mehButton.frame.origin.x:
            reactionsControl.setActive(reaction: .offensive)
        case reactionsControl.mehButton.frame.origin.x...(reactionsControl.mehButton.frame.origin.x + 80):
            reactionsControl.setActive(reaction: .apathetic)
        default:
            reactionsControl.setActive(reaction: .none)
        }
    }
    
    func selectReaction(x:CGFloat) {
        switch x {
        case reactionsControl.agreeButton.frame.origin.x...reactionsControl.disagreeButton.frame.origin.x:
            setReactionButtonSelected(reaction: .agree)
        case reactionsControl.disagreeButton.frame.origin.x...reactionsControl.goodPointButton.frame.origin.x:
            setReactionButtonSelected(reaction: .disagree)
        case reactionsControl.goodPointButton.frame.origin.x...reactionsControl.offensiveButton.frame.origin.x:
            setReactionButtonSelected(reaction: .goodPoint)
        case reactionsControl.offensiveButton.frame.origin.x...reactionsControl.mehButton.frame.origin.x:
            setReactionButtonSelected(reaction: .offensive)
        case reactionsControl.mehButton.frame.origin.x...(reactionsControl.mehButton.frame.origin.x + 80):
            setReactionButtonSelected(reaction: .apathetic)
        default:
            setReactionButtonSelected(reaction: .none)
            reactionsControl.setActive(reaction: .none)
        }
        reactionsControl.isHidden = true
    }
}
