//
//  TriangleView.swift
//  Tangent
//
//  Created by Daniel Burke on 1/7/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit
import CoreGraphics

enum Direction {
    case right
    case left
    case up
    case down
}

class TriangleView: UIView {
    var direction: Direction = .down {
        didSet {
            self.setNeedsDisplay()
        }
    }
    var arrowColor: UIColor = .white {
        didSet {
            if let ctx = context {
                ctx.setFillColor(arrowColor.cgColor)
            }
        }
    }
    var context = UIGraphicsGetCurrentContext()
    
    init(color: UIColor) {
        arrowColor = color
        super.init(frame: .zero)
        backgroundColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        var orientationPoint = rect.midX
        switch direction {
            case .left:
                orientationPoint = rect.minX
            case .right:
                orientationPoint = rect.maxX
            default:()
        }
        
        if let ctx = UIGraphicsGetCurrentContext() {
            self.context = ctx
            ctx.beginPath()
            ctx.move(to: CGPoint(x: rect.minX, y: rect.minY))
            ctx.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
            ctx.addLine(to: CGPoint(x: orientationPoint, y: rect.maxY))
            ctx.closePath()
            ctx.setFillColor(arrowColor.cgColor)
            ctx.fillPath()
        }
    }
}
