//
//  ReactionDisplayView.swift
//  Tangent
//
//  Created by Daniel Burke on 4/4/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

typealias ReactionDisplayState = ReactionType

class ReactionDisplayView: UIControl {
    
    var reactionState: ReactionDisplayState = .none {
        didSet {
            self.updateReactionState()
        }
    }
    
    private let reactionIcon: TALabel = {
        let icon = TALabel(frame: .zero)
        icon.textAlignment = .center
        icon.font = UIFont(name: "tangent", size: 80)
        return icon
    }()
    
    init(){
        super.init(frame: .zero)
        alpha = 0
        isUserInteractionEnabled = false
        addSubview(reactionIcon)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConstraints() {
        let views: [String: UIView] = ["reactionIcon": reactionIcon]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[reactionIcon]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[reactionIcon]|", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    private func updateReactionState() {
        switch self.reactionState {
        case .agree:
            reactionIcon.text = "("
            reactionIcon.textColor = .white
            backgroundColor = TAColor.green.color.withAlphaComponent(0.8)
        case .disagree:
            reactionIcon.text = ")"
            reactionIcon.textColor = .white
            backgroundColor = TAColor.orange.color.withAlphaComponent(0.8)
        case .goodPoint:
            reactionIcon.text = "I"
            reactionIcon.textColor = .white
            backgroundColor = TAColor.blue.color.withAlphaComponent(0.8)
        case .offensive:
            reactionIcon.text = "i"
            reactionIcon.textColor = .white
            backgroundColor = TAColor.red.color.withAlphaComponent(0.8)
        case .apathetic:
            reactionIcon.text = "-"
            reactionIcon.textColor = .black
            backgroundColor = TAColor.yellow.color.withAlphaComponent(0.8)
        case .none: ()
        }
        
        UIView.animate(withDuration: 0.2, animations: { 
            self.alpha = self.reactionState == .none ? 0 : 1
        })
    }
}
