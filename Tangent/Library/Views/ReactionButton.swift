//
//  ReactionButton.swift
//  Tangent
//
//  Created by Daniel Burke on 1/7/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ReactionButton: Button {
    
    let label = TALabel(viewModel: LabelViewModel(
        textColor: .dark,
        textStyle: .tiny,
        textAlignment: .center
    ))
    
    override var isSelected: Bool {
        didSet {
            //
        }
    }
    
    init(icon: String, color: TAColor, labelText: String, iconColor: UIColor = .white) {
        super.init(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        setTitle(icon, for: .normal)
        setTitleColor(iconColor, for: .normal)
        titleLabel!.font = UIFont(name: "tangent", size: 22)
        defaultBackgroundColor = color
        highlightedBackgroundColor = color
        layer.cornerRadius = 20
        clipsToBounds = false
        
        label.text = labelText
        addSubview(label)
        
        installConstraints()
    }
    
    func installConstraints() {
        let views = ["label": label ]
        blockAutoResizing(views: views.map({ $0.value }))
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:[label]-(-20)-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-(-20)-[label(==80)]-(-20)-|", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
