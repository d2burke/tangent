//
//  PageHeaderView.swift
//  Tangent
//
//  Created by Daniel Burke on 1/16/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class PageHeaderView: UIView {
    
    var actionButton: Button? = nil
    var actionButtonAction: TapAction? = nil
    
    public convenience init(viewModel: PageHeaderViewModel) {
        self.init()
        backgroundColor = .clear
        configure(with: viewModel)
    }
    
    public func configure(with viewModel: PageHeaderViewModel) {
        let searchTextField = TATextField(viewModel: TATextFieldViewModel(
            placeholderText: viewModel.searchFieldPlaceholderText,
            leftViewIcon: viewModel.searchFieldIcon
        ))
        addSubview(searchTextField)
        
        let pageHeaderView: UIView = {
            let view = UIView()
            let titleLabel = PageTitleLabel(viewModel:PageTitleLabelViewModel(text: viewModel.pageTitleText))
            titleLabel.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(titleLabel)
            
            let views: [String: UIView] = ["titleLabel": titleLabel]
            let constraints = [
                NSLayoutConstraint.constraints(withVisualFormat: "H:|[titleLabel]", options: [], metrics: nil, views: views),
                NSLayoutConstraint.constraints(withVisualFormat: "V:|[titleLabel]|", options: [], metrics: nil, views: views)
            ]
            NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
            return view
        }()
        addSubview(pageHeaderView)
        
        var views: [String: UIView] = ["searchTextField": searchTextField, "pageHeaderView": pageHeaderView]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        var constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-35-[searchTextField(==50)]-10-[pageHeaderView(==50)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[pageHeaderView]-15-|", options: [], metrics: nil, views: views)
        ]
        
        /**
         *  Properly handle the existence of an action button
         */
        if let buttonTitle = viewModel.actionButtonTitle {
            self.actionButtonAction = viewModel.actionButtonAction
            let actionButton: Button = {
                let button = Button(type: .custom)
                button.setTitle(buttonTitle, for: .normal)
                button.setTitleColor(TAColor.orange.color, for: .normal)
                button.titleLabel?.font = UIFont(name: "tangent", size: 28)
                button.clipsToBounds = true
                return button
            }()
            actionButton.addTarget(self, action: #selector(PageHeaderView.performButtonAction), for: .touchUpInside)
            addSubview(actionButton)
            
            views["actionButton"] = actionButton
            actionButton.translatesAutoresizingMaskIntoConstraints = false
            let buttonConstraints = [
                NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[searchTextField]-10-[actionButton(==60)]-15-|", options: [], metrics: nil, views: views),
                NSLayoutConstraint.constraints(withVisualFormat: "V:|-35-[actionButton(==50)]", options: [], metrics: nil, views: views)
            ]
            constraints.append(contentsOf: buttonConstraints)
        } else {
            let constraint = NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[searchTextField]-15-|", options: [], metrics: nil, views: views)
            constraints.append(constraint)
        }
        
        /**
         *  Finally, add all constraints
         */
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    func performButtonAction() {
        if let action = actionButtonAction {
            action()
        }
    }
}
