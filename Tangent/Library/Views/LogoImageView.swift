//
//  LogoImageView.swift
//  Tangent
//
//  Created by Daniel Burke on 12/30/16.
//  Copyright © 2016 Daniel Burke. All rights reserved.
//

import UIKit

class LogoImageView: UIImageView {
    init() {
        super.init(frame: .zero)

        image = UIImage(named: "logo")
        image = image?.withRenderingMode(.alwaysTemplate)
        tintColor = TAColor.orange.color
        contentMode = .scaleAspectFit
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
