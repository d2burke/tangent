//
//  ReactionsDisplay.swift
//  Tangent
//
//  Created by Daniel Burke on 4/14/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ReactionsDisplay: UIView {
    
    //reaction views
    let displayStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        return stackView
    }()
    
    lazy var reactionsView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 20))
        return view
    }()
    
    //reactions count
    lazy var countLabel: TALabel = {
        let label = TALabel(viewModel: LabelViewModel(textColor: .gray, textStyle: .boldSmall, textAlignment: .left))
        return label
    }()
    
    lazy var reactionsWidthConstraint: NSLayoutConstraint = {
        return NSLayoutConstraint(
            item: self.reactionsView,
            attribute: NSLayoutAttribute.width,
            relatedBy: NSLayoutRelation.equal,
            toItem: nil,
            attribute: NSLayoutAttribute.notAnAttribute,
            multiplier: 1,
            constant: 0
        )
    }()
    
    init() {
        super.init(frame: .zero)
        isUserInteractionEnabled = false
        displayStackView.addArrangedSubview(reactionsView)
        displayStackView.addArrangedSubview(countLabel)
        addSubview(displayStackView)
        installConstraints()
    }
    
    func configure(reactions: [Reaction], author: User) {
        let otherUserReactions = reactions.filter({ $0.author != author })
        
        countLabel.isHidden = true
        for view in reactionsView.subviews {
            view.removeFromSuperview()
        }
        
        if otherUserReactions.count == 0 { return }
        
        let uniqueReactions = Set<Reaction>(reactions)
        
        for reaction in uniqueReactions {
            let reactionView = self.viewForReaction(type: reaction.type)
            var frame = reactionView.frame
            if let index = Array(uniqueReactions).index(of: reaction) {
                frame.origin.x = CGFloat(index) * CGFloat(15)
                reactionView.frame = frame
                reactionsView.addSubview(reactionView)
            }
        }
        
        if reactionsView.subviews.count > 0 {
            isHidden = false
            countLabel.isHidden = false
            reactionsWidthConstraint.constant = CGFloat(reactionsView.subviews.count * 15) + 8
            countLabel.text = "\(reactions.count)"
        } else {
            isHidden = true
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "reactionsView": reactionsView,
            "displayStackView": displayStackView,
            "countLabel": countLabel
        ]
        blockAutoResizing(views: views.map({ $0.value }))
        let constraints = [
            [reactionsWidthConstraint],
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[reactionsView]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[displayStackView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[displayStackView]|", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    func viewForReaction(type: ReactionType) -> UIView {
        let view = TALabel()
        view.font = UIFont(name: "tangent", size: 10)
        view.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        view.textAlignment = .center
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
        view.layer.borderColor = TAColor.white.color.cgColor
        view.layer.borderWidth = 2
        switch type {
        case .agree:
            view.text = "("
            view.textColor = TAColor.white.color
            view.layer.backgroundColor = TAColor.green.color.cgColor
        case .disagree:
            view.text = ")"
            view.textColor = TAColor.white.color
            view.layer.backgroundColor = TAColor.orange.color.cgColor
        case .goodPoint:
            view.text = "I"
            view.textColor = TAColor.white.color
            view.layer.backgroundColor = TAColor.blue.color.cgColor
        case .offensive:
            view.text = "i"
            view.textColor = TAColor.white.color
            view.layer.backgroundColor = TAColor.red.color.cgColor
        case .apathetic:
            view.text = "-"
            view.textColor = TAColor.dark.color
            view.layer.backgroundColor = TAColor.yellow.color.cgColor
        case .none:
            view.text = "("
            view.textColor = TAColor.gray.color
            view.layer.backgroundColor = TAColor.clear.color.cgColor
        }
        return view
    }
}
