//
//  Button.swift
//  Tangent
//
//  Created by Daniel Burke on 2/12/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

typealias TapAction = () -> ()

enum HighlightType {
    case size(scale: Float)
    case color
}

class Button: UIButton {
    
    var defaultBackgroundColor: TAColor? = nil {
        didSet {
            backgroundColor = defaultBackgroundColor?.color
        }
    }
    
    var highlightedBackgroundColor: TAColor? = nil
    var highlightType: HighlightType = .color
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 6
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isSelected: Bool {
        didSet {
            backgroundColor = isSelected ? highlightedBackgroundColor?.color : defaultBackgroundColor?.color
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            switch isHighlighted {
            case true:
                switch highlightType {
                case let HighlightType.size(scale: scale):
                    UIView.animate(withDuration: 0.075, animations: {
                        self.transform = CGAffineTransform(scaleX: CGFloat(scale), y: CGFloat(scale))
                    })
                default:
                    if let highlightedBackground = highlightedBackgroundColor {
                        backgroundColor = highlightedBackground.color
                    } else {
                        backgroundColor = TAColor.medGray.color
                    }
                }
            case false:
                switch highlightType {
                case .size(scale: _):
                    UIView.animate(withDuration: 0.075, animations: {
                        self.transform = CGAffineTransform(scaleX: 1, y: 1)
                    })
                default:
                    if let defaultBackground = defaultBackgroundColor {
                        backgroundColor = defaultBackground.color
                    } else {
                        backgroundColor = TAColor.clear.color
                    }
                }
            }
        }
    }

}
