//
//  PageTitleLabel.swift
//  Tangent
//
//  Created by Daniel Burke on 12/29/16.
//  Copyright © 2016 Daniel Burke. All rights reserved.
//

import UIKit

class PageTitleLabel: UILabel {
    public convenience init(viewModel: PageTitleLabelViewModel) {
        self.init()
        configure(with: viewModel)
    }
    
    public func configure(with viewModel: PageTitleLabelViewModel) {
        font = TextStyle.pageTitle.font
        textColor = TAColor.dark.color
        
        text = viewModel.text
        textAlignment = viewModel.textAlignment
        numberOfLines = viewModel.numberOfLines
        lineBreakMode = viewModel.lineBreakMode
    }
}
