//
//  TATextField.swift
//  Tangent
//
//  Created by Daniel Burke on 12/30/16.
//  Copyright © 2016 Daniel Burke. All rights reserved.
//

import UIKit

class TATextField: UITextField {
    public convenience init(viewModel: TATextFieldViewModel) {
        self.init()
        configure(with: viewModel)
    }
    
    public func configure(with viewModel: TATextFieldViewModel) {
        layer.cornerRadius = 6
        
        text = viewModel.text
        placeholder = viewModel.placeholderText
        font = viewModel.textStyle.font
        textColor = viewModel.textColor.color
        backgroundColor = viewModel.backgroundColor.color
        isSecureTextEntry = viewModel.secure
        
        let width = viewModel.leftViewIcon == nil ? 15 : 40
        let searchIcon = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: 50))
        searchIcon.text = viewModel.leftViewIcon
        searchIcon.textColor = .gray
        searchIcon.textAlignment = .center
        searchIcon.font = UIFont(name: "tangent", size: 18)
        leftView = searchIcon
        leftViewMode = .always
        
        delegate = self
    }
}

extension UITextField: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        resignFirstResponder()
        return true
    }
}
