//
//  ConfigurableCell.swift
//  Tangent
//
//  Created by Daniel Burke on 3/15/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

protocol ConfigurableCell {
    var isPressed: Bool { get set }
    func configure(viewModel: IdentifiableViewModel)
}
