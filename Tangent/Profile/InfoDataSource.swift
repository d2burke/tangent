//
//  InfoDataSource.swift
//  Tangent
//
//  Created by Daniel Burke on 4/14/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

protocol InfoCell {
    func configure(viewModel: IdentifiableViewModel)
}

protocol InfoCellDelegate: class {
    func didTapInfoButton(button: Button)
}

class InfoDataSource: NSObject, UITableViewDelegate, UITableViewDataSource {
    weak var delegate: InfoCellDelegate? = nil
    var items = [IdentifiableViewModel]()
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellModel = items[indexPath.row]
        return CGFloat(cellModel.cellHeight)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellModel = items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: cellModel.identifier, for: indexPath) as! InfoCell
        cell.configure(viewModel: cellModel)
        
        if let logOutCell = cell as? InfoButtonCell {
            logOutCell.delegate = self
            return logOutCell
        }
        
        return cell as! UITableViewCell
    }
}

extension InfoDataSource: InfoButtonCellDelegate {
    func didTapButton(button: Button) {
        delegate?.didTapInfoButton(button: button)
    }
}
