//
//  Interest.swift
//  Tangent
//
//  Created by Daniel Burke on 3/15/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

struct Interest {
    let name: String
    
    init(name: String = "") {
        self.name = name
    }
}
