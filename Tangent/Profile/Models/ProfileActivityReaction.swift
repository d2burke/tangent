//
//  ProfileActivityReaction.swift
//  Tangent
//
//  Created by Daniel Burke on 4/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

struct ProfileActivityReaction: ProfileActivityItem {
    let type: ReactionType
    let author: User
    let postTitle: String
    let creationDate: Date
    
    init(type: ReactionType, author: User, postTitle: String, creationDate: Date) {
        self.type = type
        self.author = author
        self.postTitle = postTitle
        self.creationDate = creationDate
    }
}
