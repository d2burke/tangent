//
//  User.swift
//  Tangent
//
//  Created by Daniel Burke on 1/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

struct User: Equatable {
    let userId: String
    let firstName: String
    let lastName: String
    let userName: String
    let avatarURL: String
    let createdAt: Date
    
    init(firstName: String = "", lastName: String = "", userName: String = "", avatarURL: String = "", createdAt: Date = Date()) {
        self.userId = "cj1vhthdyvg6j0123ci0xrru6"
        self.firstName = firstName
        self.lastName = lastName
        self.userName = userName
        self.avatarURL = avatarURL
        self.createdAt = createdAt
    }
    
    static func ==(lhs: User, rhs: User) -> Bool {
        return lhs.firstName == rhs.firstName
    }
}
