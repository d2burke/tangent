//
//  LoginViewController.swift
//  Tangent
//
//  Created by Daniel Burke on 12/29/16.
//  Copyright © 2016 Daniel Burke. All rights reserved.
//

import UIKit
import Firebase

protocol LoginViewControllerDelegate: class {
    func didLogin()
}

class LoginViewController: UIViewController {
    weak var delegate: LoginViewControllerDelegate? = nil
    
    let logoImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "logo").withRenderingMode(.alwaysTemplate))
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = TAColor.orange.color
        return imageView
    }()
    
    let titleLabel = TALabel(viewModel: LabelViewModel(text: "Tangent", textStyle: .pageTitle))
    
    let textContainerView = UIView(frame: .zero)
    
    let reactTextlabel = TALabel(viewModel: LabelViewModel(
        text: "React: Here's what the world is talking about. What's your initial reaction?",
        textStyle: .paragraph,
        numberOfLines: 0
    ))
    
    let personalizeTextlabel = TALabel(viewModel: LabelViewModel(
        text: "Personalize: What are you interested in discussing?",
        textStyle: .paragraph,
        numberOfLines: 0
    ))
    
    let locationTextlabel = TALabel(viewModel: LabelViewModel(
        text: "Location: What are people talking about in other areas?",
        textStyle: .paragraph,
        numberOfLines: 0
    ))
    
    let onboardingCollectionView = OnboardingCollectionView(frame: .zero)
    lazy var onboardingBottomConstraint: NSLayoutConstraint = {
        return NSLayoutConstraint(
            item: self.onboardingCollectionView,
            attribute: NSLayoutAttribute.bottom,
            relatedBy: NSLayoutRelation.greaterThanOrEqual,
            toItem: self.signInButton,
            attribute: NSLayoutAttribute.top,
            multiplier: 1,
            constant: -30
        )
    }()
    
    let signInButton: Button = {
        let button = Button(type: .custom)
        button.setTitle("Sign Up", for: .normal)
        button.setTitleColor(TAColor.white.color, for: .normal)
        button.highlightedBackgroundColor = .lightOrange
        button.defaultBackgroundColor = .orange
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.layer.cornerRadius = 6
        view.backgroundColor = TAColor.lightGray.color
        
        if let reactText = reactTextlabel.text {
            let attributes = [NSForegroundColorAttributeName: TAColor.orange.color, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 18)]
            let attributedText = NSMutableAttributedString(string: reactText)
            attributedText.addAttributes(attributes, range: NSMakeRange(0, 6))
            reactTextlabel.attributedText = attributedText
        }
        
        if let personalizeText = personalizeTextlabel.text {
            let attributes = [NSForegroundColorAttributeName: TAColor.orange.color, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 18)]
            let attributedText = NSMutableAttributedString(string: personalizeText)
            attributedText.addAttributes(attributes, range: NSMakeRange(0, 11))
            personalizeTextlabel.attributedText = attributedText
            personalizeTextlabel.alpha = 0
        }
        
        if let locationText = locationTextlabel.text {
            let reactAttributes = [NSForegroundColorAttributeName: TAColor.orange.color, NSFontAttributeName: UIFont.boldSystemFont(ofSize: 18)]
            let reactAttributedText = NSMutableAttributedString(string: locationText)
            reactAttributedText.addAttributes(reactAttributes, range: NSMakeRange(0, 9))
            locationTextlabel.attributedText = reactAttributedText
            locationTextlabel.alpha = 0
            
            let nsText = locationText as NSString
            let textRange = NSMakeRange(0, nsText.length)
            _ = NSMutableAttributedString(string: locationText)
            
            nsText.enumerateSubstrings(in: textRange, options: .byWords, using: { [weak self]
                (substring, substringRange, _, _) in
                
                if (substring == "other") {
                    reactAttributedText.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 18), range: substringRange)
                }
                self?.locationTextlabel.attributedText = reactAttributedText
            })
            
        }
        
        view.addSubview(titleLabel)
        view.addSubview(logoImageView)
        
        textContainerView.addSubview(reactTextlabel)
        textContainerView.addSubview(personalizeTextlabel)
        textContainerView.addSubview(locationTextlabel)
        view.addSubview(textContainerView)
        
        onboardingCollectionView.clipsToBounds = false
        onboardingCollectionView.delegate = self
        onboardingCollectionView.dataSource = self
        view.addSubview(onboardingCollectionView)
        view.addSubview(signInButton)
        signInButton.addTarget(self, action: #selector(LoginViewController.login), for: .touchUpInside)
        
        installConstraints()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func login() {
//        if let email = "d2burke@gmail.com" //formView.usernameField.text,
//            let password = "daniel34" //formView.passwordField.text,
        if let auth = FIRAuth.auth() {
            auth.signIn(withEmail: "d2burke@gmail.com", password: "daniel34") { user, error in
                if error == nil {
                    self.view.endEditing(true)
                    self.delegate?.didLogin()
                }
            }
        }
    }
    
//    func register() {
//        print("register")
//        if let email = formView.usernameField.text,
//            let password = formView.passwordField.text,
//            let auth = FIRAuth.auth() {
//            auth.createUser(withEmail: email, password: password) { user, error in
//                if error == nil {
//                    self.view.endEditing(true)
//                    self.dismiss(animated: true, completion: nil)
//                } else {
//                    print("Error: \(error)")
//                }
//            }
//        }
//    }
    
    @IBAction func signOut(segue: UIStoryboardSegue) {
        
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "titleLabel": titleLabel,
            "logoImageView": logoImageView,
            "textContainerView": textContainerView,
            "reactTextlabel": reactTextlabel,
            "personalizeTextlabel": personalizeTextlabel,
            "locationTextlabel": locationTextlabel,
            "onboardingCollectionView": onboardingCollectionView,
            "signInButton": signInButton
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-40-[titleLabel(==60)][reactTextlabel]-20-[onboardingCollectionView]", options: [], metrics: nil, views: views),
            [onboardingBottomConstraint],
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-50-[logoImageView(==40)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[onboardingCollectionView]|", options: [], metrics: nil, views: views),
            
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-100-[textContainerView]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[reactTextlabel]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[personalizeTextlabel]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[locationTextlabel]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[textContainerView]-25-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[reactTextlabel]-25-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[personalizeTextlabel]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[locationTextlabel]|", options: [], metrics: nil, views: views),
            
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[titleLabel]-25-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-100-[signInButton]-100-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[signInButton(==50)]-30-|", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}

//MARK: Keyboard Methods
extension LoginViewController {
    func keyboardWillShow(notification: Notification) {
        guard let keyboardFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        let keyboardHeight = view.convert(keyboardFrame, from:nil).size.height
        let keyboardAnimationDuration = ((notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0)
        
        onboardingBottomConstraint.constant = -(40 + keyboardHeight)
        UIView.animate(withDuration: keyboardAnimationDuration, animations: {
            self.titleLabel.alpha = 0
            self.view.layoutIfNeeded()
        })
    }
    
    func keyboardWillHide(notification: Notification) {
        let keyboardAnimationDuration = ((notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0)
        onboardingBottomConstraint.constant = -20
        UIView.animate(withDuration: keyboardAnimationDuration, animations: {
            self.titleLabel.alpha = 1
            self.view.layoutIfNeeded()
        })
    }
}

extension LoginViewController: OnboardingReactionCellDelegate {
    func didReact(type: ReactionType) {
        let pageWidth = self.onboardingCollectionView.frame.size.width
        let positions = Int(self.onboardingCollectionView.contentSize.width / pageWidth)
        let currentPosition = Int(self.onboardingCollectionView.contentOffset.x / pageWidth)
        
        guard currentPosition+1 < positions else { return }
        
        let nextXPosition = pageWidth * CGFloat(currentPosition+1)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            UIView.animate(withDuration: 0.2, animations: {
                self.onboardingCollectionView.contentOffset = CGPoint(x: nextXPosition, y: 0)
            })
        }
    }
}
