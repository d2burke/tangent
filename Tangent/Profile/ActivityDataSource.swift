//
//  ActivityDataSource.swift
//  Tangent
//
//  Created by Daniel Burke on 4/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation
import UIKit

protocol ActivityCell {
    func configure(viewModel: IdentifiableViewModel)
}

class ActivityDataSource: NSObject, UITableViewDelegate, UITableViewDataSource {
    var items = [IdentifiableViewModel]() //Reactions, Comments, Shares, Saves
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellModel = items[indexPath.row]
        return CGFloat(cellModel.cellHeight)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellModel = items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: cellModel.identifier, for: indexPath) as! ActivityCell
        cell.configure(viewModel: cellModel)
        return cell as! UITableViewCell
    }
}


