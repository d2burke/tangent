//
//  OnboardingCollectionViewDataSource.swift
//  Tangent
//
//  Created by Daniel Burke on 4/5/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation
import UIKit

extension LoginViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return onboarding.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellViewModel = onboarding[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellViewModel.identifier, for: indexPath) as! ConfigurableCell
        cell.configure(viewModel: cellViewModel)
        
        if let reactionCell = cell as? OnboardingReactionCell {
            reactionCell.delegate = self
        }
        
        return cell as! UICollectionViewCell
    }
}

extension LoginViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView is OnboardingCollectionView {
            let position = scrollView.contentOffset.x/view.frame.size.width
            
            /*
                If reaction isn't set for current card and user swipes card
                left, we'll automatically react with "meh"
             */
            
            switch position {
            case 2...3:
                let remainder = 1 - (3 - position)
                reactTextlabel.alpha = 1 - remainder
                personalizeTextlabel.alpha = remainder
            case 3...4:
                let remainder = 1 - (4 - position)
                personalizeTextlabel.alpha = 1 - remainder
                reactTextlabel.alpha = remainder
            case 6...7:
                let remainder = 1 - (7 - position)
                reactTextlabel.alpha = 1 - remainder
                locationTextlabel.alpha = remainder
            case 7...8:
                let remainder = 1 - (8 - position)
                locationTextlabel.alpha = 1 - remainder
                reactTextlabel.alpha = remainder
            default: ()
            }
        }
    }
}
