//
//  InfoButtonCell.swift
//  Tangent
//
//  Created by Daniel Burke on 4/14/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

protocol InfoButtonCellDelegate: class {
    func didTapButton(button: Button)
}

class InfoButtonCell: UITableViewCell, InfoCell {
    
    weak var delegate: InfoButtonCellDelegate? = nil
    
    lazy var logoutButton: Button = {
        let button = Button(type: .custom)
        button.setTitle("Log Out", for: .normal)
        button.setTitleColor(TAColor.dark.color, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.addTarget(self, action: #selector(didTapButton(button:)), for: .touchUpInside)
        return button
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        addSubview(logoutButton)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        guard let cellModel = viewModel as? InfoButtonCellModel else { return }
        logoutButton.setTitle(cellModel.buttonText, for: .normal)
    }
    
    func installConstraints() {
        let views: [String: UIView] = ["logoutButton": logoutButton]
        
        blockAutoResizing(views: views.map({ $0.value }))
        //
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[logoutButton]-5-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[logoutButton(==120)]-15-|", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    func didTapButton(button: Button) {
        delegate?.didTapButton(button: button)
    }
}
