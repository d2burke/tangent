//
//  ProfileSavedCell.swift
//  Tangent
//
//  Created by Daniel Burke on 4/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ProfileSavedCell: UICollectionViewCell, ProfileCell {
    var items = [IdentifiableViewModel]()
    
    func configure(viewModel: IdentifiableViewModel) {
        guard let cellModel = viewModel as? ProfileSavedCellModel else { return }
        items = cellModel.items
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConstraints() {
        //
    }
}
