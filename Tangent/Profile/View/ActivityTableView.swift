//
//  ActivityTableView.swift
//  Tangent
//
//  Created by Daniel Burke on 4/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ActivityTableView: UITableView {
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: .plain)
        backgroundColor = .clear
        separatorStyle = .none
        register(ActivityReactionCell.self, forCellReuseIdentifier: "reaction")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
