//
//  OnboardingCollectionView.swift
//  Tangent
//
//  Created by Daniel Burke on 3/13/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class OnboardingCollectionView: UICollectionView {
    let layout: UICollectionViewFlowLayout = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        return layout
    }()
    
    init(frame: CGRect) {
        super.init(frame: frame, collectionViewLayout: layout)
        backgroundColor = .clear
        clipsToBounds = false
        isPagingEnabled = true
        delaysContentTouches = false
        showsHorizontalScrollIndicator = false
        register(OnboardingReactionCell.self, forCellWithReuseIdentifier: "reactionCell")
        register(OnboardingInterestsCell.self, forCellWithReuseIdentifier: "interestsCell")
        register(OnboardingLocationCell.self, forCellWithReuseIdentifier: "locationCell")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
