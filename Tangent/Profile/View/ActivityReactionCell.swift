//
//  ActivityReactionCell.swift
//  Tangent
//
//  Created by Daniel Burke on 4/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ActivityReactionCell: UITableViewCell, ActivityCell {
    
    let reactionImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = TAColor.lightGray.color
        imageView.layer.cornerRadius = 6
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let reactionIconView: TALabel = {
        let label = TALabel(viewModel: LabelViewModel(text: ")", textColor: .white, textStyle: .small, textAlignment: .center))
        label.layer.cornerRadius = 15
        label.clipsToBounds = true
        label.font = UIFont(name: "tangent", size: 12)
        label.layer.borderColor = TAColor.white.color.cgColor
        label.layer.borderWidth = 4
        return label
    }()
    
    let reactionTextLabel = TALabel(viewModel: LabelViewModel(
        text: "Heather agreed with your post: \"The nuclear option will only hurt the GOP.\"",
        textStyle: .paragraph,
        numberOfLines: 2
    ))
    
    let timeLabel = TALabel(viewModel: LabelViewModel(
        text: "4 hours ago",
        textColor: .lightGray,
        textStyle: .small
    ))
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        addSubview(reactionImageView)
        addSubview(reactionIconView)
        addSubview(reactionTextLabel)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        guard let cellModel = viewModel as? ActivityReactionCellModel else { return }
        reactionTextLabel.text = cellModel.postTitle
        reactionIconView.text = cellModel.reactionIcon
        switch cellModel.reactionIcon {
        case "(":
            reactionIconView.textColor = TAColor.white.color
            reactionIconView.layer.backgroundColor = TAColor.green.color.cgColor
        case ")":
            reactionIconView.textColor = TAColor.white.color
            reactionIconView.layer.backgroundColor = TAColor.orange.color.cgColor
        case "I":
            reactionIconView.textColor = TAColor.white.color
            reactionIconView.layer.backgroundColor = TAColor.blue.color.cgColor
        case "i":
            reactionIconView.textColor = TAColor.white.color
            reactionIconView.layer.backgroundColor = TAColor.red.color.cgColor
        case "-":
            reactionIconView.textColor = TAColor.dark.color
            reactionIconView.layer.backgroundColor = TAColor.yellow.color.cgColor
        default: ()
        }
        /*
         reactionImageView.image = UIImage(named: cellModel.reactionImage)
         reactionIconView.text = cellModel.reactionIcon
         reactionTextLabel.text = cellModel.reactionText
         timeLabel.text = cellModel.reactionTime
         */
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "reactionTextLabel": reactionTextLabel,
            "reactionImageView": reactionImageView,
            "reactionIconView": reactionIconView
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[reactionTextLabel]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[reactionImageView(==60)]-15-[reactionTextLabel]-15-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[reactionImageView]-(-15)-[reactionIconView(==30)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[reactionImageView]-(<=1)-[reactionIconView]", options: .alignAllCenterY, metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[reactionIconView(==30)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[reactionImageView(==60)]-10-|", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
        
    }
}
