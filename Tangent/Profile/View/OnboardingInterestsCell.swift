//
//  OnboardingInterestsCell.swift
//  Tangent
//
//  Created by Daniel Burke on 3/15/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class OnboardingInterestsCell: UICollectionViewCell, ConfigurableCell {
    var selectedInterests = Set<IndexPath>()
    var cellModel: OnboardingInterestsCellModel? = nil
    var interests = [InterestCellModel]()
    
    var isPressed: Bool = false {
        didSet {
            //
        }
    }
    
    let roundedContainer: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 6
        view.clipsToBounds = true
        return view
    }()
    
    let cardContainer: UIView = {
        let view = UIView()
        view.backgroundColor = TAColor.white.color
        view.layer.cornerRadius = 6
        view.layer.shadowOpacity = 0
        view.layer.shadowRadius = 7
        view.layer.shadowOffset = CGSize(width: 0, height: 7)
        view.layer.shadowColor = TAColor.dark.color.cgColor
        return view
    }()
    
    let interestsCollectionView = InterestsCollectionView(frame: .zero)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        
        interestsCollectionView.delegate = self
        interestsCollectionView.dataSource = self
        roundedContainer.addSubview(interestsCollectionView)
        cardContainer.addSubview(roundedContainer)
        addSubview(cardContainer)
        
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        
        //TODO: The problem wth this is that we won't get a configured cell
        //      if this fails
        guard let cellModel = viewModel as? OnboardingInterestsCellModel else { return }
        self.cellModel = cellModel
        if let interests = self.cellModel?.interests {
            self.interests = interests
            interestsCollectionView.reloadData()
        }
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "cardContainer": cardContainer,
            "roundedContainer": roundedContainer,
            "interestsCollectionView": interestsCollectionView
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[cardContainer]-25-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[cardContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[roundedContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[roundedContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[interestsCollectionView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[interestsCollectionView]|", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}

extension OnboardingInterestsCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! InterestCell
        cell.isHighlighted = true
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! InterestCell
        cell.isHighlighted = false
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectedInterests.contains(indexPath) {
            selectedInterests.remove(indexPath)
        } else {
            selectedInterests.insert(indexPath)
        }
        interestsCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return interests.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellModel = interests[indexPath.row]
        let text = NSString(string: cellModel.interest.name)
        let width = text.size(attributes: [NSFontAttributeName: TextStyle.button.font]).width + 30
        return CGSize(width: width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellViewModel = interests[indexPath.row]
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellViewModel.identifier, for: indexPath) as! ConfigurableCell
        cell.configure(viewModel: cellViewModel)
        cell.isPressed = selectedInterests.contains(indexPath)
        return cell as! UICollectionViewCell
    }
}
