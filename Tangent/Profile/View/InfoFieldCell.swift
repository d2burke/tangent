//
//  InfoFieldCell.swift
//  Tangent
//
//  Created by Daniel Burke on 4/14/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class InfoFieldCell: UITableViewCell, InfoCell {
    
    let fieldIcon: TALabel = {
        let field = TALabel(viewModel: LabelViewModel(
            text: "u",
            textColor: .gray,
            textAlignment: .center
        ))
        field.font = UIFont(name: "tangent", size: 20)
        return field
    }()
    
    let infoField = TATextField(viewModel: TATextFieldViewModel(
        textStyle: .field,
        placeholderColor: .dark,
        backgroundColor: .medGray
    ))
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        
        infoField.returnKeyType = .done
        infoField.clearButtonMode = .whileEditing
        addSubview(infoField)
        addSubview(fieldIcon)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        guard let cellModel = viewModel as? InfoFieldCellModel else { return }
        fieldIcon.text = cellModel.fieldIcon
        
        infoField.text = cellModel.fieldText
        infoField.placeholder = cellModel.placeHolderText
        infoField.isSecureTextEntry = cellModel.isSecure
        infoField.backgroundColor = TAColor.medGray.color
    }
    
    func installConstraints() {
        let views: [String: UIView] = ["fieldIcon": fieldIcon, "infoField": infoField]
        
        blockAutoResizing(views: views.map({ $0.value }))
        //
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-15-[fieldIcon(==40)]-15-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[infoField(==40)]-8-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[fieldIcon(==40)]-15-[infoField]-15-|", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}
