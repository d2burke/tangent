//
//  OnboardingCardView.swift
//  Tangent
//
//  Created by Daniel Burke on 3/16/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class OnboardingCardView: UIView {
    
    var shadowOpacity: Float = 0 {
        didSet {
            self.cardContainer.layer.shadowOpacity = shadowOpacity
        }
    }
    
    let roundedContainer: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 6
        view.clipsToBounds = true
        return view
    }()
    
    let cardContainer: UIView = {
        let view = UIView()
        view.backgroundColor = TAColor.white.color
        view.layer.cornerRadius = 6
        view.layer.shadowOpacity = 0
        view.layer.shadowRadius = 7
        view.layer.shadowOffset = CGSize(width: 0, height: 7)
        view.layer.shadowColor = TAColor.dark.color.cgColor
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        backgroundColor = .white
        cardContainer.addSubview(roundedContainer)
        addSubview(cardContainer)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addContent(_ view: UIView) {
        roundedContainer.addSubview(view)
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "cardContainer": cardContainer,
            "roundedContainer": roundedContainer
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[cardContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[cardContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[roundedContainer]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[roundedContainer]|", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}
