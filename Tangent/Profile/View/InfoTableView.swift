//
//  InfoTableView.swift
//  Tangent
//
//  Created by Daniel Burke on 4/14/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class InfoTableView: UITableView {
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: .plain)
        backgroundColor = .clear
        separatorStyle = .none
        register(InfoFieldCell.self, forCellReuseIdentifier: "infoField")
        register(InfoButtonCell.self, forCellReuseIdentifier: "infoButton")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
