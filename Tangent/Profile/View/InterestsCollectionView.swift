//
//  InterestsCollectionView.swift
//  Tangent
//
//  Created by Daniel Burke on 3/15/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class InterestsCollectionView: UICollectionView {
    let layout = FlowLayout()
    
    init(frame: CGRect) {
        super.init(frame: frame, collectionViewLayout: layout)
        backgroundColor = .clear
        delaysContentTouches = false
        showsHorizontalScrollIndicator = false
        layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        register(InterestCell.self, forCellWithReuseIdentifier: "interestCell")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class FlowLayout: UICollectionViewFlowLayout {
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributesForElementsInRect = super.layoutAttributesForElements(in: rect)
        var newAttributesForElementsInRect = [UICollectionViewLayoutAttributes]()
        
        var leftMargin: CGFloat = 0.0;
        for attributes in attributesForElementsInRect! {
            let refAttributes = attributes
            
            if (refAttributes.frame.origin.x == self.sectionInset.left) {
                leftMargin = self.sectionInset.left
            } else {
                
                var newLeftAlignedFrame = refAttributes.frame
                newLeftAlignedFrame.origin.x = leftMargin
                refAttributes.frame = newLeftAlignedFrame
            }
            
            leftMargin += refAttributes.frame.size.width + 8
            newAttributesForElementsInRect.append(refAttributes)
        }
        return newAttributesForElementsInRect
    }
}
