//
//  InterestCell.swift
//  Tangent
//
//  Created by Daniel Burke on 3/15/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class InterestCell: UICollectionViewCell, ConfigurableCell {
    var cellModel: InterestCellModel? = nil
    var isPressed: Bool = false {
        didSet {
            self.interestLabel.backgroundColor = isPressed ? TAColor.orange.color : TAColor.gray.color
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            self.interestLabel.backgroundColor = TAColor.lightOrange.color
        }
    }
    
    let interestLabel: TALabel = {
        let label = TALabel(viewModel: LabelViewModel(text: "Politics", textStyle: .button))
        label.textColor = TAColor.white.color
        label.layer.cornerRadius = 3
        label.backgroundColor = TAColor.gray.color
        label.textAlignment = .center
        label.clipsToBounds = true
        label.isUserInteractionEnabled = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 3
        clipsToBounds = true
        
        addSubview(interestLabel)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        guard let cellModel = viewModel as? InterestCellModel else { return }
        self.cellModel = cellModel
        interestLabel.text = cellModel.interest.name
    }
    
    func installConstraints() {
        let views = [ "interestLabel": interestLabel ]
        interestLabel.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[interestLabel]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[interestLabel]|", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}
