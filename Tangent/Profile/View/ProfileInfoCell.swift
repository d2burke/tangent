//
//  ProfileInfoCell.swift
//  Tangent
//
//  Created by Daniel Burke on 4/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

protocol ProfileCellDelegate: class {
    func didTapInfoButton(button: Button)
}

class ProfileInfoCell: UICollectionViewCell, ProfileCell {
    weak var delegate: ProfileCellDelegate? = nil
    var items = [IdentifiableViewModel]() {
        didSet {
            self.infoDataSource.items = items
            self.infoTableView.reloadData()
        }
    }
    
    let infoTableView = InfoTableView()
    let infoDataSource = InfoDataSource()
    
    func configure(viewModel: IdentifiableViewModel) {
        guard let cellModel = viewModel as? ProfileInfoCellModel else { return }
        items = cellModel.items
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        infoDataSource.delegate = self
        infoTableView.delegate = infoDataSource
        infoTableView.dataSource = infoDataSource
        addSubview(infoTableView)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "infoTableView": infoTableView
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[infoTableView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[infoTableView]|", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}

extension ProfileInfoCell: InfoCellDelegate {
    func didTapInfoButton(button: Button) {
        delegate?.didTapInfoButton(button: button)
    }
}
