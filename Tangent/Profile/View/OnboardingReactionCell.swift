//
//  OnboardingReactionCell.swift
//  Tangent
//
//  Created by Daniel Burke on 3/15/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

protocol OnboardingReactionCellDelegate: class {
    func didReact(type: ReactionType)
}

class OnboardingReactionCell: UICollectionViewCell, ConfigurableCell {
    
    var isPressed: Bool = false {
        didSet {
            //
        }
    }
    
    let cardView = OnboardingCardView(frame: .zero)
    let reactionDisplayView = ReactionDisplayView()
    
    weak var delegate: OnboardingReactionCellDelegate? = nil
    var topicImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "tweeting")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = false
        return imageView
    }()

    let postTextLabel = TALabel(viewModel: LabelViewModel(
        numberOfLines: 0
    ))

    let agreeButton = ReactionButton(icon: "(", color: .green, labelText: "Agree")
    let disagreeButton = ReactionButton(icon: ")", color: .orange, labelText: "Disagree")
    let goodPointButton = ReactionButton(icon: "I", color: .blue, labelText: "Good Point!")
    let mehButton = ReactionButton(icon: "-", color: .yellow, labelText: "Apathetic", iconColor: TAColor.dark.color)

    lazy var reactionSelector: UIStackView = {
        let view = UIStackView()
        view.alignment = .center
        view.distribution = .equalCentering
        view.backgroundColor = TAColor.lightOrange.color
        
        self.agreeButton.highlightType = .size(scale: 1.5)
        self.agreeButton.addTarget(self, action: #selector(didTapReactionButton), for: .touchUpInside)
        view.addArrangedSubview(self.agreeButton)
        self.disagreeButton.highlightType = .size(scale: 1.5)
        self.disagreeButton.addTarget(self, action: #selector(didTapReactionButton), for: .touchUpInside)
        view.addArrangedSubview(self.disagreeButton)
        self.goodPointButton.highlightType = .size(scale: 1.5)
        self.goodPointButton.addTarget(self, action: #selector(didTapReactionButton), for: .touchUpInside)
        view.addArrangedSubview(self.goodPointButton)
        self.mehButton.highlightType = .size(scale: 1.5)
        self.mehButton.addTarget(self, action: #selector(didTapReactionButton), for: .touchUpInside)
        view.addArrangedSubview(self.mehButton)
        
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        
        cardView.addContent(topicImageView)
        postTextLabel.adjustsFontSizeToFitWidth = true
        cardView.addContent(postTextLabel)
        cardView.addContent(reactionDisplayView)
        cardView.addSubview(reactionSelector)
        addSubview(cardView)
        
        installConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        guard let cellModel = viewModel as? OnboardingReactionCellModel else { return }
        postTextLabel.text = cellModel.text
        topicImageView.image = UIImage(named: cellModel.imageURL)
        reactionDisplayView.reactionState = .none
    }

    func installConstraints() {
        let views: [String: UIView] = [
            "agreeButton": agreeButton,
            "disagreeButton": disagreeButton,
            "goodPointButton": goodPointButton,
            "mehButton": mehButton,
            
            "topicImageView": topicImageView,
            "postTextLabel": postTextLabel,
            "reactionSelector": reactionSelector,
            "reactionDisplayView": reactionDisplayView,
            "cardView": cardView
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[cardView]-25-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[cardView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[topicImageView]-10-[postTextLabel(==70)]-10-[reactionSelector(==50)]-25-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[topicImageView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[postTextLabel]-15-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[reactionSelector]-15-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[agreeButton(==40)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[agreeButton(==40)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[disagreeButton(==40)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[disagreeButton(==40)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[goodPointButton(==40)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[goodPointButton(==40)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[mehButton(==40)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[mehButton(==40)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[reactionDisplayView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[reactionDisplayView]|", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    func didTapReactionButton(button: ReactionButton) {
        var reactionType: ReactionDisplayState
        switch button {
        case agreeButton:
            reactionType = .agree
        case disagreeButton:
            reactionType = .disagree
        case goodPointButton:
            reactionType = .goodPoint
        case mehButton:
             reactionType = .apathetic
        default:
            reactionType = .none
        }
        reactionDisplayView.reactionState = reactionType
        delegate?.didReact(type: reactionType)
    }
}
