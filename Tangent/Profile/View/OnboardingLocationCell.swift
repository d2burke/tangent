//
//  OnboardingLocationCell.swift
//  Tangent
//
//  Created by Daniel Burke on 3/16/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class OnboardingLocationCell: UICollectionViewCell, ConfigurableCell {
    var isPressed: Bool = false {
        didSet {
            //
        }
    }
    
    let cardView = OnboardingCardView(frame: .zero)
    let contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalCentering
        return stackView
    }()
    
    let mapImageView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "usa-map").withRenderingMode(.alwaysTemplate))
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = TAColor.orange.color
        imageView.alpha = 0.06
        return imageView
    }()
    
    let locationInfoLabel = TALabel(viewModel: LabelViewModel(
        text: "Enter your zip code or tap 'Allow Location' to see discussions in other parts of the country.",
        textStyle: .message,
        numberOfLines: 0,
        textAlignment: .center
    ))
    
    let zipCodeField = TATextField(viewModel: TATextFieldViewModel(
        placeholderText: "Zip Code",
        leftViewIcon: "S"
    ))
    
    let locationButton: Button = {
        let button = Button(type: .custom)
        button.setTitle("Use My Location", for: .normal)
        button.setTitleColor(TAColor.white.color, for: .normal)
        button.highlightedBackgroundColor = .lightBlue
        button.defaultBackgroundColor = .blue
        button.titleLabel?.font = TextStyle.small.font
        return button
    }()
    
    let skipButton: Button = {
        let button = Button(type: .custom)
        button.setTitle("Skip", for: .normal)
        button.setTitleColor(TAColor.gray.color, for: .normal)
        button.highlightedBackgroundColor = .lightGray
        button.defaultBackgroundColor = .clear
        button.titleLabel?.font = TextStyle.small.font
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        
        cardView.addContent(mapImageView)
        zipCodeField.returnKeyType = .done
        contentStackView.addArrangedSubview(locationInfoLabel)
        contentStackView.addArrangedSubview(zipCodeField)

        let bottomRow = UIStackView()
        bottomRow.addArrangedSubview(locationButton)
        bottomRow.addArrangedSubview(skipButton)
        
        contentStackView.addArrangedSubview(bottomRow)
        cardView.addContent(contentStackView)
        addSubview(cardView)
        
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(viewModel: IdentifiableViewModel) {
        guard viewModel is OnboardingLocationCellModel else { return }
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "cardView": cardView,
            "mapImageView": mapImageView,
            "contentStackView": contentStackView,
            
            "locationInfoLabel": locationInfoLabel,
            "zipCodeField": zipCodeField,
            "locationButton": locationButton,
            "skipButton": skipButton
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:[cardView]-25-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[cardView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[mapImageView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[mapImageView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[contentStackView]-20-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[contentStackView(>=200)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[cardView]-(<=1)-[contentStackView]", options: .alignAllCenterY, metrics: nil, views: views),
            
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[locationInfoLabel]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[locationInfoLabel(==60)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[zipCodeField]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[zipCodeField(==50)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:[skipButton(==80)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[skipButton(==40)]", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[locationButton(==40)]", options: [], metrics: nil, views: views)
        ]
        
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}
