//
//  ProfileCollectionView.swift
//  Tangent
//
//  Created by Daniel Burke on 4/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ProfileCollectionView: UICollectionView {
    let layout: UICollectionViewFlowLayout = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        return layout
    }()
    
    init(frame: CGRect) {
        super.init(frame: frame, collectionViewLayout: layout)
        backgroundColor = .clear
        isPagingEnabled = true
        delaysContentTouches = false
        showsHorizontalScrollIndicator = false
        register(ProfileActivityCell.self, forCellWithReuseIdentifier: "activity")
        register(ProfileSavedCell.self, forCellWithReuseIdentifier: "saved")
        register(ProfileInfoCell.self, forCellWithReuseIdentifier: "info")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
