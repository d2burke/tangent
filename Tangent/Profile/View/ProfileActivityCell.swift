//
//  ProfileActivityCell.swift
//  Tangent
//
//  Created by Daniel Burke on 4/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit

class ProfileActivityCell: UICollectionViewCell, ProfileCell {
    var items = [IdentifiableViewModel]() {
        didSet {
            self.activityDataSource.items = items
            self.activityTableView.reloadData()
        }
    }
    
    let activityTableView = ActivityTableView()
    let activityDataSource = ActivityDataSource()
    
    func configure(viewModel: IdentifiableViewModel) {
        guard let cellModel = viewModel as? ProfileActivityCellModel else { return }
        items = cellModel.items
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        activityTableView.delegate = activityDataSource
        activityTableView.dataSource = activityDataSource
        addSubview(activityTableView)
        installConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "activityTableView": activityTableView
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[activityTableView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[activityTableView]|", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
}
