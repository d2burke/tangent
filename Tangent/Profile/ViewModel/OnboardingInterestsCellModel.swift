//
//  OnboardingInterestsCellModel.swift
//  Tangent
//
//  Created by Daniel Burke on 3/15/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

extension OnboardingInterestsCellModel: IdentifiableViewModel {
    var identifier: String { return "interestsCell" }
    var cellHeight: Float { return 230 }
}

struct OnboardingInterestsCellModel {
    let interests: [InterestCellModel]
    
    init(interests: [InterestCellModel]) {
        self.interests = interests
    }
    
}
