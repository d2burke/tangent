//
//  ProfileInfoCellModel.swift
//  Tangent
//
//  Created by Daniel Burke on 4/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

extension ProfileInfoCellModel: IdentifiableViewModel {
    var identifier: String { return "info" }
    var cellHeight: Float { return 0.0 }
}

public struct ProfileInfoCellModel {
    let items: [IdentifiableViewModel]
    
    init(items: [IdentifiableViewModel]) {
        self.items = items
    }
}
