//
//  InfoButtonCellModel.swift
//  Tangent
//
//  Created by Daniel Burke on 4/14/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

extension InfoButtonCellModel: IdentifiableViewModel {
    var identifier: String { return "infoButton" }
    var cellHeight: Float { return 50.0 }
}

public struct InfoButtonCellModel {
    let buttonText: String
    
    init(buttonText: String = "") {
        self.buttonText = buttonText
    }
}


