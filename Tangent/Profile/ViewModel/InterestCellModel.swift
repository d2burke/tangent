//
//  InterestCellModel.swift
//  Tangent
//
//  Created by Daniel Burke on 3/15/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

extension InterestCellModel: IdentifiableViewModel {
    var identifier: String { return "interestCell" }
    var cellHeight: Float { return 230 }
}

struct InterestCellModel {
    let interest: Interest
    let score: Float
    
    init(interest: Interest, score: Float = 0) {
        self.interest = interest
        self.score = score
    }
}
