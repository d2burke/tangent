//
//  ActivityReactionCellModel.swift
//  Tangent
//
//  Created by Daniel Burke on 4/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation


extension ActivityReactionCellModel: IdentifiableViewModel {
    var identifier: String { return "reaction" }
    var cellHeight: Float { return 80.0 }
}

struct ActivityReactionCellModel {
    let reactionIcon: String
    let authorName: String
    let postTitle: String
    let dateString: String
    
    init(reactionIcon: String, authorName: String, postTitle: String, dateString: String) {
        self.reactionIcon = reactionIcon
        self.authorName = authorName
        self.postTitle = postTitle
        self.dateString = dateString
    }
}
