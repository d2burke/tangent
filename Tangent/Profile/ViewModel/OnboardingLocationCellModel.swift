//
//  OnboardingLocationCellModel.swift
//  Tangent
//
//  Created by Daniel Burke on 3/16/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

extension OnboardingLocationCellModel: IdentifiableViewModel {
    var identifier: String { return "locationCell" }
    var cellHeight: Float { return 230 }
}

struct OnboardingLocationCellModel {
    
}
