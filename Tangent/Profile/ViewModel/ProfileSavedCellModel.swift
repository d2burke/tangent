//
//  ProfileSavedCellModel.swift
//  Tangent
//
//  Created by Daniel Burke on 4/11/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

extension ProfileSavedCellModel: IdentifiableViewModel {
    var identifier: String { return "saved" }
    var cellHeight: Float { return 0.0 }
}

public struct ProfileSavedCellModel {
    let items: [IdentifiableViewModel]
    
    init(items: [IdentifiableViewModel]) {
        self.items = items
    }
}
