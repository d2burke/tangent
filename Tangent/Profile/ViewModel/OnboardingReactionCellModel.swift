//
//  OnboardingReactionCellModel.swift
//  Tangent
//
//  Created by Daniel Burke on 3/15/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

extension OnboardingReactionCellModel: IdentifiableViewModel {
    var identifier: String { return "reactionCell" }
    var cellHeight: Float { return 230 }
}

struct OnboardingReactionCellModel {
    let text: String
    let imageURL: String
    
    init(
        text: String = "",
        imageURL: String = ""
        ) {
        self.text = text
        self.imageURL = imageURL
    }
    
}
