//
//  InfoFieldCellModel.swift
//  Tangent
//
//  Created by Daniel Burke on 4/14/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation

extension InfoFieldCellModel: IdentifiableViewModel {
    var identifier: String { return "infoField" }
    var cellHeight: Float { return 66.0 }
}

public struct InfoFieldCellModel {
    let placeHolderText: String
    let fieldText: String
    let fieldIcon: String
    let isSecure: Bool
    
    init(placeHolderText: String = "", fieldText: String = "", fieldIcon: String = "", secure: Bool = false) {
        self.placeHolderText = placeHolderText
        self.fieldText = fieldText
        self.fieldIcon = fieldIcon
        self.isSecure = secure
    }
}

