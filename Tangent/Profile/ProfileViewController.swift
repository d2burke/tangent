//
//  ProfileViewController.swift
//  Tangent
//
//  Created by Daniel Burke on 1/8/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import UIKit
import Firebase

class ProfileViewController: UIViewController {
    
    let profileCollectionView = ProfileCollectionView(frame: .zero)
    let tabView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fillEqually
        stackView.axis = .horizontal
        
        let notificationsButton = Button(type: .custom)
        notificationsButton.setTitle("Activity", for: .normal)
        notificationsButton.setTitleColor(TAColor.dark.color, for: .normal)
        notificationsButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        stackView.addArrangedSubview(notificationsButton)
        
        /**
        let savedButton = Button(type: .custom)
        savedButton.setTitle("Saved", for: .normal)
        savedButton.setTitleColor(TAColor.dark.color, for: .normal)
        savedButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        stackView.addArrangedSubview(savedButton)
        */
        
        let infoButton = Button(type: .custom)
        infoButton.setTitle("My Info", for: .normal)
        infoButton.setTitleColor(TAColor.dark.color, for: .normal)
        infoButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        stackView.addArrangedSubview(infoButton)
        
        return stackView
    }()
    
    let pageHeaderView = PageHeaderView(viewModel: PageHeaderViewModel(
        searchFieldPlaceholderText: "Search My Profile",
        searchFieldIcon: "S",
        pageTitleText: "Profile",
        headerImageViewURL: "profile"
    ))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        view.addSubview(pageHeaderView)
        view.addSubview(tabView)
        profileCollectionView.delegate = self
        profileCollectionView.dataSource = self
        view.addSubview(profileCollectionView)
        installConstraints()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func installConstraints() {
        let views: [String: UIView] = [
            "pageHeaderView": pageHeaderView,
            "tabView": tabView,
            "profileCollectionView": profileCollectionView
        ]
        
        blockAutoResizing(views: views.map({ $0.value }))
        
        let constraints = [
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[pageHeaderView(==175)][tabView(==40)]-15-[profileCollectionView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[pageHeaderView]|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[tabView]-15-|", options: [], metrics: nil, views: views),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[profileCollectionView]|", options: [], metrics: nil, views: views)
        ]
        NSLayoutConstraint.activate(constraints.flatMap({ $0 }))
    }
    
    func logout() {
        do {
            try FIRAuth.auth()?.signOut()
        } catch {

        }
    }
}
