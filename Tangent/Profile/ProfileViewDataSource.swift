//
//  ProfileViewDataSource.swift
//  Tangent
//
//  Created by Daniel Burke on 4/9/17.
//  Copyright © 2017 Daniel Burke. All rights reserved.
//

import Foundation
import UIKit

protocol ProfileCell {
    func configure(viewModel: IdentifiableViewModel)
}

extension ProfileViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return profileTabs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellModel = profileTabs[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellModel.identifier, for: indexPath) as! ProfileCell
        cell.configure(viewModel: cellModel)
        
        if let infoCell = cell as? ProfileInfoCell {
            infoCell.delegate = self
            return infoCell
        }
        
        return cell as! UICollectionViewCell
    }
}

extension ProfileViewController: ProfileCellDelegate {
    func didTapInfoButton(button: Button) {
        logout() //ProfileViewController:logout()
    }
}
